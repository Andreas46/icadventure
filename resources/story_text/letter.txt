                                    28th November 1912

                                       Whitaker Estate
                                        Norwich Forest
                                               Norfolk

Dear Dr Paul Smith,
                   I'm writing to you, a member of the Topaz Society of Sciences and Scholarly Arts, with The most agreeable of news.

Despite you and your college's unwarranted ridicule of my work and unfounded assertions with regard to my character, I am graciously prepared to put the whole untoward incident behind us, and accept your written apology which you will be compelled to submit after bearing witness to my accomplishments.

I have pressed forward with my research despite the Society's lack of financial, and professional support, With admirable results if I do say so myself.

I, Dr Elric Whittaker, hereby invite your distinguished person to be Guest at the Whittaker Family Estate to witness and marvel the successful genesis of the first stable Fused Etheric-Corporeal Entity, here after referred to as Subject X.

While I will attest to the stability of Subject X in the physical sense, it's ability far exceeds my original expectation, and so for your own safety, I must insist that the utmost caution be exhibited while you are a guest at the Whittaker est--