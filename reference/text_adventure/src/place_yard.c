/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the Back yard, 			*/
/*	Place_yard, and location 8		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_yard.h"

int place_yard(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "yard";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
      switch (cmdtest[0]) {
	// If the user entered "look around", or just "look":
	case 0: {

	  // Print description.
	  printf("You're now outside, at the back of the mansion.  The ivy covered walls of the mansion\n"
	  "are to the west, with access back to laundry, SOUTH.  An overgrown hedge prevents\n"
	  "you from going any further east.  There is a tool SHED to the NORTH, and the dense\n"
	  "leafless wood discouraging any progress beyond the shed.\n");
	  return 0;
	}

	// look ... shed
	case 1: {
	  printf ("The wooden tool shed is a simple design, but appears to be robust.  Must have been built\n"
	  "by quite a carpenter considering how it looks less rotten that most of the wood inside\n"
	  "the mansion.\n");
	  if (OBJST[PLCCD][0] == 1) {	// if the shed still has a padlock on it:
	    printf("\nA heavy, old fashioned PADLOCK on the shed door prevents you from progressing NORTH\n"
	    "into the shed.\n");
	  }
	  return 0;
	}

	case 2: 	// look ... padlock
	case 3: {	// look ... lock
	   if (OBJST[PLCCD][0] == 1) {	// if you have not opened the padlock.
	     printf ("It's pretty big padlock, but it doesn't look very secure.\n");
	     return 0;
	   }	// If the padlock is gone...
	   printf("There is no padlock anymore.  It was whisked away, along with the paperclip, to the\n"
	   "magical land of Lucus's lost arts, where all unnecessary adventure game items end up.\n"
	   "Just as well too.  Sometimes, if an adventure game doesn't supply enough items to feed the\n"
	   "Sprite's that live there, they'll cross over to our world, and eat the entire\n"
	   "adventure game.  Ever wonder what happend to Monkey Island 5, or Sam and Max 2?  Thats\n"
	   "right.  Poor George.  He shouldn't have kept the REAL Starwars prequels on the same\n"
	  "server...  Those buggers sure are voracious.\n");
	return 0;
	}

	// look ... ivy
	case 4: {
	  printf("There's a thick cover of it on the southern, and western walls of the mansion.\n"
	  "Although it covers nearly the whole walls, it doesn't look very substantial with only\n"
	  "sparse autumn leaves on it.\n");
	  return 0;
	}
	// look ... hedge
	case 5: {
	  printf("The thick, thorny bush appears to be an evergreen variety, but you wouldn't\n"
	  "have guessed by how poorly it looks.  It's taller than you are, and has grown out so\n"
	  "much, that any path that may once have existed beside the mansion, is now inaccessible.\n");
	  return 0;
	}
	case 6:		// look ... wood
	case 7: {	// look ... forest
		printf("Its the forboding forest that's encircling the the mansion, and you got lost in to begin\n"
		"with.  You have intention of wandering off into it.\n");
		return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
      }
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 4:
		case 0:	{	// North
			// if the door is open:
			if (OBJST[PLCCD][0] == 0) {
				change_location(9);
				return 0;
			}
			printf("You can't, there is big padlock on the shed door doing it's job by keeping you out.\n");
			return 0;
		}
		case 1: {	// South
			change_location(7);
			return 0;
		}
		case 2:		// Forest
		case 3: {	// woods
			printf("You have no desire to wander aimlessly around the forrest until you starve to death.\n"
			"This mansion may be creepy, but at least your not likely to get lost in it.\n");
			return 0;
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	  	switch (cmdtest[0]) {
	  	  // open ... door
	  	  case 0: {
			if (OBJST[PLCCD][0] == 0) { // if door is open:
				printf("You've already unlocked the door.  It would be a waste of your time, and my code\n"
				"to make you manually open every door in the game. ;)\n");
				return 0;
			}

			// If it's locked, say so.
			printf ("You shake, push, pull, and swear at the door, but you finally begin to accept that\n"
			"the door will not open while it's got that big padlock on it.\n");
	  		return 0;
		  }
	  	}
	// Should not reach here
	printf("Control has reached end of \"Open\" in function: place_%s\n",funcloc);
	return -1;
    }

    //  Perform action code 3 or use.

    case 3: {
	switch (cmdtest[0]) {
	    case 0: {  // Use... paperclip ...
		if (INV[7] == 1) {	// If you don't have a paperclip:
			// return for witty repartee	
			return -2;
		} 
		// If your not using the paperclip with anything usefull:
		if (cmdtest[1] < 0) {	
		    // return for witty repartee
		    return -2;
		}
		// Use ... paperclip ... door
		if (cmdtest[1] == 1) {
		    printf("You try scraping the door with the paperclip, but your progressing too slowly.\n"
		    "Perhaps if you directed your efforts toward the mechanism retaining the door?\n");
		    return 0;
		}
		// Use ... paperclip ... (padlock || lock)
		if (cmdtest[1] == 2 || cmdtest[1] == 3) {
			OBJST[PLCCD][0] = 0;
			INV[7] = 1;
			printf("You unfold the large paperclip, insert it into the padlock keyhole which it easily\n" 
			"fits in, and start twisting it around in random combonations.  After a few minutes,\n"
			"you hear a click, which prompts you to successfully remove the lock.\n");
			return 0;
		}
	    }
	}
	printf("Control has reached end of \"Use\" in function: place_%s\n",funcloc);
	return -1;

    }

    //  Miscellaneous commands

    case 8: {
	switch (cmdtest[TESTNUM -1]) {

	    case 0:	// yank
	    case 1: {	// pull

		switch (cmdtest[0]) {
		    case 0:	// pull ... padlock
		    case 1: {	// pull ... lock

			// if there is no padlock.
			if (OBJST[PLCCD][0] == 0) {
			    printf("You weren't paying attention.  You've ALREADY pulled off the lock automatically when you unlocked the door.\n");
			    return 0;
			}

			// if there is:
			printf("You give the padlock a hard yank.  Yep.  Definitely locked.\n");
			return 0;
		    }
		    // pull ... ???
		    default: {
			// return for witty repartee
			return -2;
		    }
		}
		case 2: {	// Climb
		  switch (cmdtest[0]) {
		    case 0:	// climb ... wall
		    case 1: {	// climb ... ivy
			printf("Deciding that you might have a better view on the roof of the mansion, you start climbing\n"
			"up the wall using the ivy.  Unfortunately for you, it's not until your almost at the top\n"
			"that ivy becomes too weak to support your weight, and you fall to the hard ground below.\n");
			game_over(cmdlen);
			return 1;
		    }
		    case 2: {	// climb ... hedge
			printf("Thats not a good idea.  The hedge is almost closer to a bramble than a hedge.  It's\n"
			"riddled with long sharp spines which you have no desire to make contact with.\n");
			return 0;
		    }
		    default: {
			// return for witty repartee
		  	return -2;
		    }
		  }
		}
		case 3: {	// Pick
		   if (cmdtest[0] == 0) {	// Pick ... nose
			printf("[You just can't help yourself, can you?]\n"
			"You get bored with adeventure gameing, and decide to engage in an activity more worthy\n"
			"of your attention.  You go in for a booger but it just won't come out, so you assult it\n"
			"with a second finger.  You manage to get a hold of it, and extract the long stringy snot.\n"
			"you roll it around between two fingers until it congeals enough for you to flick it\n"
			"into the hedge.  Now that you've got that out of your system, back to the game.\n");
			return 0;
		    }
		    // pick ... padlock || lock
		    if (cmdtest[0] == 1 ||cmdtest[0] == 2 || cmdtest[1] == 1 || 
								    cmdtest[1] == 2) {
			if (cmdlen < 3) { // just "pick lock"
				printf("With what?  You left your 32 piece lock pick set in your other pants, and your bump\n"
				"keys won't fit this lock.\n");
				return 0;
			}

			// pick ... lock||padlock ... paperclip
			if (cmdtest[1] == 3 || cmdtest[0] == 3) {
			    int dmytst[TESTNUM] = {0,2,-1};
			    place_yard(cmdlen, 3, dmytst, 0);
			    return 0;
			}
			// pick ... lock||padlock ???
			printf("locks can't be picked that way.\n");
			return 0;
		    }
		    // pick ???
		    printf("There is only one item that can be picked here, and it's not your nose.\n");
		    return 0;
		}
	    }
	    printf("Control has reached end of \"Misc\" in function: place_%s\n",funcloc);
	    return -1;
	}
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

