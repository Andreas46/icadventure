/************************************************/
/*						*/
/*  input_ctrl.c				*/
/*						*/
/*  This file is containes the functions	*/
/*  relative aquiring, and manipulating input	*/
/*  from the user.				*/
/*						*/
/************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "input_ctrl.h"

// Define variables for processing user commands
#define NOOFSTR 20 		// Max words to process. 
#define INPUTWRDBUFSIZE 30	// Max characters per word
// #define CMPBUFF 16		//  buffer size for static word comparison.

char *NEXTCMD[NOOFSTR];


// This function will take input from the user, remove
// puntuation, make sure it's all lower case, and then
// store it in the global array of strings; *NEXTCMD[].

int get_input()
{
	char cmdbuff[INPUTWRDBUFSIZE], cmdend = ' ';
	int i, j, k, cmdlen;


	printf ("\n> ");	// game prompt.

	// take user input

	for (i = 0; cmdend != '\n'; i++) {
		char cmdread[255];

		//  Get word "i" from input file
		scanf ("%254s",cmdread);

		// Sanitise input.
		for (j = 0, k = 0; j <= strlen(cmdread);j++) {	
			// Put lower case character into temporary string,
			// or skip character if it's punctuation.
			if (!ispunct(cmdread[j])) {
				cmdbuff[k] = tolower(cmdread[j]);
				k++;
			}
		}

		// allocate memory in array for word.
		NEXTCMD[i] = malloc(strlen(cmdbuff) + 1);

		// test to see if memory was properly allocated
		if (NEXTCMD[i] == NULL) {
			printf ("Can't allocate memory!\n");
			//QuitSafely ();
		}

		// Copy word from buffer, to newly allocated memory, in array.
		strcpy (NEXTCMD[i], cmdbuff);

		// Get next character in input ready to check if its the last. 
		cmdend = getchar();

		// test to check the user hasn't entered too many words.
		if (i == NOOFSTR) {
			printf ("Too... Many... words...\n");
			return 0;
		}
	}
	// Define number of commands inputed:
	cmdlen = i;

	// for better formatting...
	putchar('\n');

	return cmdlen;
}

// De-allocate memory, and compalain if it doesn't work.

int free_mem(int cmdlen)
{	
	int i;
	
	for (i = 0; i < cmdlen; i++) {
		// Free all the elements of NEXTCMD.
		free(NEXTCMD[i]);
	}

	return 0;
}


// Compair a string to each element in the current input.

#define setary(refarylen) \
char *refary[refarylen] = aryobj;	\
arylen = (refarylen);	\
cmpary = ary_cpy(cmpary, arylen, refary);	\
break

int input_srch(int cmdlen, int arynum, int elcmp)
{
	void free();

	int i,j;

	// Test to make sure the specified element of NEXTCMD is within bounds.
	if (elcmp > cmdlen - 1) {
		printf("specified element of NEXTCMD out of bounds!\n");
		return -2;
	}

	//  struct to hold all possible search options
	//  (dam, what a waste of memory!  isn't there a more conservetive way, 
	//  that doesen't involve a mess of macro's and case statements?)
	//  (...And I can't use structures containg auto arrays (flexi?) members
	//  inside another structure or array!?  ...But you could with gcc versions
	//  less than 3.0!?!? Curse you C!!)

	#define maxwrd 16 /*An arbitrary word length neccessitated by C's homosexuality*/
	struct  cmdstruct {int cmdsetid; char *cmdset[maxwrd];}
	cmdlst[] = { 

	  // Primary commands to search input for:
	  {0, {"look","observe",NULL}},	// look
	  {1, {"go","move","travel","walk","north","south","east","west",NULL}},  // move
	  {2, {"get","take","obtain","liberate",NULL}},  // get
	  {3, {"inventory","inv",NULL}},
	  {4, {"quit",NULL}},
	  {5, {"restart",NULL}},
	  {6, {"open",NULL}},
	  {7, {"use",NULL}},
	  {8, {"hint","help",NULL}},
	  // Inv look options.
	  {31, {"broom","doorstop","cat","key","moustache","mo","salt","gum","mop","paperclip","crowbar","hacksaw", "handle",NULL}},
	  // Inventory use options, for items within the inventory
	  {32,{ "hacksaw","mop",NULL}},

	  //  Random option for extra searching.
	  {51, {"through",NULL}},
	  {52, {"under",NULL}},

	  //  Secondry commands for place_verandah (1) to search input for:
	  {101, {"broom","cat","doorstop","verandah",NULL}},  // Get options
	  {102, {"around","door","windows","window","doormat","mat","doorstop","cat","broom","keyhole","lock","verandah","key",NULL}},  //look options
	  {103, {"north","south","east",NULL}},  //move options
	  {104, {"door",NULL}},  //open options
	  {105, {"key","lock","door",NULL}},  //use options
	  {179, {"lift","unlock",NULL}},  // Micellaneous (primary) commands.
	  //  Options for each of the miscellaneous primary commands.
	  {180, {"mat",NULL}},
	  {181, {"door",NULL}},

	  // Secondry command for place_foyer (2)
	  {201, {NULL}},  // get - Not used.
	  {202, {"around","foyer","portrate","painting","door","stair","stairs",NULL}},  // look opt
	  {203, {"north","south","east","west","up","upstairs",NULL}},  // Move opt
	  {204, {"door",NULL}},  // Open opt
	  {205, {"mo","moustache","painting","portrate",NULL}},  // Use opt
	  {279, {"kick",NULL}},  // Misc Primary commands
	  //  Options for each of the miscellaneous primary commands.
	  {280, {"door",NULL}},  // Kick opt

	  // Secondry command for place_hall1 (3)
	  {301, {NULL}},	// get - Not used. 
	  {302, {"around","window","wall","walls",NULL}},  // look opt
	  {303, {"south","east","west",NULL}},  // Move opt
	  {304, {"window",NULL}},  // Open opt
	  {305, {NULL}},  // Use opt - Unused
	  {379, {NULL}},  // Misc Primary commands - Unused

	  // Secondry commands for place_library (4)
	  {401, {"moustache","mo",NULL}},	// Get opt
	  {402, {"around","window","windows","book","books","bust",NULL}},  // look opt
	  {403, {"east",NULL}},  // Move opt
	  {404, {NULL}},  // Open opt
	  {405, {"mop","bust",NULL}},  // Use opt
	  {479, {"put","talk",NULL}},  // Misc Primary commands
	  //  Options for each of the miscellaneous primary commands.
	  {480, {"mop",NULL}},  // put opt
	  {481, {"bust"}},  // talk opt

	  // Secondry commands for place_kitchen (6)
	  {601, {"utensils","soylent","droppings","gum","salt",NULL}},  // Get opt
	  {602, {"around","oven","utensils","window","windows","shelves","shelf","items","soylent",NULL}},  // Look opt
	  {603, {"east","west",NULL}},  // Move opt
	  {604, {NULL}},  // Open opt
	  {605, {NULL}},  // Use opt
	  {679, {NULL}},  // Misc Primary commands

	  // Secondry commands for place_laundry (7)
	  {701, {"mop",NULL}},  // Get opt
	  {702, {"around","sink","mangle","mangles","bench",NULL}},  // Look opt
	  {703, {"north","west",NULL}},  // Move opt
	  {704, {NULL}},  // Open opt
	  {705, {"mangle","mangles",NULL}},  // Use opt
	  {779, {NULL}},  // Misc Primary commands

	  // Secondry commands for place_yard (8)
	  {801, {"lock","padlock",NULL}},  // Get opt
	  {802, {"around","shed","padlock","lock","ivy","hedge","wood","forest",NULL}},  // Look opt
	  {803, {"north","south","woods","forest","shed",NULL}},  // Move opt
	  {804, {"door",NULL}},  // Open opt
	  {805, {"paperclip","door","lock","padlock",NULL}},  // Use opt
	  {879, {"yank","pull","climb","pick",NULL}},  // Misc Primary commands
	  //  Options for each of the miscellaneous primary commands.
	  {880, {"lock","padlock",NULL}},  // yank opt
	  {881, {"lock","padlock",NULL}},  // pull opt
	  {882, {"wall","ivy","hedge",NULL}},  // climb opt
	  {883, {"nose","padlock","lock","paperclip",NULL}},  // pick opt

	  // Secondry commands for place_shed (9)
	  {901, {"hammer","pincers","dust","crowbar","hacksaw",NULL}},  // Get opt
	  {902, {"around","shed","tools","bench","dust","hammer","pincers",NULL}},  // Look opt
	  {903, {"south",NULL}},  // Move opt
	  {904, {NULL}},  // Open opt
	  {905, {NULL}},  // Use opt
	  {979, {NULL}},  // Misc Primary commands
	  //  Options for each of the miscellaneous primary commands.

	  {-1, {NULL}}

	  // Secondry commands for place_library (4)
/*	  {01, {,NULL}},  // Get opt
	  {02, {,NULL}},  // Look opt
	  {03, {,NULL}},  // Move opt
	  {04, {,NULL}},  // Open opt
	  {05, {,NULL}},  // Use opt
	  {79, {,NULL}},  // Misc Primary commands*/
	  //  Options for each of the miscellaneous primary commands.

	};

	for (i = 0; arynum != cmdlst[i].cmdsetid; i++) {
		// if we reach the end of the command sets:
		if (cmdlst[i].cmdsetid == -1) {
			printf("invalid search aray!");
			return -3;
		}
	}
	for (j = 0; cmdlst[i].cmdset[j] != NULL; j++) {
		if (strcmp(NEXTCMD[elcmp], cmdlst[i].cmdset[j]) == 0) {
			return j;	// Return element number of matched word
		}
	}

	// no items found...
	return -1;
	// Test each string in the provided array, with the element of NEXTCMD
	// specified.
/*	for (i = 0; i < arylen; i++) {
		if (strcmp(NEXTCMD[elcmp], cmpary[i]) == 0) {
			elmatch = i;
			break;
		}
	}

	// Free up cmpary.
	for (i = 0; i < arylen; i++) {
		free(cmpary[i]);
	}
	free(cmpary);

	// If no element was found to match:
	if (elmatch == -1) {
		return -1;
	}

	return elmatch;*/
}

// Function to dynamically allocate the appropriate static array for 
// comparison to the collected input.  (Uses least memory).

char** ary_cpy(char **cmpary, int arylen, char *refary[arylen])
{	void *malloc();
	void exit();

	int i;
	
	// allocate memory for pointers to strings:
	cmpary = malloc(arylen * sizeof *cmpary);

	// check memory allocation.
	if (cmpary == NULL) {
		printf ("Faild to allocate memory for cmpary!\n");
		exit(-1);
	} 

	// Allocate memory for each string in cmpary:
	for (i = 0; i < arylen; i++) {
		cmpary[i] = malloc(strlen(refary[i]) + 1);
		// check memory allocation.
		if (cmpary[i] == NULL) {
			printf ("Faild to allocate memory for cmpary[%d]!\n", i);
			exit(-1);
		} 

		strcpy (cmpary[i], refary[i]);
	}
	return cmpary;
}
