/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the First Hall way,		*/
/*	place_hall1, and location 3		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_hall1.h"

int place_hall1(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "hall1";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // Print description with available objects.
	  printf("You are standing in a dimly lit corridor, with the only light source,\n"
	  "an almost opaque grimy window at he end of the hall way, casting broken\n"
	  "shadows on the peeling wallpaper, and stained celling.\n\n"

	  "The living room is off to the EAST, and what looks like a library to\n"
	  "the WEST.  The foyer is back to the SOUTH.\n");

	  return 0;
	}

	// look ... window
	if (cmdtest[0] == 1) {
	  printf ("It's exceptionally dirty.  Even for THIS mansion.  It's as if\n"
		"someone has thrown balls of mud at it from the outside.\n");
	  return 0;
	}

	// look ... wall || walls
	if (cmdtest[0] == 2 || cmdtest[0] == 3 ) {

	printf ("Aside from the creepy shadows, the walls are rather unremarkable.\n");
	return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// South
			change_location(2);	// foyer
			return 0;
		}
		case 1: {	// east
			change_location(5);	// living room
			return 0;
		}
		case 2: {	// west
			change_location(4);	// library
			return 0;
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	  	switch (cmdtest[0]) {
	  	  // open ... window
	  	  case 0: {
		    printf("That window will never open again, and even if it did, you would much prefer\n"
			"a dimly lit hallway to the cascade of dirt that would shower you upon opening the\n"
			"window.\n");
		    return 0;
		  }
	  	}
	// open ... ???
	printf("Control has reached end of \"Open\" in hall 1");

	return 0;
    }

    //  Perform action code 3 or use.

    case 3: {	// not used
	printf("unused!\n");
	return -1;
    }

    //  Miscellaneous commands

    case 8: {	// not used
	printf("unused!\n");
	return -1;
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

