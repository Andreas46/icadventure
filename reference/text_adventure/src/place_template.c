/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the template, 			*/
/*	Place_template, and location x		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_*****.h"

int place_*****(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "template";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // compile a list of available items.
	  int objel[] = {1,2}, invel[] = {0,1};
	  char *avobj;

	  avobj = av_itms(0, objel, invel, avobj);

	  // Print description with available objects.
	  printf("description.\n\n");

	  printf("Near you, you can see: %s\n",avobj);
	
	  free(avobj);
	  return 0;
	}

	// look ... item 1
	if (cmdtest[0] == 1) {
	  printf ("item 1 description.\n");
	  return 0;
	}

	// look ... item 2
	if (cmdtest[0] == 2 ) {

	  // look ... through item 2.
	  if (input_srch(cmdlen, 51 ,inputpos -2) >= 0) {
	    printf("Item 2 is opaque\n");
	    return 0;
	  }
	  // look ... (!through)[windows || window]
	  printf ("Tis a conceptual item\n");
	  return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// North
			// if the door is open:
			if (OBJST[PLCCD][0] == 0) {
				printf ("going north!\n");
				change_location(1);
				return 0;
			}
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	  	switch (cmdtest[0]) {
	  	  // open ... door
	  	  case 0: {
			if (OBJST[PLCCD][0] == 0) { // if door is open:
				printf("The scary door is already open.\n");
				return 0;
			}

	  		// if the door is unlocked, open it:
	  		if (OBJST[PLCCD][4] == 0) {
	  			OBJST[PLCCD][0] = 0;
	  			printf("The scary door opens with a creak.\n");
	  			return 0;
	  		}
	  		
			// If it's locked, say so.
			printf ("You swerve, narrowly avoiding the scary door.\n");
	  		return 0;
		  }
	  	}
	// Should not reach here
	printf("Control has reached end of \"Open\" in function: place_%s\n",funcloc);
	return -1;
    }

    //  Perform action code 3 or use.

    case 3: {
	switch (cmdtest[0]) {
	    case 0: {  // Use...banna...
		if (cmdtest[1] < 0) {
		    printf("What do you want to use the banna with?\n");
		    return 0;
		}
		// Use ... key ... door
		if (cmdtest[1] == 1) {
		    int dmytst[TESTNUM] = {0,0,1};
//		    place_verandah(cmdlen, 8, dmytst, 0);
		    return 0;
		}
	    }
	}
	printf("Control has reached end of \"Use\" in function: place_%s\n",funcloc);
	return -1;

    }

    //  Miscellaneous commands

    case 8: {
	switch (cmdtest[TESTNUM -1]) {

	    case 0: {	// grove

		switch (cmdtest[0]) {
		    case 0: {	// grove ... up

			//  If the grove is down:
			if (OBJST[PLCCD][0] == 1) {
			    OBJST[PLCCD][0] = 1;
			    printf("You lift up the grove, and party OWAN!\n");

			   return 0;
			}

			// if not...
			printf("Grove be grove'n at maximum groviness!\n");
			return 0;
		    }
		}
	    }
	    printf("Control has reached end of \"Misc\" in function: place_%s\n",funcloc);
	    return -1;
	}
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

