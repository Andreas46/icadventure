/****************************************/
/*					*/
/*  A text adventure written in C!	*/
/*  Do not use this source file for 	*/
/*  Cheeting!				*/
/*					*/
/*  Programmed by Andreas Fischer for	*/
/*  Practice, and learning of C		*/
/*  Commenced prgramming on 26/10/07	*/
/*					*/
/*  31-05-08: I've been at for a long 	*/
/*  time now.  I think I've got the	*/
/*  hang of C, so I think I'll develop	*/
/*  A a Tibes clone using OGRE now.	*/
/*  (C++ can't be that different right?)*/
/*  and I shall call it... Effigy!	*/
/*					*/
/****************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

#include "txtadv1.h"
#include "input_ctrl.h"
#include "inv_ctrl.h"

#include "place_verandah.h"
#include "place_foyer.h"
#include "place_hall1.h"
#include "place_library.h"
#include "place_kitchen.h"
#include "place_laundry.h"
#include "place_yard.h"
#include "place_shed.h"

// Define Starting location
#define STARTLOC 4	// verandah
short PLCCD = STARTLOC;

// Size to initilize comparison array's strings to.
// const char CMPSIZE = 16;


/************ Declare functions ************/

int call_location(int,int,int*,int);
int perform_action(void);
int give_help(int*);
int quit_sure();
void fare_well();

/************** Begin program **************/

int main()
{
	int gamecontinue = 0, gameretry = 0, i, j;
	
	// Make a backup of OBJST:
	short objstbak[NUMOFPLACES][MAXOBJ];
	
	for (i = 0; i < NUMOFPLACES; i++) {
		for (j = 0; j < MAXOBJ; j++) {
			objstbak[i][j] = OBJST[i][j];
		}
	}

	// Welcome message.
	printf ("\f");
	printf ("Welcome to my text adventure game!\n\n");


	// Begin game

	while (gameretry == 0) {
		int dmytst[TESTNUM] = {0,0,0};

		// print starting locations description.
		call_location(0,0,dmytst,-1);

		// Play until user dies.
		while (gamecontinue <= 0) {
			gamecontinue = perform_action();
		}

		// Game over.  Reset inventory:
		for (i = 0; i < INVLEN ; i++) {
			INV[i] = 1;
		}
		
		// ...Reset item states:
		for (i = 0; i < NUMOFPLACES; i++) {
			for (j = 0; j < MAXOBJ; j++) {
				OBJST[i][j] =  objstbak[i][j];
			}
		}

		// ...Reset Game starting location:
		PLCCD = STARTLOC;
		
		// ...And continue the game:
		gamecontinue = 0;
	}

	return 0;
}

// After the user performs an action, it shall be
// Proceesed (parsed?) with the following function


int perform_action()
{
	int cmdlen, gamecont = 0, cmdtest[TESTNUM] = {-1,-1,-1}, i, j, k;

	// get input :)
	cmdlen = get_input();

	// Find out what the first command is:
	for (i = 0; i < cmdlen; i++) {
		// is it to look around, or at something?
		if (input_srch(cmdlen, 0, i) >= 0) {

			// If The command was simply "look":
			if (cmdlen == 1) {
				int dmytst[TESTNUM] = {0,0,0};
				gamecont = call_location(cmdlen, 0, dmytst,-1);
			
				free_mem(cmdlen);
				return gamecont;
			}

			// Search input for inventory items that one might look at.
			for (i = 0; i < cmdlen && cmdtest[0] < 0; i++) {
				cmdtest[0] = input_srch(cmdlen, 31, i);
			}

			//  If any were found:
			if (cmdtest[0] >= 0) {
				inv_look(cmdlen, cmdtest[0]);

				free_mem(cmdlen);
				return 0;
			}

			// Search for objects that one might look at.
			for (i = 0; i < cmdlen && cmdtest[0] < 0; i++) {
				cmdtest[0] = input_srch(cmdlen, (PLCCD * 100 + 2), i);
			}

			//  If it's to look at somthing specific to the current location:
			if (cmdtest[0] >= 0) {
			    gamecont = call_location(cmdlen, 0, cmdtest, i);  // 0 = look
			} else {	// If you cant look at that object:
				printf ("You can't look at that.\n");
			}

			free_mem(cmdlen);
			return gamecont;
		}
		// is it to move somewhere?
		if (input_srch(cmdlen, 1, i) >= 0) {

			//  Search input for valid destinations.
			for (i = 0; i < cmdlen && cmdtest[0] < 0 ; i ++) {
				cmdtest[0] = input_srch(cmdlen, (PLCCD * 100 + 3), i);
			}

			// If the destination is valid:
			if (cmdtest[0] >= 0) {
				gamecont = call_location(cmdlen, 1, cmdtest, -1);  // 1 = "move"
			} else {
				printf("You can not go there!\n");
			}

			free_mem(cmdlen);
			return gamecont;
		}	

		// is it to quit?
		if (input_srch(cmdlen, 4, i) >= 0) {

			printf ("Are you sure you want to quit? [y/n]: ");
			if (quit_sure() == 1) {
				free_mem(cmdlen);
				fare_well();
			}

			free_mem(cmdlen);
			return 0;
		}

		// Is it to get something?
		if (input_srch(cmdlen, 2, i) >= 0) {
			//  Search input for obtainable items.
			for (j = 0; j < cmdlen && cmdtest[0] < 0; j++) {
				cmdtest[0] = input_srch(cmdlen, (100 * PLCCD + 1), j);
			}

			//  If the item is obtainable:
			if (cmdtest[0] >= 0 && get_itm(cmdlen, cmdtest[0]) == 0) {

			} else {	// Otherwise:
				printf ("That can not be obtained!\n");
			}

			free_mem(cmdlen);
			return 0;
		}

		// Is it to print the inventory?
		if (input_srch(cmdlen, 3, i) >=0) {
			inv_print(cmdlen);

			free_mem(cmdlen);
			return 0;
		}

		//  Is it to restart the game?
		if (input_srch(cmdlen, 5, i) >= 0) {
			printf("Are you sure you want to restart the game? [y/n]:");

			free_mem(cmdlen);
			if (quit_sure() == 1) {
				return 1;
			}
			return 0;
		}

		//  Is it to get help?
		if ((cmdtest[0] = input_srch(cmdlen, 8, i)) >= 0) {
			give_help(cmdtest);

			free_mem(cmdlen);
			return 0;
		}

		//  Is it to open something?
		if (input_srch(cmdlen, 6, i) >= 0)  {

			for (j = 0; j < cmdlen && cmdtest[0] < 0; j++) {
	  			cmdtest[0] = input_srch(cmdlen, (PLCCD * 100 + 4), j);
			}

			// If the object is able to be opened:
			if (cmdtest[0] >= 0) {
				gamecont = call_location(cmdlen, 2, cmdtest, -1);  // 2 = "open"
			} else {
				printf("That doesn't open!\n");
			}

			free_mem(cmdlen);
			return gamecont;
		}

		// is it to use an item?
		if(input_srch(cmdlen, 7, i) >= 0) {
			//  Search input for ambiguous item usage.
			for (j = 0, k = 0; j < cmdlen && k < 2; j++) {
				cmdtest[k] = input_srch(cmdlen, 32, j);
				// if a match is found, increment to a new cmdtest element.
				if (cmdtest[k] != -1) k++;
			}

			//  If a match was found, process it.
			if (cmdtest[0] >= 0) {
				// return if input was processed successfully.
				if ((gamecont = inv_use(cmdtest)) >= 0) {
					free_mem(cmdlen);
					return gamecont;
				}

				// reset cmdtest
				cmdtest[0] = -1;
				cmdtest[1] = -1;
				cmdtest[2] = -1;
			}

			//  Search input for "useable" items.
			for (j = 0; j < cmdlen && cmdtest[0] < 0; j++) {
				cmdtest[0] = input_srch(cmdlen, (100 * PLCCD + 5), j);
			}

			//  Search input for a second "usable" item.
			for (; j < cmdlen && cmdtest[1] < 0; j++) {
				cmdtest[1] = input_srch(cmdlen, (100 * PLCCD + 5), j);
			}

			//  If the item is obtainable:
			if (cmdtest[0] >= 0) {
				// 3 = use
				gamecont = call_location(cmdlen, 3, cmdtest, j);
				if (gamecont >= 0) {
					free_mem(cmdlen);
					return gamecont;
				}
			}
			// Otherwise:
			if (gamecont == -2) {
				witty_remark(3, cmdtest); // 3 = use
			}

			free_mem(cmdlen);
			return gamecont;
		}

		// Check for various Inventory targeted commands.


		// Is it some other miscellaneous, location specific command?

		for (j = 0; j < cmdlen && cmdtest[TESTNUM -1] < 0; j++) {
			cmdtest[TESTNUM -1] = input_srch(cmdlen, (PLCCD * 100 + 79), j);
		}

		// If there is a misc command found:
		if (cmdtest[TESTNUM -1] >= 0) {

			for (j = 0, k = 0; j < cmdlen && k < (TESTNUM -1); j++) {
			  cmdtest[k] = input_srch(cmdlen, 
				(PLCCD * 100 + 80 + cmdtest[TESTNUM -1]), j);
			  if (cmdtest[k] >= 0) {
				k++;
			  }
			}

			gamecont = call_location(cmdlen, 8, cmdtest, j);  // 8 = "misc"

			if (gamecont == -2) {
				witty_remark(0, cmdtest);
			}

			free_mem(cmdlen);
			return gamecont;
		}
	}

	// Command not defined.  Produce snappy response.
	witty_remark(0, cmdtest);

	free_mem(cmdlen);
	return 0;
}

// Function to handle witty remarks to uninteresting

int witty_remark(int actioncode, int* cmdtest)
{
  //  Randomly select a witty response.
  srandom(time(NULL));	// make random() return an actually random number.
  switch ((int)((float) random() / RAND_MAX * 7)) {
     //  Let 0 be an action specific response.
     case 0: {
	switch (actioncode) {
	  case 0: {	// just gibberish. Try a more general response.
		printf("Thats not going to work.\n");
		return 0;
	  }
	  case 3: {	// use
		printf("You can't use the %s that way.\n",NEXTCMD[cmdtest[0]]);
		return 0;
	  }
	}
     }
     case 1: {
	printf ("Thats not going to do anything useful.\n");
	return 0;
     }
     case 2: {
	printf ("Command not found.\n");
	return 0;
     }
     case 3: {
	printf("742: PEBCAC error!\n");
	return 0;
     }
     case 4: {
	printf("You can't achieve that in this fasion.  I was too lazy to write the code for it.\n");
	return 0;
     }
     case 5: {
	printf(");  // Fix this broken remark.");
	return 0;
     }
     default: {
	printf("404: internal witt engine fail.  Snappy comeback not found.\n");
	return 0;
     }
  }
     printf("witt engin fail.\n");
     return 0;
}

// Function to give some help if needed.

int give_help(int *cmdtest)
{
	if (cmdtest[0] == 0) {	// hint
		printf("You want a hint!?  Do you have any idea how much extra code it would take to implement\n"
		"a hint system for every possible puzzle?  It's a lot.\n\n"

		"Look, if your really stuck, and REALLY need a hint, send me an email at electronics45\n"
		"at gmail.com with where your up to, or look at the faq on the game's website (if I ever\n"
		"get around to making one.)\n");
		return 0;
	}
	if (cmdtest[0] == 1) {	// help
		printf("Valid commands are:\n"
		"- help:  What your reading now.\n"
		"- quit:  Exit game to terminal.\n"
		"- restart:  You can't reach a point in the game where you can't continue, but if you wish\n"
		"to start again, use this command.\n\n"

		"In each room you can interact with certian items usually, but not always, printed in\n"
		"CAPITALS.  There are specilised commands for specific areas, such as pick, talk,\n"
		"kick,etc, but some examples of the most general commands are:\n"
		"- look:  Every interactive object, and some non-interactiive objects\n"
		"can \"looked\" at for more information on them.  E.g. \"look at broom\".\n"
		"- get: Items that can be obtained will ALWALY be printed in uppercase.  Items you obtain\n"
		"are placed in your inventory, and can be reviewed with \"inventory\", or just \"inv\".\n"
		"- inventory / inv:  display currently held inventory.\n"
		"- use:  Use an inventory item with another inventory, or interactive objct, or use an\n"
		"interactive object.  E.g. \"use the key with the door\".\n"
		"- move  / go / travl / etc:  move to another location.  E.g. \"move north\".  You can also\n"
		"move just by typing the direction you would like to travel.  E.g. \"west\".\n"
		"- open:  Open something.  E.g. \"open door\".\n");
		return 0;
	}
	printf("End of function get_help.\n");
	return -1;
}

// This function will handle the change of locations

int change_location(int dest)
{
	int dmytst[TESTNUM] = {0,0,0};

	// change location
	PLCCD = dest;
	// Print description of location
	call_location(0,0,dmytst,-1);
	
	return 0;
}

// This function will call the locations function, from the
// perform_action function.

int call_location(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{	//  I don't like this method.  It's less readable, and uses more memory.
	//  I'll go back to my previous implementation (with case statements)
	//  After I road test this one a little more.
	int (*placefunct[])() = { 
	NULL, place_verandah, place_foyer, place_hall1, place_library, NULL, place_kitchen, place_laundry, place_yard, place_shed, NULL};

	// I've done it this way so I can make place_'s inaccessible with NULL's
	if (PLCCD > (sizeof(placefunct) / sizeof(int (*)) - 1) 
						|| *placefunct[PLCCD] == NULL) { 
		printf("Can't call location %d",PLCCD);
		return -1;
	}

	// Call apropriate location fuction, and pass approprite parameters.
	return (*placefunct[PLCCD])(cmdlen, actioncode, cmdtest, inputpos);
}

//  When you die...

int game_over(int cmdlen)
{
	printf ("\nGame Over. :(\n\n");

	// Ask user if he wants to try again, and quit if he doesn't.
	printf ("Would you like to try again? [y/n]: ");
	if (quit_sure() == 0) {
		// Free up memory allocated to NEXTCMD:
		free_mem(cmdlen);
		fare_well();
	}
	putchar('\n');	// for constency.

	return 0;
}

int quit_sure()
{	int skipgarb(); 
	void exit();
	
	char give_up;

	//  Process users answer, and retry if garbage was entered.
	while ((give_up = getchar())) {
		skipgarb();
		switch (give_up) {
			case 'y': return 1;
			case 'Y': return 1;
			case 'n': return 0;
			case 'N': return 0;
		}
		printf ("\nPlease enter \"y\" or \"n\": ");
	}

	// program should never reach here...
	printf ("You broke it!!\n");
	exit (0);
	return 0;
}


// fare thee well.

void fare_well()
{
	void exit();
	
	printf ("\nThank you for playing my first text adventure game.\n");
	printf ("I hope you wasted as much time playing it, as I did \n");
	printf ("making it. :)\n\n");

	exit (0);
}

// skip garbage corrupting scanf

int skipgarb()
{ 
	while (getchar() != '\n')
	{}
	return 0;
}
