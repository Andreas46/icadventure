/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is The verandah, location 1, and	*/
/*	starting location.			*/
/*						*/
/************************************************/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "input_ctrl.h"
#include "place_verandah.h"


int place_verandah(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)

{

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // compile a list of available items.
	  int objel[] = {1,2}, invel[] = {0,1};
	  char *avobj;

	  avobj = av_itms(2, objel, invel, avobj);

	  // Print description with available objects.
	  printf("You Are in front of huge old mansion.\n"
		 "Your standing on an a weathered verandah, with a rotting\n"
		 "DOORMAT on the threshold of a heavy oak DOOR to the NORTH.\n"
		 "There are several WINDOWS, too dirty to see through\n"
		 "clearly.  The paint is peeling off the panelled walls,\n"
		 "and the place in general disrepair.\n\n");

	  printf("Near you, you can see: %s\n",avobj);

	  free(avobj);
	  return 0;
	}

	// look ... door
	if (cmdtest[0] == 1) {
	  printf ("The large oak DOOR looks very old and weathered, with a\n");
	  printf ("tarnished brass door knob, accommodating a large KEYHOLE.\n");
	  return 0;
	}

	// look ... [windows || window]
	if (cmdtest[0] == 2 || cmdtest[0] == 3) {

	  // look ... through [windows || window]
	  if (input_srch(cmdlen, 51 ,inputpos -2) >= 0) {
	    printf("You try to look through one of the dirty WINDOWS, but they\n"
		  "are too grimy to see into the dark rooms within the house.\n");
	    return 0;
	  }
	  // look ... (!through)[windows || window]
	  printf ("The panelled WINDOWS are caked with generations of dust, and \n"
		 "dirt or perhaps they were just made in Redmond, and are suppose\n"
		  "to be this shoddy.\n");
	  return 0;
	}

	// look ... [doormat || mat]
	if ((cmdtest[0] == 4 || cmdtest[0] == 5)) {

	  // look ... under mat (Alias for "lift ... mat")
	  if (input_srch(cmdlen, 52, inputpos -2) >= 0) {
		int dmytst[TESTNUM] = {0,0,0};
		place_verandah(cmdlen, 8, dmytst, 0);
		return 0;
	  }
	  printf ("It's a rotting hessian door MAT caked with mud and leaf litter\n"
		  "which has blown onto the verandah over the years.\n");
	  return 0;
	}

	// look ... [keyhole || lock]
	if (cmdtest[0] == 9 || cmdtest[0] == 10) {
	  // look ... through [keyhole || lock]
	  if (input_srch(cmdlen, 51 ,inputpos -2) >= 0) {
		printf("You peer through the door's KEYHOLE, and you can barely\n"
			"make out through the darkness, a large foyer.\n");
		break;
	  }

	  // look ... (!through) [keyhole || lock]
	  printf ("It's an old fashioned KEYHOLE located in the tarnished door\n"
		  "fitting.  The key that fits in it is probably pretty large.\n");
	  return 0;
	}

	// look ... verandah
	if (cmdtest[0] == 11) {
	  printf("The long VERANDAH stretches the length of the mansion.  The\n"
		 "wooden railing's faded paint is peeling off, and an eerie\n"
		 "breeze disturbs the leaves littering the verandah.\n");
	  return 0;
	}
	// This place is unreachable.
	printf("Oops!\n");
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// North
			// if the door is open:
			if (OBJST[PLCCD][0] == 0) {
				change_location(2);
				return 0;
			}
			// If it's closed:
			printf("You attempt to enter the mansion, and promptly plant\n"
				"your face on the closed door.\n");
			return 0;
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	  	switch (cmdtest[0]) {
	  	  // open ... door
	  	  case 0: {
			if (OBJST[PLCCD][0] == 0) { // if door is open:
				printf("The door is already open.\n");
				return 0;
			}

	  		// if the door is unlocked, open it:
	  		if (OBJST[PLCCD][4] == 0) {
	  			OBJST[PLCCD][0] = 0;
	  			printf("The door opens with a creak.\n");
	  			return 0;
	  		}
	  		
			// If it's locked, say so.
			printf ("You turn the door handle, and give a mighty heave,\n"
				"But the door will not budge.  It's either stuck fast\n"
				"with rust and dirt, or it's locked.\n");
	  		return 0;
		  }
	  	}
	// open ... ???
	printf("That doesn't open.");

	return 0;
    }

    //  Perform action code 3 or use.

    case 3: {
	switch (cmdtest[0]) {
	    case 0: {  // Use...key...
		// if you don't even have the key

		if (cmdtest[1] < 0) {
		    printf("What do you want to use the KEY with?\n");
		    return 0;
		}
		// Use ... key ... door
		if (cmdtest[1] == 1 || cmdtest[1] == 2) {
		    int dmytst[TESTNUM] = {0,0,1};
		    place_verandah(cmdlen, 8, dmytst, 0);
		    return 0;
		}
	    }
	}
    }

    //  Miscellaneous commands

    case 8: {
	switch (cmdtest[TESTNUM -1]) {

	    case 0: {	// Lift.

		switch (cmdtest[0]) {
		    case 0: {	// lift mat

			//  If the key is under the mat:
			if (OBJST[PLCCD][3] == 0) {
			    INV[2] = 0;
			    OBJST[PLCCD][3] = 1;
			    printf("You lift up the mat, and you find a KEY under it!\n"
				   "You put the KEY in your pocket.\n");

			   return 0;
			}

			// if not...
			printf("There is nothing else under the door MAT.\n");
			return 0;
		    }
		    default:  printf("That would exceed your lifting capacity!");
		    return 0;
		}

	    }
	    case 1: {  //  Unlock
		switch (cmdtest[0]) {
		    case 0: {  //  unlock door

			// if it's already unlocked:
			if (OBJST[PLCCD][4] == 0) {
			    printf("The DOOR is already unlocked.\n");
			    return 0;
			}

			// if you've got the key, unlock door, and remove key from INV.
			if (INV[2] == 0) {
			    OBJST[PLCCD][4] = 0;
			    INV[2] = 1;

			    printf("You turn the Key in the door lock, and you hear\n"
				    "a click.\n");
			    return 0;
			}

			// Otherwise...
			printf("How are you planning on doing that without a key?\n");
			return 0;
		    }
			// Return for witty repartee
			return -2;
		}
	    }

	}
    }

    // The program should not reach here.  
    default: 
	printf ("Control has reached end of function: place_verandah\n");
  }

  return 0;
}


