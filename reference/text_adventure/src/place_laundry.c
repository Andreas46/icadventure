/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the laundry, 			*/
/*	Place_laundry, and location 7		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_laundry.h"

int place_laundry(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "laundry";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // Print description with available objects.
	  printf("Your standing in a small laundry.  The only furnishings consist of a bench\n"
	  "housing a sink, and an old set of mangles bolted to one end.");
	  if (OBJST[PLCCD][0] == 0) {	// if the mop is obtainable:
	  	printf("  There is also an old MOP\n"
		"in the corner.");
	  }
	  printf("\n\nThe kitchen is back to the WEST, and there's a door leading to the backyard, NORTH.\n");
	
	  return 0;
	}

	// look ... sink
	if (cmdtest[0] == 1) {
	  printf ("it's a deep laundry tub.\n");
	  return 0;
	}

	// look ... mangle  || mangles
	if (cmdtest[0] == 2 || cmdtest[0] == 3) {
	  printf ("They're an old crank style set of mangles, used to ring out washing.\n");
	  return 0;
	}
	// look ... bench
	if (cmdtest[0] == 4) {
		printf ("It's a worn wooden bench occupying most of the eastern wall of the laundry.\n"
		"Presumably for assisting in the washing of clothes.\n");
		return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// North
			change_location(8);
			return 0;			
		}
		case 1: {	// West
			change_location(6);
			return 0;
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {	// Not used.
	// Should not reach here
	printf("Control has reached end of \"Open\" in function: place_%s\n",funcloc);
	return -1;
    }

    //  Perform action code 3 or use.

    case 3: {
	if (cmdtest[0] == 0 || cmdtest[0] == 1) {
		if (cmdlen < 3) {  // use mangles on their own
			printf("You clothes don't need ringing out.\n");
			return 0;
		}
		// otherwise...
		printf("That can't be run through the mangles.\n");
		return 0;
	}

	printf("Control has reached end of \"Use\" in function: place_%s\n",funcloc);
	return -1;

    }

    //  Miscellaneous commands

    case 8: {	// not used.
	    printf("Control has reached end of \"Misc\" in function: place_%s\n",funcloc);
	    return -1;
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

