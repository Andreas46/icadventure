/************************************************/
/*						*/
/*  inv_ctrl.c					*/
/*						*/
/*  This file is containes all the functions	*/
/*  relating to the manipulation, and managment	*/
/*  of inventory items.				*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "inv_ctrl.h"
#include "txtadv1.h"
#include "place_yard.h"


// Inventory of obtainable objects (0 = have, 1 = don't have).
// 0 broom, 1 doorstop, 2 Front door key, 3 Moustache, 4 gum, 5 salt, 6 mop, 7 paperclip, 8 crowbar, 9 hacksaw, 10 mop head, 11 mop handle.
short INV[INVLEN] = {0,1,1,1,0,0,0,0,1,1,0,0};


// The state of objects for each place, and it's initilisation.
// Each array's reference is equal to the locations place code.
// 0 = true (open, unlocked, avaiable, etc), 1 = false (closed, gone, locked, etc).

short OBJST[NUMOFPLACES][MAXOBJ] =
{
{0,0,0,0,0,0},	// Unused.
{1,0,0,0,1,0},	// 1 Verandah: 0 door, 1 broom, 2 doorstop, 3 key, 4 door lock.
{1,1,0,0,0,0},	// 2 Foyer: 0 cellar door, 1 kicked door.
{0,0,0,0,0,0},  // 3 Hall 1:
{1,1,0,0,0,0},	// 4 Library: 0 bust has hair, 1 mustache.
{0,0,0,0,0,0},  // 5 Living room:
{0,0,0,0,0,0},	// 6 kitchen: 0 salt, 1 Gum.
{0,0,0,0,0,0},  // 7 Laundry: 0 mop.
{1,0,0,0,0,0},	// 8 Backyard: 0 door unlocked.
{0,0,0,0,0,0},	// 9 Garden Shed: 0 crowbar, 1 hacksaw
};

//{0,0,0,1,1,1},  // Study: 0 paper clip, 1 penguin, 2 beads, 3 shelf, 4 safe, 5 j-box.


// Obtain an object, and put it in your inventory.

int get_itm(int cmdlen, int cmdtest)

{
  switch (PLCCD) {
     case 1: {	// items on the verandah
	// get ... broom (from verandah)
	if (cmdtest == 0) {
		if (OBJST[PLCCD][1] == 0) {
			// Add broom to inventory, and take it
			// out of the verandah.
			INV[0] = 0;
			OBJST[PLCCD][1] = 1;

			printf("Obtained broom!\n");
			return 0;
		}

		if (INV[0] == 0) {
			printf("You've already got the broom!\n");
			return 0;
		}
		return 1;
	}

	// get ... doorstop (from verandah)
	if (cmdtest == 1 || cmdtest == 2) {
		if (OBJST[PLCCD][2] == 0) {
			// Add doorstop to inventory, and take it
			// out of the verandah.
			INV[1] = 0;
			OBJST[PLCCD][2] = 1;

			printf("Obtained doorstop!\n");
			return 0;
		}

		if (INV[1] == 0) {
			printf("You've already got the doorstop!\n");
			return 0;
		}
		return 1;
	}
	// get ... verandah 
	if (cmdtest == 3) {
		printf("Even though this is an adventure game, not even YOUR pockets\n"
			"are large enough to put the front half of the mansion in.\n");
	}
    }

    case 4: {	// Items from the Library
	// get ... moustache || mo (from library)
	if (cmdtest == 0 || cmdtest == 1) {
		if (OBJST[PLCCD][1] == 0) {
			// Add mo to inventory, and take it
			// out of the library.
			INV[3] = 0;
			OBJST[PLCCD][1] = 1;

			printf("You pocket the plasta mo.  You never now when a cast of someone's facial\n"
			"hair might come it handy.\n");
			return 0;
		}

		if (INV[3] == 0) {
			printf("the moustache is already in your pocket.\n");
			return 0;
		}
		return 1;
	}
    }
    case 6: { // items from kitchen
	if (cmdtest == 0) {	// get ... utensils
		printf("Nah, you have no need for lumps of iron-oxide.\n");
		return 0;
	}
	if (cmdtest == 1) {	// get ... soylent (green)
		printf("You reach into the box for a refreshing snack of soylent green, but\n"
		"instead of retrieving a handfull of green wafers, you pull out a handlful of rat\n"
		"droppings.  Eww.  Looks like the rats beat you too them.\n");
		return 0;
	}
	if (cmdtest == 2) {	// get ... droppings
		printf("You do NOT need to carry around a pocket full of rat dropping!  ...Besides, I\n"
		"couldn't think of a puzzle that involved their use. :P\n");
		return 0;
	}
	if (cmdtest == 3) {	// get ... gum
		if (OBJST[PLCCD][1] == 0) {
			// Add bubble gum to inventory, and remove if from the kitchen.
			INV[4] = 0;
			OBJST[PLCCD][4] = 1;

			printf("You take the wad of Big Pink from the shelf, and stuff it in your pocket.\n");
			return 0;
		}

		if (INV[4] == 0) {
			printf("You've already taken the gum.\n");
			return 0;
		}
		return 1;
	}

	if (cmdtest == 4) {	// get ... salt
		if (OBJST[PLCCD][0] == 0) {
			// Add salt to inventory, and remove if from the kitchen.
			INV[5] = 0;
			OBJST[PLCCD][0] = 1;

			printf("The big jar of salt fits easily into the pockets of your trusty adventure gamer's\n"
			"pants(tm).\n");
			return 0;
		}

		if (INV[5] == 0) {
			printf("Aren't your arteries going to suffer enough with the ONE jar of salt?\n");
			return 0;
		}
		return 1;
	}
    }
    case 7: {	// laundry
	if (cmdtest == 0) {	// get mop
		if (OBJST[PLCCD][0] == 0) {
			// Add mop to inventory, and remove if from the laundry.
			INV[6] = 0;
			OBJST[PLCCD][0] = 1;

			printf("You pick up the mop.  It doesn't even take up much room in your pockets.\n");
			return 0;
		}

		if (INV[6] == 0) {
			printf("You've already got one mot, you don't need another.  Your not Roger Wilco.\n");
			return 0;
		}

	}
    }
    case 8: {	// items for yard
	switch (cmdtest) {
	  case 0:	// get lock
	  case 1: {	// get padlock
	    if (OBJST[PLCCD][0] == 1) {	// if the door is locked
		printf("Yeah, the lock is insecure, but it's not THAT insecure.\n");
		return 0;
	    }
	    // call "look ... lock || padlock"
	    int dmytst[TESTNUM] = {3,-1,-1};
	    place_yard(cmdlen, 0, dmytst, 0);
	    return 0;
	  }
	}
    }
    case 9: {	// Items from the garden shed
	switch (cmdtest) {
	  case 0:	// get hammer
	  case 1: {	// get pincers
		printf("That tool is Well beyond it's service life, and isn't going to be of any use to you.\n");
		return 0;
	  }
	  case 2: {	// get dust
		printf("You've already got plety of dust in your lungs, in your hair, and on your cloths.  You \n"
		"don't need it in your pockets as well.\n");
		return 0;
	  }
	  case 3: {	// get crowbar
		printf("Yes! A crowbar, the most versitile of all obtainable adventure game items!  ...Well\n"
		"most of one anyway, but I assure you, it's every bit as versitle, and now it's all yours!\n");
		INV[8] = 0;
		OBJST[PLCCD][0] = 1;
		return 0;
	  }
	  case 4: {	// get hacksaw
		printf("You pick up the hacksaw.");
		INV[9] = 0;
		OBJST[PLCCD][1] = 1;
		return 0;
	  }
	}
    }


  }
	printf("Control has reached end of function \"get_itm\".\n");	
	return -1;
}

//  Fuction to give a description of obtainable items.

int inv_look(int cmdlen, int cmdtest)

{
  switch (cmdtest) {
    case 0: {	// look ... broom
	if (INV[0] == 0 || (OBJST[PLCCD][1] == 0 && PLCCD == 1)) {
	  printf("The BROOM looks as though it was fashoned by hand, and like\n"
	  "everything else on the verandah, old and dirtly.\n");
	  return 0;
	}
	break;
    }

    case 1:	// look ... door stop
    case 2: {	// look ... cat & your able to see it.
	if (INV[1] == 0 || (OBJST[PLCCD][2] == 0 && PLCCD == 1)) {
	  printf ("The old DOORSTOP appears to be a curled up cat, carved out\n"
	  "of stone.  It's suprisingly lifelike, despide being covered\n"
	  "in moss.\n");
	  return 0;
	}
	break;
    }

    case 3: {
	// look ... key
	if(INV[2] == 0) {
		printf("Its and old Brass KEY you found under the door mat on the\n"
		"front verandah.  Considering it's size, it's a wonder how\n"
		"anyone would think it would go unnoticed under the door mat.\n");
		return 0;
	}
	break;
    }

    case 4:	// look ... moustache
    case 5: {	// look ... mo 
	if (INV[3] == 0 || (OBJST[PLCCD][1] == 0 && PLCCD == 4)) { 
	  printf ("It's a plaster moustache that fell off the bust when you gave it a wig.  I guess\n"
	  "the bust didn't need it anymore with all that new hair you give him.\n");
	  return 0;
	}
	break;
    }

    case 6: {	// look ... salt
	if (INV[5] == 0 || (PLCCD == 6 && OBJST[PLCCD][0] == 0)) {
		printf("It's a large, sealed glass jar, filled to the brim with iodized salt.\n");
		return 0;
	}
	break;
    }
    case 7: {	// look ... gum
	if (INV[4] == 0 || (PLCCD == 6 && OBJST[PLCCD][1] == 0)) {
		printf("It's a box of pink chewing gum ...or at least it was a box.  The cardboard packaging\n"
		"appeares to have been fodder for rodents over the years, makeing it more of a wad, than\n"
		"a box of gum.  Strangily, the gum inside hasn't been touched.\n");
		return 0;
	}
	break;
    }
    case 8: {	// look ... mop
	// original mop
	if (INV[6] == 0 || (PLCCD == 7 && OBJST[PLCCD][0] == 0)) {
		printf("It's a dirty mop.\n");
		return 0;
	}
	// Sawn off mop
	if (INV[10] == 0) {
		printf("It's head of a dirty old mop, which has had the handle sawn off.\n");
		return 0;
	}
	break;
    }
    case 9: {	// look ... paperclip
	//if (INV[7] == 0 || (PLCCD == 7 && OBJST[PLCCD][0] == 0)) {
		printf("It's an oversized, copper paperclip.\n");
		return 0;
	//}
	break;
    }
    case 10: {	// look ... crowbar
	if (INV[8] == 0 || (PLCCD == 9 && OBJST[PLCCD][0] == 0)) {
		printf("It's about 2/3 of a sturdy crowbar.\n");
		return 0;
	}
	break;
    }
    case 11: {  // look ... hacksaw
	if (INV[9] == 0 || (PLCCD == 9 && OBJST[PLCCD][1] == 0)) {
		printf("The blade is a little dull, but it should still be able to cut through softer\n"
		"materials.  It's in pretty good condition considering it's stamped wiith 1337.  Now /thats/ vintage.\n");
		return 0;
	}
	break;
    }
    case 12: {	// look ... [mop] handle
	if (INV[11] == 0) {
		printf("It's a long wooden handle which has been seperated from a mop.\n");
		return 0;
	}
	break;
    }

  }

  // Nothing in inventory to look at.
  printf("You can not look at that.\n");
  return 1;

}

int inv_use(int *cmdtest)
{
	// use ... hacksaw ... mop && you have both in inventory
	if (cmdtest[0] == 0 && cmdtest[1] == 1 && INV[6] == 0 && INV[9] == 0) {	
		printf("You take the hacksaw and saw the handle off the mop.  You now have the head of the MOP, and\n"
		"the mop HANDLE.\n");
		INV[6] = 1;	// mop be gone.
		INV[10] = 0;	// grant mop head.
		INV[11] = 0;	// grant mop handle.

		return 0;
		
	}
	return -1;
}

// Function to print your inventory.

int inv_print(int cmdlen)
{
	int i, j, avinvnum = 0, allocsize = 0;
	char *invttl;
	char *invitms[INVLEN] = INVITMS;

	// Get number of obtained inventory items:
	for (i = 0; i < INVLEN; i++) {
		if (INV[i] == 0) {
			avinvnum++;
			allocsize += (strlen(invitms[i]) + 2);
		}
	}

	// If your carring nothing, say so.
	if (avinvnum == 0) {
		MALLOC(16 * sizeof(char), invttl)
		strcpy(invttl, "Nothing at all.");
	} else {


		// Otherwise compile a list of obtained inventory items.
		if (avinvnum > 1) {
			allocsize += 4;
		}
		MALLOC(allocsize * sizeof(char), invttl)
		invttl[0] = '\0';	// This aught to stop those troublesome memory related crashes!

//		MALLOC(1 * sizeof(char), invttl)
		for (j = 0; j < INVLEN; j++) {
			if (INV[j] == 0) {
				strcat(invttl, invitms[j]);

				// Add approptiate punctuation.
				lst_punct(avinvnum,invttl);
				avinvnum--;
			}
		}
		//  First letter of First item to uppercase:
		invttl[0] = toupper(invttl[0]);
	}

	printf ("You are carring: %s\n", invttl);
	free(invttl);
	
	return 0;
}



// Available objects are formatted, processed, and compiled into a string.

char *av_itms(int objnum, int objel[], int invel[], char *avobj)
{
	int i, j, avobjnum = 0, allocsize = 1;
	char *locobj[INVLEN] = INVITMS;  // INVITMS is a macro.

	//  Get number of available objects
	for (i = 0; i < objnum; i++) {
		if (OBJST[PLCCD][objel[i]] == 0) {
			avobjnum++;
			// determine memory needed for string:
			allocsize += strlen(locobj[objel[i]]);
			switch (avobjnum) {
				case 0 : allocsize += 1;
					break;
				case 1 : allocsize += 6;
					break;
				default: allocsize += 2;
			}
		}
	}

	// if there are no available items:
	if (avobjnum == 0) {
		MALLOC(35,avobj);
		strcpy (avobj, "Nothing you can put in your pants.");
		return avobj;
	}

	// allocate memory for available objects
	MALLOC (allocsize, avobj);

	// Assemble avobj with available objects, and punctuation.
	for (i = 0, j = avobjnum; i < objnum; i++, j--) {
		// If available, cat it into the list of avilable objects + punctuate.
		if (OBJST[PLCCD][objel[i]] == 0) {
			strcat(avobj, locobj[invel[i]]);
			lst_punct(j,avobj);
		}
	}
	avobj[0] = toupper(avobj[0]);
	return avobj;
}

// Function to add appropriate punctuation to a list.

int lst_punct(int lstnum, char *lstary)
{
 	switch (lstnum) {
		case 1: strcat(lstary, ".");
			break;
		case 2: strcat(lstary, ", and ");
			break;
		default: if (lstnum > 2) {
			strcat(lstary, ", ");
		}
	}
	return 0;
}
