// current/starting location
extern short PLCCD;

//  Number of items for CMDTEST
#define TESTNUM 3

// malloc with error checking.
#define MALLOC(size,ptr) \
if ((ptr = malloc((size))) == '\0') { \
	printf("Failed to allocate memory!\n");	\
 	exit(1);	\
}

/****Function Prototypesl****/

int change_location(int);
int game_over(int);
int witty_remark(int, int*);
