/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the Library, 			*/
/*	Place_library, and location 4		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_library.h"

int place_library (int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "library";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // compile a list of available items.
	  int objel[] = {1}, invel[] = {3};
	  char *avobj;

	  avobj = av_itms(1, objel, invel, avobj);

	  // Print description with available objects.
	  printf("Your standing in the entrance of what must have once been a library.  several of the\n"
		"windows are open, which must have let many a storm into this room.  The place\n"
		"is littered with leaves and fallen books, and the floor is a patchwork of mold and mildue. \n"
		"The shelves of books seem to have been mostly destroyed by the elements, or strewn \n"
		"across the room.  The only undamaged item in here appears to be a BUST of a bald man\n"
		"located in an alacove near the entrance.\n\n"

		"The hallway you entered from was to the EAST.\n\n");

	  printf("Near you, you can see: %s\n",avobj);

	  return 0;
	}

	// look ... Window || windows
	if (cmdtest[0] == 1 || cmdtest[0] == 2) {
	  printf ("They must have blown open a long time ago to allow this much damage to the room.\n");
	  return 0;
	}

	// look ... book || books
	if (cmdtest[0] == 3 || cmdtest[0] == 4) {
	  printf ("Those that haven't been torn and scattered around the room by the wind, have been\n"
		"water logged by rain, making them illegible.\n");
	  return 0;
	}

	// look ... bust
	if (cmdtest[0] == 5) {
	  // if the bust has no hair:
	  if (OBJST[PLCCD][0] == 1) {
	    printf ("It's a plaster bust of a bald, middle aged man, with a bushy mustache.\n"
		    "He almost looks a little bit sad without any hair.\n");
	    return 0;
	  }
	  // if he does:
	  printf("It's a plaster bust with a toupee of natty bleached dreads, courtesy of the mop head you\n"
		"fixed him up with.\n");
	  return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// east
				change_location(3);  // hall 1
				return 0;
			}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	printf ("control has reached end of \"move\" in place_%s.\n",funcloc);
	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	// not used

	// Should not reach here
	printf("Control has reached end of \"Open\" in function: place_%s\n",funcloc);
	return -1;
    }

    //  Perform action code 3 or use.

    case 3: {
	switch (cmdtest[0]) {
	    case 0: {  // Use...mop...
		if (cmdtest[1] < 0) {
		    printf("there isn't a mop made that could clean up this mess.\n");
		    return 0;
		}
		// Use ... mop ... bust
		if (cmdtest[1] == 1) {
		    int dmytst[TESTNUM] = {0,0,0};
		    return place_library(cmdlen, 8, dmytst, 0);
		}
	    }
	    default: {
	    	// use bust, etc.
	    	return -2;	// return for witty repartee
	    }
	}
	printf("Control has reached end of \"Use\" in function: place_%s\n",funcloc);
	return -1;

    }

    //  Miscellaneous commands

    case 8: {
	switch (cmdtest[TESTNUM -1]) {
	    case 0: {	// put
		switch (cmdtest[0]) {
		    case 0: {	// put ... wig ...
			//  If bust is bald, put hair on him, and make mustache available
			if (OBJST[PLCCD][0] == 1) {


			      	if (INV[10] == 0) {  //if we have the mop head
				  OBJST[PLCCD][0] = 0;
				  OBJST[PLCCD][1] = 0;
				  printf("You put the mop head on the bust.  He looks a lot happier now with... Oh dear.\n"
				  "You must have been a bit too rough, since his mustache has just fallen off.\n");
				  return 0;
			      	}
				if (INV[6] == 0) {  // If we have the original mop:
				  printf("Hmmm...  Mabye a nice mop toupe would cheer him up, but it doesn't look very real\n"
				  "with the handle sticking out of the mop.  Mabye you could find some way to remove it?\n");
				  return 0;
				}
				// return for witty repartee
				return -2;
			}
			// if not...
			printf("You've already done that.  Leave the poor bust alone, already.\n");
			return 0;
		    }
		    // return for witty repartee:
		    return 2;
		}
	    }

	    case 1: {  // talk
		switch (cmdtest[0]) {
		  case 0: {	// talk ... bust...
			printf("The bust just stairs back at you blankly.\n");
			return 0;
		  }
		  default: {
			printf ("You can't talk to that.\n");
			return 0;
		  }
		}
	    }
	}
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

