/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the kitchen, 			*/
/*	Place_kitchen, and location 6		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_kitchen.h"

int place_kitchen(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "kitchen";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // compile a list of available items.
	  int objel[] = {0,1}, invel[] = {4,5};
	  char *avobj;

	  avobj = av_itms(2, objel, invel, avobj);

	  // Print description with available objects.
	  printf("You are standing in the mansion's kitchen.  The decor consisting of mouldy tiled\n"
	"walls, floors, and rusty iron utensils, make a nice change to the usual of just\n"
	"mould.  Some of the late afternoon sun has managed to penetrate the filthy\n"
	"windows on the southern wall, and the light thrown off the rust encrusted wood\n"
	"oven, and utensils hanging near the window, is casting the room a muddy orange.\n\n"

	"There are some rotting SHELVES on the northen wall holding some rotting\n"
	"ITEMS, probably containing rotten food. The foyer is back to the WEST, and the\n"
	"kitchen gives access to the laundry, EAST.\n\n");

	  printf("On the shelves, you can see: %s\n",avobj);
	
	  free(avobj);
	  return 0;
	}

	// look ... oven
	if (cmdtest[0] == 1) {
	  printf ("It's an old wood fired oven, made from cast iron.  It's more rust than iron,\n"
	  "now, though.\n");
	  return 0;
	}

	// look ... utensils
	if (cmdtest[0] == 2 ) {
	  printf ("There are some rust coated pots, and an assortment of rusty cooking utilities.\n"
	 "Nothing you need, and besides, they're so rusty, they would likely dissolve if you put\n"
	 "them near any liquid.\n");
	  return 0;
	}

	// look ... window || windows
	if (cmdtest[0] == 3 || cmdtest[0] == 4) {
	  printf("They are slightly less dirty that most of the other's you've seen so far.\n"
	  "They're still disgusting though.\n");
	  return 0;
	}

	// look ... (shelves || shelf || items)
	if (cmdtest[0] == 5 || cmdtest[0] == 6 || cmdtest[0] == 7) {
  	  // compile a list of available items.
	  int objel[] = {0,1}, invel[] = {4,5};
	  char *avobj;

	  avobj = av_itms(2, objel, invel, avobj);

	  printf("There isn't much left on the shelves of the kitchen. You can make out:\n"
	  "%s\n"
	  "A closer inspection also reveals a box of soylent green.\n",avobj);
	  return 0;
	}

	// look ... soylent (green)
	if (cmdtest[0] == 8) {
	  printf("It's your favourite green wafers!  Their packed full of the planktony\n"
	  "goodness a person needs (and another had).  They sure beat the pants of any\n"
	  "soylent steak, thats for sure.\n");
	  return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// East - to laundry
			change_location(7);
			return 0;
		}
		case 1:	{	// West - to foyer
			change_location(2);
			return 0;
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	// not used
	printf("Control has reached end of \"open\" in function: place_%s\n",funcloc);
	return -1;
    }

    //  Perform action code 3 or use.

    case 3: {
	//not used
	printf("Control has reached end of \"Use\" in function: place_%s\n",funcloc);
	return -1;

    }

    //  Miscellaneous commands

    case 8: {  // not used
	printf("Control has reached end of \"Misc\" in function: place_%s\n",funcloc);
	return -1;
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

