/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the foyer, location 2		*/
/*						*/
/************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_foyer.h"

int place_foyer(int cmdlen, int actioncode,int cmdtest[TESTNUM],int inputpos)

{

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", or just "look":
	if (cmdtest[0] == 0) {

	  // Print description with available objects.
	  printf("You are standing in the foyer of the old mansion.  The walls\n"
	  "are adorned with pealing wall paper and an old PORTRATE\n"
	  "on the western wall.\n"
	  "A hallway continues on to the NORTH, There appears to be  a \n"
	  "kitchen to the EAST, as well as a closed DOOR to the WEST, and\n"
	  "a stair case leading UP. SOUTH leads back to the verandah.\n");
	  return 0;
	}

	// look ... foyer
	if (cmdtest[0] == 1) {
	  printf ("It's very rustic.\n");
	  return 0;
	}

	// look ... portrate
	if (cmdtest[0] == 2 || cmdtest[0] == 3) {
	  printf ("The large PORTRATE is of a middle aged man.  You can almost feel\n"
		"His piercing stare.  When you look closely, there seems to be\n"
		"a slight depression above the mans upper lip.\n");
	  return 0;
	}

	// look ... door
	if ((cmdtest[0] == 4)) {
	  printf ("The heavy door on the western wall seems to have held up rather\n"
		"well considering the state of everything else in the mansion.  For\n"
		"for some reason, there isn't any door handle on the door.\n");
	  return 0;
	}

	// look ... [stair || stairs]
	if (cmdtest[0] == 5 || cmdtest[0] == 6) {
	  printf ("They lead up to the first landing.\n");
	  return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in place_foyer\n");
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// North
			change_location(3);	// To hall
			return 0;
		}

		case 1: {	// South
			change_location(1);	// To verandah.
			return 0;
		}

		case 2: {	// East
			change_location(6);	// To kitchen
			return 0;
		}

		case 3: {	// West
			// if the door is open:
			if (OBJST[PLCCD][0] == 0) {
				change_location(0);	// To mystory room.
				return 0;
			}
			// If it's closed:
			printf("You can't.  There's a door blocking your way.\n");
			return 0;
		}
	}

	// Everywhere else.
	printf ("Can't move there.\n");

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	  	switch (cmdtest[0]) {
	  	  // open ... door
	  	  case 0: {
			if (OBJST[PLCCD][0] == 0) { // if door is open:
				printf("The door is open.  All you need to do is walk through it.\n");
				return 0;
			}

	  		// If it's closed, say so.
			printf ("The door has no handle.  How are you going to open it?\n");
	  		return 0;
		  }
	  	}
	// open ... ???
	printf("That doesn't open.");

	return 0;
    }

    //  Perform action code 3 or use.

    case 3: {
	switch (cmdtest[0]) {
	    case 0: 
	    case 1: {  // Use ... (mo || moustache ) ...

		switch (cmdtest[1]) {
		    case 0:
		    case 1: { // use ... (mo || moustache) ... (painting || portrate)
		      // if you have the mo...
		      if (INV[3] == 0) {
			INV[3] = 1;
			OBJST[PLCCD][0] = 0;
			printf("Feeling the need to deface the portrate, but not being in possession of a felt \n"
			"marker, you take the next, logical course of action, and attempt to place the \n"
			"plaster moustache on the painting.  To your supprise, it fit's perfectly\n"
			"into the depression above the mans upper lip, and the door next to it swings open.\n");
			return 0;
		      }
		   }
		   default: {
			if (INV[3] == 1) {
				// return for witty repartee.
				return -2;
			}
			printf("You can't use the MO with that.\n");
			return 0;
		   }
		}
	    }
	}
    }

    //  Miscellaneous commands

    case 8: {
	switch (cmdtest[TESTNUM -1]) {

	    case 0: {	// kick

		switch (cmdtest[0]) {
		    case 0: {	// kick ... door

			//  If door is closed, and it hasn't been kicked:
			if (OBJST[PLCCD][0] == 1 && OBJST[PLCCD][1] == 1) {
			    OBJST[PLCCD][1] = 0;  //  Log attempt on door.

			    printf("You take a few step back, and deftly execute a mid-air spinning round kick on the door... \n"
			   "but the only damage you achieve, is to your pride, and your foot.\n");
			   return 0;
			}

			// if the door is open:
			if (OBJST[PLCCD][0] == 0) {
				printf("You get a running start to kick down the door, and as you sail past the threshold, and\n"
				"over the steep stairs towards the hard hard floor below, you remember that you had\n"
				"already opened that door.\n");

				game_over(cmdlen);
				return 1;
			}

			// If the door's closed and you've already tried that...
			printf("No, you learned your lesson the first time... that REALLY hurt!\n");
			return 0;
		    }
		    	// return for witty repartee
			return -2;
		}

	    }
	    case 1: {  //  Unlock
		switch (cmdtest[0]) {
		    case 0: {  //  unlock door

			// if it's already unlocked:
			if (OBJST[PLCCD][4] == 0) {
			    printf("The DOOR is already unlocked.\n");
			    return 0;
			}

			// if you've got the key, unlock door, and remove key from INV.
			if (INV[2] == 0) {
			    OBJST[PLCCD][4] = 0;
			    INV[2] = 1;

			    printf("You turn the Key in the door lock, and you hear\n"
				    "a click.\n");
			    return 0;
			}

			// Otherwise...
			printf("Thats not going to unlock the door.\n");
			return 0;
		    }
			// return for witty repartee
			return -2;
		}
	    }

	}
    }

    // The program should not reach here.  
    default: 
	printf ("Control has reached end of function: place_foyer\n");
  }

  return 0;
}
