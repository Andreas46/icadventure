int get_itm(int, int);
int inv_look(int, int);
int inv_use(int*);
int inv_print(int);
char *av_itms(int, int*, int*, char*);
int lst_punct(int, char*);

// Inventory of obtainable objects (0 = have, 1 = don't have).
// 0 broom, 1 doorstop, 2 Front door key.
#define INVLEN 12
extern short INV[INVLEN];

#define INVITMS {"a dusty BROOM","an old DOORSTOP","a brass KEY","a plaster MOUSTACHE","a big wad of GUM","a large jar of SALT","a dirty MOP","a large PAPERCLIP", "most of a CROWBAR", "a rusty HACKSAW","The head of a MOP", "the HANDLE from a mop"}

// The state of objects for each place, and it's initialisation.
// Each array's reference is equal to the locations place code.
// 0 = true (open, unlocked, available, etc), 1 = false (closed, gone, locked, etc).
#define NUMOFPLACES 10
#define MAXOBJ 6

extern short OBJST[NUMOFPLACES][MAXOBJ];

// be gone, abitrary string restraints!
// buffer size for string of objects, usually for adding punctuation
// #define LSTBUFF 64
