/************************************************/
/*						*/
/*	This is a function for processing a	*/
/*	specific location within the game.	*/
/*						*/
/*	This is the Garden shed,		*/
/*	Place_shed, and location 9		*/
/*						*/
/************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "txtadv1.h"
#include "inv_ctrl.h"
#include "place_shed.h"

int place_shed(int cmdlen, int actioncode, int cmdtest[TESTNUM], int inputpos)
{
  char* funcloc = "shed";

  switch (actioncode) {
	
    // perform action for "actioncode 0" or "look". 

    case 0: { 
	// If the user entered "look around", "look shed", or just "look":
	if (cmdtest[0] == 0 || cmdtest[0] == 1) {

	  // Print description with available objects.
	  printf("You're entrance to the small tool shed disturbs the thick layer of dust coating the heavy\n"
	  "work bench, and every other surface that's not completely vertical. There is what\n"
	  "appears to be an assortment of broken TOOLS under the thick carpet of dust covering\n"
	  "the bench.\n\n"
	  "You can exit back to the back yard SOUTH.\n");

	  return 0;
	}

	// look ... tools
	if (cmdtest[0] == 2) {

	  // compile a list of available items.
	  int objel[] = {0,1}, invel[] = {8,9};
	  char *avobj;

	  avobj = av_itms(2, objel, invel, avobj);

	  printf ("You risk a fatal asthma attack by sweeping the worst of the dust of the bench to get a\n"
	  "better look at the tools.\n"
	  "The dust cloud clears, and you can make out a hammer with a broken handle, and a pair of\n"
	  "pincers that appear to have seized up.\n\n"

	  "Also amongst the tools, you can see: %s\n", avobj);
	  free(avobj);
	  return 0;
	}

	// look ... bench
	if (cmdtest[0] == 3 ) {
	  printf ("It's heavy, worn, and adorned with broken tools, and dust.\n");
	  return 0;
	}

	// look ... dust
	if (cmdtest[0] == 4 ) {
	  printf ("It's sort of hard /not/ to look at the dust seeing how it's covering every surface,\n"
	  "and there's an unhealthy amount wafting through the air.\n");
	  return 0;
	}

	// look ... hammer
	if (cmdtest[0] == 5 ) {
	  printf ("It's handle has been snapped right through, so it's not very useful.\n");
	  return 0;
	}

	// look ... pincers
	if (cmdtest[0] == 6 ) {
	  printf ("They're just a lump of pincer shaped rust now, and of no use to you.\n");
	  return 0;
	}

	// This place is unreachable.
	printf("Control has reached end of \"look\" in function: %s\n", funcloc);
	return -1;
    }

    // perform action for "actioncode 1", or "move".

    case 1: {

	// Perform action if match was found.
	switch (cmdtest[0]) {
		case 0:	{	// South
			change_location(8);
			return 0;
		}

		default:	// Everything else.
			printf ("Can't move there.\n");
	}	

	return 0;
    }

    // Perform actioncode 2 or open.

    case 2: {
	// Should not reach here
	printf("Control has reached end of \"Open\" in function: place_%s\n",funcloc);
	return -1;
    }

    //  Perform action code 3 or use.

    case 3: {
	printf("Control has reached end of \"Use\" in function: place_%s\n",funcloc);
	return -1;

    }

    //  Miscellaneous commands

    case 8: {
	    printf("Control has reached end of \"Misc\" in function: place_%s\n",funcloc);
	    return -1;
    }

    // The program should not reach here.  
    default: 
	printf ("Invalid action code: %d\n",actioncode);
	return -1;
  }

  // And even more so here...
  printf ("Control has reached end of function: place_%s\n",funcloc);
  return -1;
}

