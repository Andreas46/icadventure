#pragma once

#include "Cue.h"
#include "GameObject.h"

class CueSetState: public Cue
{
public:
	CueSetState (void);
	~CueSetState (void);
	
	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	ATL::String objToChangeName;
	ATL::String newState;
	
	virtual void parseStoredCueString (void);
};

