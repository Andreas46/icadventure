#pragma once

#include "Cue.h"

class CuePopUpFromFile: public Cue
{
public:
	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	ATL::String textFilePath;
	int popUpWidth;
	bool centreJustified;

	virtual void parseStoredCueString (void);
};
