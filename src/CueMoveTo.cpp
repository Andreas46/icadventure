#include "CueMoveTo.h"

#include "DebugLog.h"
#include "GameManager.h"

CueMoveTo::CueMoveTo (void)
{
	//targetRoom = NULL;
}

CueMoveTo::~CueMoveTo (void)
{

}

bool CueMoveTo::execute (void)
{
	//return true;
	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	GameManager::getInstance ().moveToRoom (targetRoomName);

	return true;
}

ATL::String CueMoveTo::getName (void)
{
	return "move_to";
}

void CueMoveTo::parseStoredCueString (void)
{
	if (hasCueBeenParsed)
	{
		// Already done this...
		return;
	}

	DebugLog::assert (!unparsedCueParameters.empty(), "Can not parse move cue; empty defered string!");

	if (!GameManager::getInstance ().isThereARoomNamed (unparsedCueParameters))
	{
		ATL::String warnMsg;
		warnMsg << "CueMoveTo: No Room named '" << unparsedCueParameters << "'";
		DebugLog::log (warnMsg, DebugLog::LevelWarning);
	}

	targetRoomName = unparsedCueParameters;
	hasCueBeenParsed = true;
}
