#include "PlayerAction.h"

#include "DebugLog.h"

#include "GameManager.h"

StringMap_t PlayerAction::storyVariables;

int64_t findWordPosInString (const ATL::String& string,
							 const ATL::String& wordToFind,
							 const size_t startPos = 0)
{
	int64_t findPos;

	findPos = string.find (wordToFind, startPos);
	
	if (findPos >= 0)
	{
		// We found the string, but was it a full word?

		// Test for word bounds at start.
		if (findPos != 0 && // Word at beginning of string.
			string [(size_t) findPos - 1] != ' ') // Previous character to the word is a space.
		{
			// We found the string, but not as a discrete word.
			findPos = -1;
		}
		// Test for word bounds at end of word position.
		else if (findPos + wordToFind.length() != string.length() && // word at end?
			string [(size_t) findPos + wordToFind.length()] != ' ') // word followed by space?
		{
			// We found the string, but not as a discrete word.
			findPos = -1;
		}
	}
	
	return findPos;
}

//ATL::String findWordNumInString



// --- PlayerAction --- //

PlayerAction::PlayerAction (GameObject* parent)
{
	parentObject = parent;
}


PlayerAction::~PlayerAction (void)
{
}

void PlayerAction::addTarget (const ATL::String& target,
							  const ATL::String& parentObjCondition,
							  const ATL::String& returnText,
							  const ATL::String& failText,
							  CueList_t execActions)
{
	ActionTarget actionTarget;

	actionTarget.onExecActions = execActions;

	actionTarget.target = target;
	actionTarget.parentObjCondition = parentObjCondition;
	actionTarget.setReturnText (returnText);
	actionTarget.failText = failText;
	
	// Attach this action target.
	actionTargets.push_back (actionTarget);

// 	LOG ("Adding new target with '", (double) actionTarget.onExecActions.size() + "' actions to: " + actionTarget.target);
}

void PlayerAction::addTarget (ATL::String target, ATL::String returnText)
{
// 	ActionTarget actionTarget;
// 	
// 	actionTarget.target = target;
// 	actionTarget.setReturnText (returnText);
// 	
// 	actionTargets.push_back (actionTarget);
	addTarget (target, "", returnText);
}

void PlayerAction::addObjTarget (const ATL::String& returnText,
								 const ATL::String& parentObjCondition,
								 const ATL::String& failText,
								 CueList_t execActions)
{
	ActionTarget actionTarget;

	actionTarget.onExecActions = execActions;

	//actionTarget.target = target;
	actionTarget.parentObjCondition = parentObjCondition;
	actionTarget.setReturnText (returnText);
	actionTarget.failText = failText;

	NameList_t allObjNames = parentObject->getAllNames();
	NameList_t::iterator itr;

	// Attach target for all names this (parent) object is known as.
	for (itr = allObjNames.begin(); itr != allObjNames.end(); ++itr)
	{
		// We pass "ActionTargets" by value, so it's safe to modify the original.
		actionTarget.target = *itr;
		
		actionTargets.push_back (actionTarget);
	}
}

//std::pair <ATL::String, int> PlayerAction::extractVerb (const ATL::String& inString) const
//{
//	return std::pair <ATL::String, int> ();
//}

bool PlayerAction::attemptParseProperty (const ATL::String& property,
										 const ATL::String& value)
{
	if (property == actionName)
	{
		addTarget (parentObject->getName(), value);
		//addObjTarget (value);
		//DebugLog::log (ATL::String ("Adding look object: ") + actionName + " print text: " + value);
	}
	else if (property == actionName + "_state")
	{
		StringVector_t vals = splitValueString (value, 2);
		
		addTarget (parentObject->getName(), vals [0], vals[1], vals[2]);
		//addObjTarget (vals [1], vals[0]);
	}
	else if (property == actionName + "_fail")
	{
		//LOG ("Adding fail text:", value);
		setFailMessage (value);
	}
	else
	{
		return false;
	}
	
	return true;
}

ATL::String PlayerAction::getName (void) const
{
	return actionName;
}

ATL::String PlayerAction::getUid (void) const
{
	return actionUid;
}

void PlayerAction::setFailMessage (ATL::String message)
{
	badTargetFailString = message;
}

void PlayerAction::setStoryVariable (const ATL::String& varName, const ATL::String& varValue)
{
	LOGD ("Setting story var '", varName + "' to '" + varValue + "'.");

	storyVariables [varName] = varValue;
}

ATL::String PlayerAction::getStoryVariable (const ATL::String& varName)
{
	ASSERT (isStoryVarSet (varName) == true,
		"Can not get story var '" + varName + "' as it has not been set!");
	
	return storyVariables [varName];
}

bool PlayerAction::isStoryVarSet (const ATL::String& varName)
{
	if (storyVariables.count (varName) > 0)
	{
		// This  story variable has been set.
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String& PlayerAction::replaceAllStoryVarsIsStr (ATL::String& inStr)
{
	int64_t varPos = 0;
	ATL::String storyVar;

	varPos = inStr.find ("$", (size_t) varPos);

	while (varPos >= 0)
	{
		storyVar = inStr.getNextWord ((size_t) (varPos + 1));

		// Do we have this story var set?
		if (isStoryVarSet (storyVar))
		{
			LOG ("replacing var '$", storyVar + "' with text '" + getStoryVariable (storyVar) + "'.");
			// perform substitution!
			inStr.replace (ATL::String ("$") + storyVar, getStoryVariable (storyVar), true, (size_t) varPos);
		}
		// did we find something, but it's not a valid 
		else if (varPos >= 0)
		{
			// Not a set story var.  Just increment to once past the "$" and keep searching.
			++varPos;
		}

		varPos = inStr.find ("$", (size_t) varPos);
	}

	return inStr;
}

void PlayerAction::clearStoryVariables(void)
{
	storyVariables.clear();
}

ParseResult PlayerAction::extractVerbParams (ATL::String command)
{
	int64_t verbPos;
	ActionAliasList_t::iterator itr;
	ATL::String verb;
	ATL::String verbOperands;

	DebugLog::assert (!actionName.empty(), "Action has not had name set!");

	// Does command contain the name of this action?
	verbPos = command.findWordPosInString (actionName);
	verb = actionName;

	// If not, check for one of it's aliases.
	for (itr = actionAliases.begin(); itr != actionAliases.end () && verbPos < 0; ++itr)
	{
		verbPos = command.findWordPosInString (*itr);
		//verb = actionName;
		verb = *itr;
	}

	// If we still have not found the verb for this action, it is not to be found.
	if (verbPos < 0)
	{
		return ParseResult (false, "");
	}

	size_t operandStartPos = (size_t) verbPos + verb.length () + 1;
	if (operandStartPos < command.length())
	{
		verbOperands = command.subStr (operandStartPos);
	}

	LOGV ("Matched verb: ", actionName);

	// Set the found verb, and operands as a story variable.
	PlayerAction::setStoryVariable ("_last_verb_", verb);
	PlayerAction::setStoryVariable ("_last_objects_", verbOperands);
	PlayerAction::setStoryVariable ("_last_object_", verbOperands.getWordNumInString (-1));

	//return evaluateActionTargetsFromString (command.subStr ((size_t)verbPos + verb.length ()));
	return ParseResult (true, verbOperands, "", verb);
}

bool PlayerAction::evaluateTargetState (ActionTarget target,
										ATL::String& retText,
										ATL::String& failString,
										const ATL::String& verb)
{
	if (target.parentObjCondition.empty() ||  // Ignore empty conditions.
		parentObject->getState() == target.parentObjCondition)
	{
		LOG ("Executing cues: ", (double) target.onExecActions.size());
		// We've made a match.  Execute associated cues.
		target.executeActionCues();

		//return ParseResult (true, itr2->returnText);
		retText += target.getReturnText();

		return true;
	}
	else if (!target.failText.empty() && failString.empty())
	{
		LOG ("setting fail string: ", target.failText);
		failString = target.failText;
	}
	
	return false;
}

bool PlayerAction::evaluateTargetFromString (ActionTarget target,
											 const ATL::String& fromString,
											 ATL::String& retText,
											 ATL::String& failString,
											 const ATL::String& verb)
{
//	LOG ("Eval target:", target.target);

	// If this action's target can be found in "fromString"
	// following the verb's position in it.
	//if (target.isTargetNameInString (fromString.getWordNumInString (-1)) ||
	//if (target.isTargetNameInString (fromString) ||
	if (target.isTargetNameInString (fromString) ||
		// Is this a default (empty) target, and no parameters
		// we specified for this action?
		(target.target.empty() && fromString.empty())
	)
	{
		LOGD ("Matched target: ", target.target + " attached to: " +
			parentObject->getName());

		// We're trying to operate on "target". Are all conditions met to
		// allow us to do so?
		if (evaluateTargetState (target, retText, failString, verb))
		{
			return true;
		}
// 		else if (failString.empty() && !badTargetFailString.empty())
// 		{
// 			LOG ("Return fail string: ", badTargetFailString);
// 
// 			failString = badTargetFailString;
// 		}
	}
	
	return false;
}

ParseResult PlayerAction::evaluateActionTargetsFromString (const ATL::String& params,
														   const ATL::String& verb)
{
	ATL::String failString;
	ATL::String returnString;
	ATL::String matchingTarget;
	bool parseSuccessful = false;

	LOGD ("Evaluating with verb '", actionName + "' params: " + params);

	// Next, we interrogate the action targets.
	ActionTargetList_t::iterator itr2;
	for (itr2 = actionTargets.begin(); itr2 != actionTargets.end(); ++itr2)
	{
		LOGD ("Evaluating target: ", itr2->target);
		if (evaluateTargetFromString (*itr2, params, returnString, failString, verb))
		{
			//LOG ("Bloarg!","");
			
			parseSuccessful = true;
		}
	}

	if (parseSuccessful)
	{
		return ParseResult (true, returnString, matchingTarget, verb);
	}

	// We failed to find a valid target for this verb.  Assign the predefined
	// response for this scenario (which, incidentally, may also be blank).
	if (failString.empty() && !badTargetFailString.empty())
	{
		LOG ("Return fail string: ", badTargetFailString);

		failString = badTargetFailString;
	}
	
	// This command was not parsed by this Action.
	return ParseResult (false, failString);
}
