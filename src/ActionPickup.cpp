#include "ActionPickup.h"

#include "DebugLog.h"

ActionPickup::ActionPickup (GameObject* parent): PlayerAction (parent)
{
	actionName = "pickup";
	actionUid = actionName;

	actionAliases.push_back ("pick up");
	actionAliases.push_back ("pick");
	actionAliases.push_back ("get");
	actionAliases.push_back ("take");
	actionAliases.push_back ("steal");
	actionAliases.push_back ("aquire");
	actionAliases.push_back ("abscond with");
	actionAliases.push_back ("purloin");
	
	// Some common spelling mistakes (at least, /my/ common mistakes).
	actionAliases.push_back("pikcup");
}

ActionPickup::ActionPickup (const ActionPickup& rhs, GameObject* parent):
	PlayerAction (rhs)
{
	parentObject = parent;
}

ActionPickup::~ActionPickup (void)
{

}

ParseResult ActionPickup::attemptParse (ATL::String command)
{
	ParseResult result;

	result = extractVerbParams (command);

	if (result.parseSuccessfull == true)
	{
		//DebugLog::log ("Attempting to apply pickup command");
		return evaluateActionTargetsFromString (result.returnText, result.verb);
	}

	return ParseResult();
}

void ActionPickup::addTargetFromKeyVal (const KeyValList_t params)
{

}

// bool ActionPickup::attemptParseProperty (const ATL::String& property,
// 									const ATL::String& value)
// {
// 	if (property == actionName)
// 	{
// 		//addTarget (parentObject->getName(), value);
// 		addObjTarget (value);
// 	}
// 	else if (property == actionName + "_state")
// 	{
// 		LOG ("Adding pickup; key: ", property + "val: " +value);
// 		StringVector_t vals = splitValueString (value, 2);
// 
// 		addTarget (parentObject->getName(), vals [0], vals [1]);
// 	}
// 	else
// 	{
// 		return false;
// 	}
// 
// 	return true;
// }

PlayerAction_ptr ActionPickup::copy (GameObject* newParent) const
{
	return std::make_shared <ActionPickup> (*this, newParent);
}
