#pragma once

#include <memory>

#include "ForwardDeclarations.h"
#include "Tractable.h"
#include "VerbParser.h"

class DefaultResponse;

typedef std::shared_ptr <DefaultResponse> DefaultResponse_ptr;

typedef std::list <DefaultResponse_ptr> DefaultResponseList_t;

class DefaultResponse
{
public:
	static DefaultResponse_ptr newResponseFromParams (KeyValList_t params);
	static DefaultResponse_ptr newFuzzyResponseFromParams (KeyValList_t params);

	DefaultResponse (void);
	virtual ~DefaultResponse (void);

	ParseResult attemptParse (ATL::String command);
	
	void setFuzzy (const bool fuzzy);
	
protected:
	StringVector_t matchToVerbs;

	StringVector_t badResponses; // Responses to an unknown operand.
	StringVector_t emptyResponses; // Responses for this verb with now operand.
	
	bool isFuzzy;  // Set to true to match any occurance of this verb.
	
	ATL::String getRandomResponse (const StringVector_t& responsePool);
	//ATL::String getRandomResponse (void);
};

class DefaultActions: public VerbParser
{
public:
	DefaultActions (void);
	virtual ~DefaultActions (void);
	
	void loadResponsesFromFile (ATL::String file);
	
	Tractable_ptr getTractable (ATL::String objName);
	bool isTractableInScope (const ATL::String& objName) const;

	virtual ParseResult attemptParse (ATL::String command);
	virtual ATL::String getBadCommandResponse (void);
	
	ATL::String getParserName (void) const;

protected:
	TractableList_t emptyObjects;
	
	DefaultResponseList_t defaultResponses;
	StringVector_t badCommandResponses;
	
};
