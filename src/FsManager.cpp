#include "FsManager.h"

#include <dirent.h>
#include <stdexcept>

FsManager::FsManager (void)
{
}


FsManager::~FsManager (void)
{
}

FilePathList_t FsManager::getDirListing (ATL::String dir, ATL::String fileExt)
{
	FilePathList_t::iterator itr;


	// Get a list of files in that directory.
	FilePathList_t dirListing = listFileNamesInDir (dir, fileExt);

	// Prepend the directory to the file paths.
	for (itr = dirListing.begin(); itr != dirListing.end (); ++itr)
	{
		itr->prepend (dir + "/");
	}

	return dirListing;
}

FilePathList_t FsManager::listFileNamesInDir (ATL::String dir, ATL::String fileExt)
{
	FilePathList_t returnList;

	// first off, we need to create a pointer to a directory
	DIR *pdir = NULL; // remember, it's good practice to initialise a pointer to NULL!
	struct dirent *pent = NULL;

	// I used the current directory, since this is one which will apply to anyone reading
	pdir = opendir (dir.cStr ()); // "." will refer to the current directory
	if (pdir == NULL) // if pdir wasn't initialised correctly
	{
		throw std::runtime_error ("pdir could not be initialised correctly");
	}

	while ((pent = readdir (pdir))) // while there is still something in the directory to list
	{
		if (pent == NULL) // if pent has not been initialised correctly
		{
			throw std::runtime_error ("ERROR! pent could not be initialised correctly");
			//exit (3);
		}

		ATL::String entryName (pent->d_name);

		// Exclude the "." and ".." entries.
		if (entryName == "." || entryName == "..")
		{
			continue;
		}

		// Exclude files that don't have "fileExt" at the end, if "fileExt" is non-empty.
		if (!fileExt.empty () && entryName.find (fileExt, entryName.length () - fileExt.length ()) < 0)
		{
			continue;
		}

		returnList.push_back (pent->d_name);
	}

	// finally, let's close the directory
	closedir (pdir);

	return returnList;
}
