#pragma once

#include <AtlString.h>

#include <list>
#include <map>
#include <memory>

class Parsable;

typedef std::shared_ptr <Parsable> Parsable_ptr;

// typedef std::pair <ATL::String, ATL::String> KeyValPair_t;
// typedef std::list <KeyValPair_t> KeyValList_t;

class Parsable
{
public:
	Parsable (void);
	~Parsable (void);

	//virtual ATL::String getName (void) const = 0;
	
	//virtual Parsable_ptr newObjectFromParams (std::list <ATL::String> params) = 0;
	//template <typename T>
	//virtual std::shared_ptr <T> newObjectFromParams (std::list <ATL::String> params) = 0;

protected:
	//static std::map <ATL::String, Parsable_ptr> parsables;
	
	//static void registerParsable (ATL::String name);
};