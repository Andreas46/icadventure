#pragma once

#include <vector>

#include "ForwardDeclarations.h"
#include "PlayerAction.h"

class VerbParser
{
public:
	VerbParser (void);
	virtual ~VerbParser (void);
	
	/**
	* @brief Extract the first occurance of this actions verb.
	*
	* @param inString
	*	The String to parse.
	*
	* @return A pair contining the extracted and normalised verb,
	*	and a number corresponding To the position in "inString" that
	*	is was found.
	*	If the verb is not found, the pair consists of an empty string
	*	and "-1".
	**/
	//virtual VerbSearchResult extractVerb (const ATL::String& inString) const = 0;
	
	/**
	* @brief Parse the command.
	*
	* @param operands
	*	The rest of the string appling to this verb.
	*
	* @param action
	*	The verb being used (usually that of this action).
	*
	* @return A pair; The first return item (bool) indicating
	*	successful handling (true) or failure (false), and the second
	*	item is the string to be reported.
	*	
	*	The reason for the two parameters is, that if a command fails early in
	*	The parsing hirachey, it can optinally return a witty fail message.
	*	If it is not provided (empty string) an appropriate fail response will be
	*	returned from the default responses.
	**/
	virtual ParseResult attemptParse (ATL::String command) = 0;
	
	virtual bool hasAction (ATL::String actionUid) const;
	virtual void addAction (PlayerAction_ptr action);
	virtual PlayerAction_ptr getAction (const ATL::String& actionUid) const;
	
	virtual ATL::String getParserName (void) const = 0;

protected:
	PlayerActionList_t actions;
};