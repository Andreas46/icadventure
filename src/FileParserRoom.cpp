#include "FileParserRoom.h"

#include "ActionMove.h"
#include "ActionCustom.h"
#include "DebugLog.h"

Room_ptr FileParserRoom::parseRoomFile (ATL::String filePath)
{
	parseFile (filePath);

	return currentRoom;
}

void FileParserRoom::parseSection (void)
{
	KeyValList_t::iterator itr;
	ATL::String str;

	LOGV ("Parsing Room Section: [", currentSection + "].");
	
	for (itr = sectionKeyPairs.begin(); itr != sectionKeyPairs.end(); ++itr)
	{
		str.clear();
		str = ATL::String (itr->first) + ": " + itr->second;
		DebugLog::getInstance().log (str, DebugLog::LevelVerbose);
	}
	
	// First thing to do is make sure this section is, or we have a working room.
	if (currentRoom.use_count() == 0 && currentSection != "room" && currentSection != "strings")
	{
		ATL::String str = ATL::String ("Can not parse section '") + 
										currentSection + "': No room set!";
		throw std::runtime_error (str.cStr());
	}

	// Now, which, oh which section are we to parse?
	if (currentSection == "room")
	{
		currentRoom = Room::newObjectFromParams (sectionKeyPairs);
	}
	else if (currentSection == "move")
	{
		ActionMove_ptr moveAction;
		//PlayerAction_ptr moveAction = ActionMove::newObjectFromParams (sectionKeyPairs, //currentRoom.get());
		//currentRoom->addRoomAction ("move", moveAction);
		//Cue::newCueFromString ("move");
		moveAction = std::static_pointer_cast <ActionMove> (currentRoom->getAction ("move"));
		moveAction->addTargetFromKeyVal (sectionKeyPairs);
	}
	else if (currentSection == "object")
	{
		//PlayerActionMap_t::iterator itr;
		Tractable_ptr tractable = Tractable::newObjectFromParams (sectionKeyPairs,
																  currentRoom.get(),
																  customActions
 																);

		currentRoom->addTractable (tractable);
	}
	else if (currentSection == "object_template")
	{
		Tractable_ptr tractable = Tractable::newObjectFromParams (sectionKeyPairs,
																  currentRoom.get(),
																  customActions
 																);

		Tractable::addTemplateObject (tractable);
	}
	else if (currentSection == "custom_verb")
	{
		//DebugLog::log (ATL::String ("Parsing custom verb"));
		PlayerAction_ptr customAction;
		customAction = ActionCustom::newObjectFromParams (sectionKeyPairs,
														  currentRoom.get());
		
		LOG ("Parsing Custom Action: ", (customAction->getUid()));

		currentRoom->addAction (customAction);
		customActions.push_back (customAction->copy (currentRoom.get()));

		DebugLog::log (ATL::String ("Parsing custom verb: ") + customAction->getName());
	}
	else if (currentSection == "strings")
	{
// 		KeyValList_t::iterator itr;
// 		for (itr = sectionKeyPairs.begin(); itr != sectionKeyPairs.end(); ++itr)
// 		{
// 			valueVariables [] =
// 		}
		valueVariables.insert (sectionKeyPairs.begin(), sectionKeyPairs.end());
	}
	else
	{
		LOGW ("Failed to handle section nname: ", currentSection);
	}
}

void FileParserRoom::clear (void)
{
	FileParser::clear();

	currentRoom.reset();
}