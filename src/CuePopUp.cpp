#include "CuePopUp.h"

#include "DebugLog.h"

#include "GraphicsPopUpWindow.h"

bool CuePopUp::execute (void)
{
	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}
	
	LOG ("CuePopUp: Creating popup of width '", popUpWidth + "'.");
	
	GraphicsPopUpWindow::newPopUp (popUpText, popUpWidth);
	
	return true;
}

ATL::String CuePopUp::getName (void)
{
	return "pop_up";
}

void CuePopUp::parseStoredCueString (void)
{
	// Format: %(pop_up width text)
	ATL::String widthAsText;
	
	//LOG ("creating pop");

	// Extract the width the popup.
	widthAsText = unparsedCueParameters.getNextWord();
	popUpWidth = (int) widthAsText.getAsDouble();

	ASSERT_S (popUpWidth > 2 && popUpWidth < 800,
			  "CuePopUp: invalid width '", + popUpWidth + "' specified!");

	popUpText = unparsedCueParameters.subStr (widthAsText.length() + 1);
}
