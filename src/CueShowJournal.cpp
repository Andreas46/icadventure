#include "CueShowJournal.h"

#include "DebugLog.h"
#include "JournalPopUpWindow.h"

bool CueShowJournal::execute (void)
{
	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	JournalPopUpWindow::showJournal (pageNum);
	
	return true;
}

ATL::String CueShowJournal::getName (void)
{
	return "show_journal";
}

void CueShowJournal::parseStoredCueString (void)
{
	// Format: %(show_journal 0 90 70)
	ATL::String paramAsStr;

	// Extract pagenum.
	paramAsStr = unparsedCueParameters.getNextWord ();
	ASSERT_S (!paramAsStr.empty(),
			  "CueShowJournal: no pageNum specified in:", unparsedCueParameters);
	pageNum = paramAsStr.getAsInt64();
}

