#pragma once

#include "Cue.h"

class CuePopUp: public Cue
{
public:
	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	ATL::String popUpText;
	int popUpWidth;

	virtual void parseStoredCueString (void);
};
