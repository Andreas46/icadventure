#include "RawInputHandler.h"

#include "DebugLog.h"

#include <curses.h>

// void rawInputHandler::update (void)
// {
// 
// }

void RawInputManager::pushHandler (RawInputHandler* handler)
{
	inputHandlers.push_front (handler);
}

RawInputHandler* RawInputManager::popHandler (void)
{
	RawInputHandler* handler = inputHandlers.front();
	
	inputHandlers.pop_front();
	
	return handler;
}

void RawInputManager::removeHandler (RawInputHandler* handler)
{
//	ASSERT (inputHandlers.);

	inputHandlers.remove (handler);
}

#include "Graphics.h"

void RawInputManager::checkForInput (void)
{
	//int inChar = getch();
	//inChar = wgetch ();
	int inChar = inputHandlers.back()->getChar ();

//	Graphics::refreshScreen ();
	
	// If there was valid input.
	if (inChar != ERR)
	{
		handleChar (inChar);
	}
}

void RawInputManager::handleChar (int c)
{
	RawInputHandlerList_t::iterator itr;

	for (itr = inputHandlers.begin(); itr != inputHandlers.end(); ++itr)
	{
		// Input handlers return false when the char should no longer properage.
		if ((*itr)->handleChar (c) == false)
		{
			break;
		}
	}
}
