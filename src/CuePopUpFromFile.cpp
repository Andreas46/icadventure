#include "CuePopUpFromFile.h"

#include <fstream>

#include "DebugLog.h"
#include "Graphics.h"
#include "GraphicsPopUpWindow.h"

bool CuePopUpFromFile::execute (void)
{
	ATL::String popUpText;
	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}
	
	LOG ("CuePopUpFromFile: Creating popup of width '", popUpWidth +
		"' from file '" + textFilePath + "'.");
	
	std::ifstream fileStream (textFilePath.cStr());

	if (fileStream.is_open())
	{
		char buff [51];
		
		while (fileStream.get (buff, 50, EOF))
		{
	//		fileStream.get (buff, 50);
			popUpText.append (buff);
			
// 			fileStream.get (buff, 1150);
// 			popUpText.append (buff);
			
//			LOGW ("popip text: ", popUpText);

			if (fileStream.eof())
			{
				break;
			}
			
		}

		fileStream.close();
	}
	else
	{
		FAIL ("Failed to open popup text file '" + textFilePath + "'.");
	}
	
	GraphicsPopUpWindow::newPopUp (popUpText, popUpWidth, centreJustified);

	Graphics::refreshScreen();
	
	return true;
}

ATL::String CuePopUpFromFile::getName (void)
{
	return "pop_up";
}

void CuePopUpFromFile::parseStoredCueString (void)
{
	// Format: %(pop_up width text)
	ATL::String widthAsText;
	ATL::String	centreJusAsText;
	
	//LOG ("creating pop");

	// Extract the width the popup.
	widthAsText = unparsedCueParameters.getNextWord();
	popUpWidth = (int) widthAsText.getAsDouble();

	ASSERT_S (popUpWidth > 2 && popUpWidth < 800,
			  "CuePopUpFromFile: invalid width '", + popUpWidth + "' specified!");

	// Is the popup centreJustified?
	centreJusAsText = unparsedCueParameters.getNextWord (widthAsText.length());

	ASSERT (!centreJusAsText.empty (), "CuePopUpFromFile: must specify centre Justification!");

	centreJustified = centreJusAsText.getAsBool();

	textFilePath = unparsedCueParameters.subStr (widthAsText.length() + centreJusAsText.length() + 2);
	
	ASSERT (!textFilePath.empty(), "CuePopUpFromFile: Must specify file name!");
	
	// Assert that file exists and it readable.
	std::ifstream fileStream (textFilePath.cStr());
	bool fileStreamOpen = fileStream.is_open();
	fileStream.close();
	
	ASSERT (fileStreamOpen, "File '" + textFilePath + "' does not exist!");

	hasCueBeenParsed = true;
}
