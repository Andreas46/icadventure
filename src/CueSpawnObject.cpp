#include "CueSpawnObject.h"

#include "GameManager.h"

CueSpawnObject::CueSpawnObject (void)
{
	spawnInInv = false;
}

CueSpawnObject::~CueSpawnObject (void)
{

}

bool CueSpawnObject::execute (void)
{
	Tractable_ptr tracTemplate;
	Tractable_ptr newTractable;

	LOG ("Executing Cue spawn cue.","");

	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	Tractable::spawnObjFromTemplate (objTemplateName, spawnInInv);

	return true;
}

ATL::String CueSpawnObject::getName (void)
{
	return "spawn";
}

void CueSpawnObject::parseStoredCueString (void)
{
	// Format: %(spawn item_name inv|room)
	ATL::String invSpwnStr;

	// Find the tractable (first param).
	objTemplateName = unparsedCueParameters.getNextWord();

	ASSERT (!objTemplateName.empty(), "Object template name must not be empty!");

	// Get the location of where to spawn the object.
	invSpwnStr = unparsedCueParameters.getNextWord (objTemplateName.length());

	// Are we spawning to the inventory?
	if (invSpwnStr == "inv")
	{
		spawnInInv = true;
	}
	// In the room, perhaps?
	else if (invSpwnStr == "room")
	{
		spawnInInv = false;
	}
	else
	{
		// Unknown, missing token.
		FAIL_S ("Can not parse 'spawn' cue; bad 'spawnInInv' token: ", invSpwnStr);
	}

	hasCueBeenParsed = true;
}
