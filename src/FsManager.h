#pragma once

#include <AtlString.h>
#include <list>

typedef std::list <ATL::String> FilePathList_t;

class FsManager
{
public:
	FsManager (void);
	~FsManager (void);

	/**
	* @brief get a list of file (relative to "dir") in "dir".
	*
	* @parm dir
	*	The directory path to query.
	*
	* @param fileExt
	*	When non-empty, only files with this extension will be listed.
	*/
	FilePathList_t getDirListing (ATL::String dir, ATL::String fileExt = "");

protected:
	FilePathList_t listFileNamesInDir (ATL::String dir, ATL::String fileExt = "");
};

