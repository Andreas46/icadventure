#pragma once

#include "PlayerAction.h"

class ActionEmpty: public PlayerAction
{
public:
	ActionEmpty (GameObject* parent);
	ActionEmpty (const ActionEmpty& rhs, GameObject* parent);
	virtual ~ActionEmpty (void);

	virtual ParseResult attemptParse (ATL::String command);

	virtual void addTargetFromKeyVal (const KeyValList_t params);
	
	virtual PlayerAction_ptr copy (GameObject* newParent) const;

protected:
	virtual bool attemptParseProperty (const ATL::String& property,
										const ATL::String& value);
};