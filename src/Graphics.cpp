﻿#include "Graphics.h"

#include <curses.h>
#include <stdexcept>

#include "GameManager.h"
#include "GraphicsWindow.h"

Graphics* Graphics::instance = NULL;

Graphics& Graphics::getInstance (void)
{
	return *instance;
}

void Graphics::refreshScreen (void)
{
	refresh ();
	update_panels ();
	doupdate();
}

Graphics::Graphics (void)
{
	if (instance == NULL)
	{
		instance = this;
	}
	else
	{
		throw std::runtime_error ("Multiple instances of Graphics!");
	}

	cmdHandler = NULL;

	numInputLines = 1;
	inputCursorPos = 0;

	inputPromptEnabled = false;
	
	inputHistory.push_back ("");
	currentInput = --inputHistory.end();

	// initialise curses.
	initCurses();

//#ifdef _WIN32
//	// Set terminal size.
//	resize_term (40, 100);  // Lines, coloumns
//	//wresize (stdscr, 40, 100);  // Lines, coloumns
//#endif
	
	updateScreenVars();

	drawPrompt();
}

Graphics::~Graphics (void)
{
	delete outTextWin;
	delete inputPromptWin;

	// Clean up curses.
	endwin ();
	
	instance = NULL;
}

void Graphics::addText (ATL::String text)
{
	outTextWin->addText (text);
}

void Graphics::addTextLine (ATL::String text)
{
	outTextWin->addText (text + '\n' + '\n');
}

void Graphics::clearOuputText (void)
{
	outTextWin->clearText();
}

void Graphics::initCurses (void)
{
	//setlocale (LC_ALL, "");
	initscr ();
	noecho ();
	curs_set (FALSE);
	cbreak ();       /* take input chars one at a time, no wait for \n */
	//echo();         /* echo input - in color */
	//raw ();

	timeout (1); // Non-blocking input (timout after 100ms).

	keypad (stdscr, true);  // Enable return of extra keys (del, arrow, etc).

	//clear();

	refresh();

#ifdef _WIN32
	// Set terminal size.
	resize_term (50, 100);  // Lines, coloumns
	//wresize (stdscr, 40, 100);  // Lines, coloumns
#endif
	
	// Create windows.
	outTextWin = new GraphicsWindow();
	inputPromptWin = new GraphicsWindow();
	
	outTextWin->addText("First line\n");
}

void printAllAltCodes (void)
{
	for (int i = 0; i < 255; ++i)
	{
		//addch (NCURSES_ACS(i));
	}
}

void Graphics::drawFrame (void)
{
	updateScreenVars();
	
	outTextWin->setSize (0, 0, screenXSize, screenYSize - numInputLines - 1);
	inputPromptWin->setSize (0, screenYSize - numInputLines - 2, screenXSize, numInputLines + 2);

	//getPromptInput ();
	
//	outTextWin->addText("Here, have some gum!And then: ");

	//drawBoarder();
	//drawOutputText();
	drawPrompt();
	//inputPromptWin->setText ("adfsdf");
	
//	WINDOW *local_win;
	
//	printAllAltCodes();

	//refresh ();

	refreshScreen();
}

void Graphics::leaveCursesMode (void)
{
	def_prog_mode();		/* Save the tty modes		  */
	endwin();
}

void Graphics::restoreCursesMode (void)
{
	reset_prog_mode();
}

void Graphics::setCommandHandler (CommandHandler handler)
{
	cmdHandler = handler;
}

int Graphics::getScreenXsize (void) const
{
	return screenXSize;
}

int Graphics::getScreenYsize (void) const
{
	return screenYSize;
}

void Graphics::resizeTerminal (int lines, int columns)
{
	// Set terminal size.
	resize_term (lines, columns);  // Lines, coloumns

	updateScreenVars ();
}

void Graphics::updateScreenVars (void)
{
	// Update window sizes (in case screen is resized).
	getmaxyx (stdscr, screenYSize, screenXSize);

	// Input box vars.
	inputCursorStartPosX = 2;
	inputCursorStartPosY = screenYSize - numInputLines - 1; // -1 to account for boarder lines.

	//inputCursorPos = inputCursorStartPosX;

	// Main text output box vars.
	outTextStartPosX = 2;
	outTextStartPosY = 2;
	outTextMaxWidth = screenXSize - 2;
	outTextMaxHeight = screenYSize - inputCursorStartPosY - 3;
}

void Graphics::drawBoarder (void)
{
	int inputTopLinePos;

	// Draw top and bottom lines.
	for (int i = 0; i < screenXSize; ++i)
	{
		mvprintw (0, i, "-"); // Top line
		mvprintw (screenYSize - 1, i, "-"); // Bottom line
	}

	// Draw sides.
	for (int i = 0; i < screenYSize; ++i)
	{
		mvprintw (i, 0, "|"); // Left  line
		mvprintw (i, screenXSize - 1, "|"); // Bottom line
	}

	// Draw boarder's corners.
	mvprintw (0, 0, "+");  // Upper Left.
	mvprintw (screenYSize - 1, 0, "+"); // Lower Left.
	mvprintw (0, screenXSize - 1, "+"); // upper right.
	mvprintw (screenYSize - 1, screenXSize - 1, "+"); // Lower right.

	// draw text input area.
	inputTopLinePos = screenYSize - numInputLines - 2; // -2 for 2x lines to offset past.

	for (int i = 0; i < screenXSize; ++i)
	{
		mvprintw (inputTopLinePos, i, "-"); // Top line
	}
	
	mvprintw (inputTopLinePos, 0, "+");  // Right upper corner.
	mvprintw (inputTopLinePos, screenXSize - 1, "+");  //  Left upper corner.
}

void Graphics::drawPrompt (void)
{
	ATL::String prompt;
	//mvaddstr (inputCursorStartPosY, inputCursorStartPosX, ">");
	
	//inputPromptWin->addText (">");
	prompt += "> ";

	// Draw inputed text.
	if (currentInput->length() > 0)
	{
		//addstr (inputLine.cStr());
		//inputPromptWin->addText (inputLine);
		prompt += *currentInput;
	}

	// Draw cursor.
	//addch ('_');
	//addch (']' | A_UNDERLINE);
	// inputPromptWin->addText ('_');
	prompt += "_";
	
	inputPromptWin->setText (prompt);
}

void Graphics::drawOutputText (void)
{
	mvaddstr (outTextStartPosY, outTextStartPosX, outputText.cStr());
}

void Graphics::clearInputBox (void)
{
	// Reset cursor position.
	move (inputCursorStartPosY, inputCursorStartPosX);

	// We'll just overwrite the text with spaces.
	for (int i = 0; i < numInputLines; ++i)
	{
		for (int j = 0; j < outTextMaxWidth; ++j)
		{
			addch (' ');
		}
	}
}

//void Graphics::getPromptInput (void)
bool Graphics::handleChar (int c)
{
//	int inChar;

	// Return if this prompt is disabled.
	if (!inputPromptEnabled)
	{
		return true;
	}

	// Return if there is no input to precess.
	//if (kbhit)
	
//	inChar = getch();

	switch (c)
	{
		case ERR:
			// Error.  Most likely, just timed out.
			break;

		case KEY_RESIZE:
			// Terminal has been resized.
			break;

		case 8:
		case KEY_DC:
		case KEY_BACKSPACE:
			if (currentInput->length () > 0)
			{
				// Clear last printed character.
				//mvaddch (inputCursorStartPosY, inputCursorStartPosX + inputLine.length () + 1, ' ');
				//clearInputBox();

				--inputCursorPos;

				// Remove character from inputLine.
				*currentInput = currentInput->subStr (0, currentInput->length () - 1);
			}
			break;

		case 27: // Escape key
			// Quit.
			GameManager::getInstance().quit();
			break;
			
		//case KEY_DC: // Delete
		//	break;

		case '\r':
		case '\n':
			// If nothing has been entered, do nothing.
			if (currentInput->empty())
			{
				break;
			}

			// Print the input.
			addTextLine (ATL::String ("> ") + *currentInput);

			// Handle this new command, if a command handler has been set.
			if (cmdHandler)
			{
				cmdHandler (*currentInput);
			}
			
			// Don't push empty lines onto the history
			if (!inputHistory.back().empty())
			{
				// Push a new line onto the input history, and begin editing that
				inputHistory.push_back ("");
			}

			currentInput = --inputHistory.end(); // Return to the new command
			
			
			
			inputCursorPos = 0;
			break;
		
		// Keys we don't wan't cluttering up the console.
		case KEY_LEFT:
		case KEY_RIGHT:
			break;
			
		case KEY_UP:
			if (currentInput != inputHistory.begin())
			{
				// Roll one input command further back into history.
				--currentInput;
			}
			break;
			
		case KEY_DOWN:
			if (currentInput != --inputHistory.end())
			{
				// Roll one input command back into the present.
				++currentInput;
			}
			break;

		//case ' ':
		//	break;

		default:
		//	if (inputCursorPos == currentInput->length())//&& isalnum (inChar))
			//{
				currentInput->append ((char) c);
				++inputCursorPos;
			//}
	}
	
	return false;
}

int Graphics::getChar (void)
{
	//return wgetch (inputPromptWin->getWindow());
	return getch ();
}
