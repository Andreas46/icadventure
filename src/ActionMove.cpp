#include "ActionMove.h"

#include "CueMoveTo.h"
#include "DebugLog.h"
#include "GameManager.h"

ActionMove::ActionMove (GameObject* parent): PlayerAction (parent)
{
	actionName = "move";
	actionUid = actionName;

	actionAliases.push_back ("travel");
	actionAliases.push_back ("go");
	actionAliases.push_back ("head");
	actionAliases.push_back ("walk");
	actionAliases.push_back ("perambulate");
	actionAliases.push_back ("stroll");
	actionAliases.push_back ("meander");
}

ActionMove::ActionMove (const ActionMove& rhs, GameObject* parent):
	PlayerAction (rhs)
{
	parentObject = parent;
}

ActionMove::~ActionMove (void)
{
}

ParseResult ActionMove::attemptParse (ATL::String command)
{
	ParseResult result;

	result = extractVerbParams (command);

	if (result.parseSuccessfull == true)
	{
		//DebugLog::log ("Attempting to parse move command");
		return evaluateActionTargetsFromString (result.returnText, result.verb);
	}
// 	else
// 	{
// 		return ParseResult (false, "");
// 	}

	// There is a special case with move where we want to be able to
	// travel to the target by only typing in the targets name.
	ActionTargetList_t::iterator itr;
	for (itr = actionTargets.begin(); itr != actionTargets.end(); ++itr)
	{
		if (itr->target == command)
		{
			LOGV ("Performing short move for cmd: ", command);

			return evaluateActionTargetsFromString (command, command);
// 			if (itr->parentObjCondition.empty() ||  // Ignore empty conditions.
// 				parentObject->getState() == itr->parentObjCondition)
// 			{
// 				// We've made a match.  Execute associated cues.
// 				itr->executeActionCues();
// 
// 				return ParseResult (true, itr->getReturnText(), itr->target);
// 			}
		}
	}

	//return ParseResult (false, "");
	return ParseResult();
}

//PlayerAction_ptr ActionMove::newObjectFromParams (KeyValList_t params, GameObject* parent)
//{
//	KeyValList_t::iterator itr;
//	ActionMove_ptr newMove = std::make_shared <ActionMove> (parent);
//
//	for (itr = params.begin (); itr != params.end (); ++itr)
//	{
//		//if (itr->first == "")
//		//{
//		//	direction
//		//}
//	}
//
//	return newMove;
//}

//void ActionMove::addMoveTargetFromKeyVal (const KeyValList_t params)
void ActionMove::addTargetFromKeyVal (const KeyValList_t params)
{
	KeyValList_t::const_iterator itr;
	ActionTarget target;

	ATL::String moveText;

	//DebugLog::log (ATL::String ("Parsing Move params: "));

	for (itr = params.begin (); itr != params.end (); ++itr)
	{
		if (itr->first == "direction")
		{
			// This is the direction displayed to the user.  It will be stored as the "target".
			target.target = itr->second;
		}
		else if (itr->first == "room_name")
		{
			ATL::String roomNameAsTarget;
			CueMoveTo_ptr newCue = std::make_shared <CueMoveTo>();

			// This is room we will be moving to.  This will be implemented as an cue.
			newCue->setParameters (itr->second);  // Only one param; move target.
			target.onExecActions.push_back (newCue);

			// We'll also add an alias for which is the room name, with underscores replaced.
			roomNameAsTarget = itr->second;
			roomNameAsTarget.replace ("_", " ");
			target.addAlias (roomNameAsTarget);
		}
		else if (itr->first == "move_text")
		{
			//moveText = target.returnText = itr->second;
			target.setReturnText (itr->second);
			//DebugLog::log (ATL::String ("Adding move Text: ") + itr->second + " to move target: " + itr->second);
		}
		else if (itr->first == "prereq_state")
		{
			// Set the move condition.
			target.parentObjCondition = itr->second;
		}
		else if (itr->first == "move_state")
		{
			ActionTarget movStateTarget;
			StringVector_t vals;
			//door|locked|You attempt to ignore the door.
			
			movStateTarget.target = target.target;
			
			vals = splitValueString (itr->second, 2);

			movStateTarget.setReturnText (vals [2]);
			movStateTarget.parentObjCondition = vals [0] + '|' + vals [1];
			
			actionTargets.push_back (movStateTarget);
		}
		else if (itr->first == "alias")
		{
			target.addAlias (itr->second);
		}
// 		else if (itr->first == "look")
// 		{
// 			parentObject->
// 		}
		else
		{
			ATL::String str;
			str << "Failed to parse Room Param: " << itr->first << ": " << itr->second;
			DebugLog::log (str, DebugLog::LevelWarning);
		}
	}

	// Assert that the action target has both "target" and at least once Cue.
	DebugLog::assert (!target.target.empty (), "Move target has no Direction!");
	DebugLog::assert (!target.onExecActions.empty (), target.target + ": Move Target has no room_name!");

	// If we had move text, add it the included cue.
	//target.onExecActions.back()->

	actionTargets.push_back (target);
}

bool ActionMove::evaluateTargetState (ActionTarget target,
										ATL::String& retText,
										ATL::String& failString,
										const ATL::String& verb)
{
	// Attempt to identify this as a "normal" target (no split dependant state)
	if (target.parentObjCondition.find ("|") < 0)
	{
		return PlayerAction::evaluateTargetState (target, retText, failString, verb);
	}
	else if (!target.parentObjCondition.empty())
	{
		// The parent object condition is a little different here.
		// Format is "object|dependant_state".
		StringVector_t vals;

		//LOG ("Evaluating move condition: ", target.parentObjCondition);

		vals = splitValueString (target.parentObjCondition, 1);
		
		if (!GMGR.isTractableInScope (vals[0]))
		{
			// The tractable whose state we depend on is not here.
			LOGD ("Move obj not in scope: ", vals[0]);
			return false;
		}
	
		// Determine the objects state.
		if (GMGR.getTractableFromCurrentContext(vals[0])->getState() != vals[1])
		{
			// State does not match.
			LOGD ("Move obj; state does not match: ", vals[1]);
			return false;
		}
	
	}

// 	if (target.parentObjCondition.empty() ||  // Ignore empty conditions.
// 		parentObject->getState() == target.parentObjCondition)
// 	{
		// We've made a match.  Execute associated cues.
		target.executeActionCues();

		//return ParseResult (true, itr2->returnText);
		retText += target.getReturnText();

		return true;
// 	}
// 	else if (!target.failText.empty() && failString.empty())
// 	{
// 		failString = target.failText;
// 	}
}

//bool ActionMove::attemptParseProperty (const ATL::String& property,
//									   const ATL::String& value)
//{
//	return false;
//}

PlayerAction_ptr ActionMove::copy (GameObject* newParent) const
{
	return std::make_shared <ActionMove> (*this, newParent);
}
