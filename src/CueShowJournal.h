#pragma once

#include "Cue.h"

class CueShowJournal: public Cue
{
public:
	virtual bool execute (void);
	virtual ATL::String getName (void);

protected:
//	int width;
//	int height;
	int pageNum;

	virtual void parseStoredCueString (void);
};