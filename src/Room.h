#pragma once

#include <AtlString.h>
#include <list>
#include <map>
#include <memory>

#include "ForwardDeclarations.h"
#include "ActionLook.h"
#include "PlayerAction.h"
#include "Tractable.h"
#include "VerbParser.h"

class PlayerAction;
class Room;

//typedef std::pair <ATL::String, ATL::String> KeyValPair_t;
//typedef std::list <KeyValPair_t> KeyValList_t;

typedef std::shared_ptr <Room> Room_ptr;
typedef std::map <ATL::String, Tractable_ptr> TractableMap_t;

class Room: public GameObject, public VerbParser
{
public:
	Room();
	~Room();

	/**
	* @brief Return the room by the name of "name".
	*
	* @param name
	*	name of the room to find.
	*
	* @return The room by "name", or null if not found.
	**/
	//static Room* findRoom (ATL::String name);
	
	/**
	* @brief Return a new instance of type Room, with members set from "params".
	* 
	* @param params
	* 	A list of key/value pairs containg text representations of the
	* 	members to populate.
	* 
	* @return A new instance of Room, wrapped in a shared pointer.
	* 
	* @exception std::runtime_error
	* 	Thrown when parameter required to construct the new object is absent.
	**/
	static Room_ptr newObjectFromParams (KeyValList_t params);
	
	ATL::String getName (void) const;
	ATL::String getParserName (void) const;

	ParseResult attemptParse (ATL::String command);

	/**
	* @brief Return true if this rooms name, or one of it's aliases match.
	*
	* @param name
	*	String of the name to test.
	*
	* @return True if this rooms name, or one of it's aliases match 
	**/
	bool isNameOfRoom (ATL::String name);

	//bool attemptToHandleActionString (ATL::String);
	bool attemptParsePropertyWithActions (const ATL::String& property,
										  const ATL::String& value);
	
	//void addRoomAction (ATL::String verb, PlayerAction_ptr action);

	void addTractable (Tractable_ptr tractable);
	Tractable_ptr popTractable (ATL::String tractableName);

	//virtual VerbSearchResult extractVerb (const ATL::String& inString) const;

	Tractable_ptr getTractable (ATL::String objName);
	bool isTractableInScope (const ATL::String& objName) const;
	//void destroyTractable (const ATL::String& objName);
	ATL::String getNearbyTractableText (void);

	//PlayerAction_ptr getAction (const ATL::String& actionName);
	ATL::String triggerRoomEnter (void);
	ATL::String triggerRoomExit (void);

protected:
	ATL::String roomName;
	//std::list <ATL::String> nameAliases;
	ATL::String roomBrief;
	ActionTarget onEnterTarget;
	ActionTarget onExitTarget;

	TractableMap_t tractables;
	std::map <ATL::String, Room*> directions;
//	ATL::String roomState;

// 	PlayerActionMap_t actions;

//	ATL::String getState (void) const;
	
	Look lookAction;
};

