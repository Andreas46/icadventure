#pragma once

#include <AtlString.h>
#include <map>

#include "ForwardDeclarations.h"
#include "Tractable.h"
#include "VerbParser.h"

//void padStringRemainder ()

class Inventory: public VerbParser, public GameObject
{
public:
	Inventory();
	~Inventory();

	//virtual VerbSearchResult extractVerb (const ATL::String& inString) const;

	virtual ParseResult attemptParse (ATL::String command);
	
	ATL::String getInventory (void);
	void printInventory (void);
	
	Tractable_ptr getTractable (ATL::String objName);
	bool isTractableInScope (const ATL::String& objName) const;
	void addTractable (Tractable_ptr tractable);
	Tractable_ptr popTractable (const ATL::String& tractableName);
	void destroyTractable (const ATL::String& objName);

	
	ATL::String getName (void) const;
	ATL::String getParserName (void) const;

protected:
	TractableMap_t inventory;

//	PlayerActionMap_t actions;
};

