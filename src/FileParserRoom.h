#pragma once

#include "AtlString.h"

#include "FileParser.h"
#include "Room.h"

class FileParserRoom: public FileParser
{
public:
	Room_ptr parseRoomFile (ATL::String filePath);

	virtual void clear (void);
protected:
	Room_ptr currentRoom;

	void parseSection (void);
};
