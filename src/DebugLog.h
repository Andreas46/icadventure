#pragma once

#include <AtlString.h>
#include <map>

#define LN_INF (ATL::String ("[") + __FILE__ + ":" + (double) __LINE__ + "] ")

#define ASSERT(X,Y) DebugLog::assert (X, LN_INF + Y)
#define ASSERT_S(X,Y,Z) DebugLog::assert (X, LN_INF + Y + Z)
#define FAIL(X) DebugLog::abort (LN_INF + X); throw 0;
#define FAIL_S(X,Y) DebugLog::abort (LN_INF + X + Y); throw 0;
#define LOG(X,Y) DebugLog::log (ATL::String (X) + Y)
#define LOGD(X,Y) DebugLog::log (ATL::String (X) + Y, DebugLog::LevelDebug)
#define LOGV(X,Y) DebugLog::log (ATL::String (X) + Y, DebugLog::LevelVerbose)
#define LOGW(X,Y) DebugLog::log (ATL::String (X) + Y, DebugLog::LevelWarning)
#define LOGE(X,Y) DebugLog::log (ATL::String (X) + Y, DebugLog::LevelError)

class DebugLog;
// enum DebugLog::LogLevel;

// typedef std::map <int, ATL::String> LogLevelMap_t;

class DebugLog
{
public:
	typedef enum
	{
		LevelDebug,
		LevelVerbose,
		LevelInfo,
		LevelWarning,
		LevelError,
		LevelCritical
	} LogLevel;
	
	typedef std::map <LogLevel, ATL::String> LogLevelMap_t;

	bool logToGraphics;

	static DebugLog& getInstance (void);

	DebugLog (void);
	~DebugLog (void);

	static ATL::String getLogLevelAsText (LogLevel level);
	
	static void log (ATL::String text, LogLevel level = LevelInfo);
	static void assert (bool testConidtion, ATL::String message = "");
	static void abort (ATL::String message);
	
	void setLogLevel (LogLevel);
	void setLogLevelFromText (ATL::String level);
	LogLevel getLogLevel (void) const;
	
protected:
	static DebugLog* instance;

	static LogLevelMap_t logLevelMap;

	static void initLogLevelMap (void);

	ATL::String debugLogName;
	LogLevel logLevel;
};
