#include "CueDestoryObject.h"

#include "GameManager.h"
#include "Tractable.h"

CueDestoryObject::CueDestoryObject ()
{
}


CueDestoryObject::~CueDestoryObject ()
{
}

bool CueDestoryObject::execute (void)
{
	Tractable_ptr tractable;

	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	// Check for valid object name.
	if (!GMGR.isTractableInScope (objectName))
	{
		LOGE ("CueDestoryObject: Can't locate tractable '", objectName +"' as it's not in scope!");

		return false;
	}
	
	LOG ("CueDestoryObject: Destroying obj '", objectName + "'.");
	
	// Attempt to locate the tractable in the game scene.
	//tractable = GMGR.getTractableFromCurrentContext (objectName);

	tractable = GMGR.popTractable (objectName);
	
	// Defer destruction in case this is the calling object.
	Tractable::destroyTractableLater (tractable);
	
	return true;
}

ATL::String CueDestoryObject::getName (void)
{
	return "destroy_obj";
}

void CueDestoryObject::parseStoredCueString (void)
{
	// Extract name of cue condition.
	objectName = unparsedCueParameters.getNextWord ();

	ASSERT_S (!objectName.empty(),
			  "CueDestoryObject: no object specified in String", unparsedCueParameters);
}