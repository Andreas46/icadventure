#pragma once

#include <AtlString.h>
#include <memory>
#include <set>

class CueCondition;

typedef std::shared_ptr <CueCondition> CueCondition_ptr;

class CueCondition
{
public:
	static CueCondition_ptr newCueCondFromString (ATL::String& inString);

//	static void parseAllCueConditions (void);

	CueCondition (void);
	virtual ~CueCondition (void);

	// Extract this condition from the string, the return the string remainder.
	virtual ATL::String parseFromString (const ATL::String& parameters) = 0;

	virtual bool evaluate (void) = 0;

	virtual ATL::String getName (void) = 0;

	// Return the number of "words" this cue expects.
//	virtual size_t getNumTokens (void);

 //   ATL::String getParsedStringRemainder (void);

protected:
//	static std::set <CueCondition*> allTheConditions;

//	ATL::String unparsedString;
	bool hasConditionBeenParsed;

	// Returns the remainder of the string after parsing.
//	virtual ATL::String parseStoredString (void) = 0;
};

class CueConditionIsObjInState: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);	

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String NameOfObjToCheck;
	ATL::String state;

//	ATL::String parseStoredString (void);
};

class CueConditionVarEqual: public CueCondition
{
public:
    virtual bool evaluate (void);
    virtual ATL::String getName (void);
    virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String varName;
	ATL::String compareState;
};

class CueConditionVarNotEqual: public CueCondition
{
public:
    virtual bool evaluate (void);
    virtual ATL::String getName (void);
    virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String varName;
	ATL::String compareState;
};

class CueConditionRoomInState: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String roomName;
	ATL::String compareState;
};

class CueConditionInRoom: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String roomName;
};

class CueConditionObjInRoom: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String roomName;
	ATL::String objectName;
};

class CueConditionObjInCurrentRoom: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String objectName;
};

class CueConditionObjInInv: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	ATL::String objectName;
};

// This cue condition always returns false.
class CueConditionFalse: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);
};

// Logical not.  Return the opposit of another cue.
class CueConditionNot: public CueCondition
{
public:
	virtual bool evaluate (void);

	virtual ATL::String getName (void);

	virtual ATL::String parseFromString (const ATL::String& parameters);

protected:
	CueCondition_ptr conditionToProcess;
};