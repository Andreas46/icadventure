#pragma once

#include <AtlString.h>

#include "Cue.h"

class CueMoveObject: public Cue
{
public:
	CueMoveObject (void);
	virtual ~CueMoveObject (void);
	
	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	bool moveToInv;
	ATL::String tractableTarget;

	virtual void parseStoredCueString (void);
};
