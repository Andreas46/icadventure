#include "ActionUse.h"

#include "DebugLog.h"

ActionUse::ActionUse (GameObject* parent): PlayerAction (parent)
{
	actionName = "use";
	actionUid = actionName;
	
	actionAliases.push_back ("operate");
	actionAliases.push_back ("actuate");
	actionAliases.push_back ("manipulate");
}

ActionUse::ActionUse (const ActionUse& rhs, GameObject* newParent): PlayerAction (rhs)
{
	parentObject = newParent;
}

ActionUse::~ActionUse (void)
{
	
}

void ActionUse::addTargetFromKeyVal (const KeyValList_t params)
{
	
}

ParseResult ActionUse::attemptParse (ATL::String command)
{
	ParseResult result;

	result = extractVerbParams (command);

	if (result.parseSuccessfull == true)
	{
		return evaluateActionTargetsFromString (result.returnText, result.verb);
	}
	else
	{
		return ParseResult (false, "");
	}
}

// bool ActionUse::attemptParseProperty (const ATL::String& property,
// 									const ATL::String& value)
// {
// 	return false;
// }

PlayerAction_ptr ActionUse::copy (GameObject* newParent) const
{
	return std::make_shared <ActionUse> (*this, newParent);
}

// bool ActionUse::evaluateTargetState (ActionTarget target,
// 									  ATL::String& retText,
// 									  ATL::String& failString,
// 									  const ATL::String& verb)
// {
// 	// In order to discriminate actions such as "use door" from use-with
// 	// actions such as "use key with door", we are going to enforce a
// 	// maximum on the number of words in the command
// }

bool ActionUse::evaluateTargetFromString (ActionTarget target,
										  const ATL::String& fromString,
										  ATL::String& retText,
										  ATL::String& failString,
										  const ATL::String& verb)
{
	// In order to discriminate actions such as "use door" from use-with
	// actions such as "use key with door", we are going to enforce a
	// maximum on the number of words in the command.  Specifically,
	// there shall be no more that the target, and the command for
	// these targets to be considered.

	if (fromString.length() != target.getTargetNameInString (fromString).length())
	{
// 		LOG ("Skipping target with length: ",
// 			 (double)(target.target.length() + verb.length() + 1));
		
		LOGD ("Skipping use target '", target.getTargetNameInString (fromString) +
			"' matched against '" + fromString + "'.");
		
		return false;
	}
	
	return PlayerAction::evaluateTargetFromString (
		target, fromString, retText, failString, verb);
}

// virtual bool attemptParseProperty (const ATL::String& property,
// 									   const ATL::String& value)
// {
// 	
// }
