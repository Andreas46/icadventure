#pragma once

#include "Cue.h"

class CueSetBrief: public Cue
{
public:
	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	ATL::String tracName;
	ATL::String newBrief;

	virtual void parseStoredCueString (void);
};
