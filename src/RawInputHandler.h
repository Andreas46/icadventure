#pragma once

#include <list>
//#include <memory>

class RawInputHandler;

//typedef std::shared_ptr <RawInputHandler> RawInputHandler_ptr;

typedef std::list <RawInputHandler*> RawInputHandlerList_t;

class RawInputHandler
{
public:
	/**
	* @return User returns true to allow character to fall down to lower character handlers.
	**/
	virtual bool handleChar (int c) = 0;
//	virtual void update (void);

	virtual int getChar (void) = 0;
};

class RawInputManager
{
public:
	virtual void pushHandler (RawInputHandler* handler);
	virtual RawInputHandler* popHandler (void);
	virtual void removeHandler (RawInputHandler* handler);

	virtual void checkForInput (void);

protected:
	RawInputHandlerList_t inputHandlers;

	virtual void handleChar (int c);
};
