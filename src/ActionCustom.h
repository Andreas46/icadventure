#pragma once

#include <memory>

#include "PlayerAction.h"

class ActionCustom;

typedef std::shared_ptr <ActionCustom> ActionCustom_ptr;

class ActionCustom: public PlayerAction
{
public:
	static PlayerAction_ptr newObjectFromParams (KeyValList_t params, GameObject* parent);

	ActionCustom (GameObject* gameObject);
 	ActionCustom (const ActionCustom& rhs, GameObject* newParent);
	~ActionCustom (void);
	
	virtual ParseResult attemptParse (ATL::String command);
	
	virtual void addTargetFromKeyVal (const KeyValList_t params);
	
	virtual bool attemptParseProperty (const ATL::String& property,
									   const ATL::String& value);
	
	virtual PlayerAction_ptr copy (GameObject* newParent) const;

protected:
	PlayerAction_ptr baseAction;
	
	ATL::String baseActionOriginalName;
};
