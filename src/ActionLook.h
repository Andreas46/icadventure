#pragma once

#include <AtlString.h>
#include <list>

#include "Cue.h"
#include "PlayerAction.h"

//class LookTarget
//{
//public:
//	
//
//	LookTarget (void);
//};

class Look: public PlayerAction
{
public:
	Look (GameObject* gameObject);
	Look (const Look& rhs, GameObject* parentObject);
	virtual ~Look (void);

	//void addLookTarget (ATL::String target, ATL::String parentObjCondition, ATL::String returnText, std::list <Cue> execActions);

	virtual ParseResult attemptParse (ATL::String command);
	
	virtual void addTargetFromKeyVal (const KeyValList_t params);

	virtual PlayerAction_ptr copy (GameObject* newParent) const;

protected:
	// Key: Dependant state; Value: the look string.
	//std::map <ATL::String, ATL::String> lookStateMap;
	
	//virtual bool attemptParseProperty (const ATL::String& property,
	//								   const ATL::String& value);
};

