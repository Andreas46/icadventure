#pragma once

#include "PlayerAction.h"

#include "ForwardDeclarations.h"

class ActionPickup: public PlayerAction
{
public:
	ActionPickup (GameObject* parent);
	ActionPickup (const ActionPickup& rhs, GameObject* parent);
	~ActionPickup (void);

	virtual ParseResult attemptParse (ATL::String command);

	virtual void addTargetFromKeyVal (const KeyValList_t params);

//	virtual bool attemptParseProperty (const ATL::String& property,
//										const ATL::String& value);

	virtual PlayerAction_ptr copy (GameObject* newParent) const;
protected:

};
