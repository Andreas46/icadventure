#include "ActionCustom.h"

#include "DebugLog.h"
#include "ActionLook.h"
#include "ActionUseWith.h"

PlayerAction_ptr ActionCustom::newObjectFromParams (KeyValList_t params, GameObject* parent)
{
	KeyValList_t::iterator itr;
	ATL::String verbName;
	ATL::String isLikeName;
	ATL::String defaultText;
	//ActionAliasList_t aliasList;

	//Tractable_ptr newTractable = std::make_shared <Tractable>();
	ActionCustom_ptr newAction = std::make_shared <ActionCustom> (parent);

	for (itr = params.begin (); itr != params.end (); ++itr)
	{
		if (itr->first == "name")
		{
			verbName = itr->second;
			newAction->actionName = verbName;

			// If a distinct uid has not been specified for this verb,
			// use the verbs name instead.
			if (newAction->actionUid.empty())
			{
				newAction->actionUid = verbName;
			}
		}
		else if (itr->first == "uid")
		{
			newAction->actionUid = itr->second;
		}
		else if (itr->first == "alias")
		{
			newAction->actionAliases.push_back (itr->second);
		}
		else if (itr->first == "is_like")
		{
			isLikeName = itr->second;
			newAction->baseActionOriginalName = isLikeName;
		}
		else if (itr->first == "fail_text")
		{
			LOG ("add fail test:", itr->second);
			defaultText = itr->second;
		}
		else
		{
			ATL::String str;
			str << "Failed to parse Custom Action Param: "
				<< itr->first << ": " << itr->second;
			DebugLog::log (str, DebugLog::LevelWarning);
		}
	}
	
	// Check for required parameters.
	ASSERT (!verbName.empty(), "Missing custom verb name!");
	ASSERT (!isLikeName.empty(), "Missing custom verb 'is_like' property!");
	
	// Determine the base action to be used.
	if (isLikeName == "look")
	{
		newAction->baseAction = std::make_shared <Look> (parent);
	}
	else if (isLikeName == "use_with")
	{
		newAction->baseAction = std::make_shared <UseWith> (parent);
	}
	else
	{
		FAIL (ATL::String ("Can't use Action '") + isLikeName + "' as base; Not handled!");
	}

	// Set the action name for the base Action.
	newAction->baseAction->actionName = newAction->actionName;
	
	newAction->baseAction->actionUid = newAction->actionUid;
	
	// Set the "fail text" for the base Action.
	newAction->baseAction->badTargetFailString = defaultText;
	//newAction->badTargetFailString = defaultText;

	// Apply the correct action aliases to the base action.
	newAction->baseAction->actionAliases = newAction->actionAliases;

	//LOG ("Adding custom verb; name '", newAction->actionName + "' with base name: " + newAction->baseAction->actionName);
	
	//LOG ("Uid '", newAction->actionUid + "' with base uid: " + newAction->baseAction->actionUid);

	return newAction;
}

ActionCustom::ActionCustom (GameObject* gameObject): PlayerAction (gameObject)
{
	
}

ActionCustom::ActionCustom (const ActionCustom& rhs, GameObject* newParent):
	PlayerAction (rhs)
{
	parentObject = newParent;

	baseAction = rhs.baseAction->copy (newParent);

	//baseActionOriginalName = rhs.baseActionOriginalName;
	//baseAction->badTargetFailString = rhs.badTargetFailString;
}

ActionCustom::~ActionCustom (void)
{
	
}

ParseResult ActionCustom::attemptParse (ATL::String command)
{
	return baseAction->attemptParse (command);
}

void ActionCustom::addTargetFromKeyVal (const KeyValList_t params)
{
	
}

bool ActionCustom::attemptParseProperty (const ATL::String& property,
								   const ATL::String& value)
{
	return baseAction->attemptParseProperty (property, value);
}

PlayerAction_ptr ActionCustom::copy (GameObject* newParent) const
{
//	LOG ("Fail String1: ", badTargetFailString);
//	LOG ("Fail String2: ", baseAction->badTargetFailString);

	ActionCustom_ptr newAction = std::make_shared <ActionCustom> (*this, newParent);

//	LOG ("Fail String3: ", badTargetFailString);
//	LOG ("Fail String4: ", baseAction->badTargetFailString);
//	LOG ("Fail String5: ", newAction->badTargetFailString);
//	LOG ("Fail String6: ", newAction->baseAction->badTargetFailString);

	return newAction;
}
