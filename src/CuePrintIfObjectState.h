#pragma once

#include "Cue.h"
#include "GameObject.h"

class CuePrintIfObjectState: public Cue
{
public:

protected:
	GameObject_ptr objectToTest;
	ATL::String requiredState;
	ATL::String testToPrint;
};

