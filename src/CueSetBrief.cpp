#include "CueSetBrief.h"

#include "GameManager.h"
#include "Tractable.h"

bool CueSetBrief::execute (void)
{
	Tractable_ptr tractable;

	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}
	
	LOG ("CueSetBrief: setting obj '", tracName + "' brief to: " + newBrief);
	
	// Attempt to locate the tractable in the game scene.
	tractable = GMGR.getTractableFromCurrentContext (tracName);
	
	tractable->setBriefText (newBrief);
	
	return true;
}

ATL::String CueSetBrief::getName (void)
{
	return "set_obj_brief";
}

void CueSetBrief::parseStoredCueString (void)
{
	// Extract name of cue condition.
	tracName = unparsedCueParameters.getNextWord ();

	ASSERT_S (!tracName.empty(),
			  "CuePrintIf: no condition specified in String", unparsedCueParameters);

	newBrief = unparsedCueParameters.subStr (tracName.length() + 1);
}
