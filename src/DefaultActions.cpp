#include "DefaultActions.h"

#include "DebugLog.h"
#include "FileParserDefault.h"
#include "GameManager.h"

DefaultResponse_ptr DefaultResponse::newResponseFromParams (KeyValList_t params)
{
	KeyValList_t::iterator itr;
	DefaultResponse_ptr newResponse = std::make_shared <DefaultResponse> ();

	for (itr = params.begin (); itr != params.end (); ++itr)
	{
		if (itr->first == "verb")
		{
			newResponse->matchToVerbs.push_back (itr->second);
		}
		else if (itr->first == "target_wrong")
		{
			newResponse->badResponses.push_back (itr->second);
		}
		else if (itr->first == "target_empty")
		{
			newResponse->emptyResponses.push_back (itr->second);
		}
		else
		{
			ATL::String str;
			str << "Failed to parse Default Response Param: " << itr->first <<
				": " << itr->second;
			DebugLog::log (str, DebugLog::LevelWarning);
		}
	}
	
	return newResponse;
}

DefaultResponse_ptr DefaultResponse::newFuzzyResponseFromParams(KeyValList_t params)
{
	KeyValList_t::iterator itr;
	DefaultResponse_ptr newResponse = std::make_shared <DefaultResponse> ();
	
	newResponse->isFuzzy = true;

	for (itr = params.begin (); itr != params.end (); ++itr)
	{
		if (itr->first == "match")
		{
			newResponse->matchToVerbs.push_back (itr->second);
		}
		else if (itr->first == "response")
		{
			newResponse->emptyResponses.push_back (itr->second);
		}
		else
		{
			ATL::String str;
			str << "Failed to parse fuzzy Default Response param: " << itr->first <<
				": " << itr->second;
			DebugLog::log (str, DebugLog::LevelWarning);
		}
	}
	
	return newResponse;
}

DefaultResponse::DefaultResponse (void)
{
	isFuzzy = false;
}

DefaultResponse::~DefaultResponse (void)
{
	
}

ParseResult DefaultResponse::attemptParse (ATL::String command)
{
	ParseResult result;
	StringVector_t::iterator itr;
//	StringVector_t::iterator endItr;
	StringVector_t* responsePool;
	
// 	if (command.find (' ') >= 0)
// 	{
// 		// Multiple words in command. We're looking for a badCommand response.
// 		responsePool = &badResponses;
// 	}
// 	else
// 	{
// 		// Single word.  Use empty responses.
// 		responsePool = &emptyResponses;
// 	}
// 	
// 	// Check that we have a response in the specified pool.
// 	if (responsePool->empty())
// 	{
// 		LOG ("No response available; Skipping default response for: ", command);
// 		return result;
// 	}
	
	for (itr = matchToVerbs.begin(); itr != matchToVerbs.end(); ++itr)
	{
		ATL::String verbOperands;
		bool isMatch;
		int64_t verbPos;

		if (isFuzzy == false)
		{
			// Attempt to identify the verb used.
			verbPos = command.subStr (0, itr->length ()).find (*itr);
			isMatch = verbPos >= 0;
		}
		else
		{
			// Attempt to find the verb anywhere (as a word) in command.
			verbPos = command.findWordPosInString (*itr);
			isMatch = verbPos >= 0;
		}
		
		if (isMatch)
		{
			if (verbPos + itr->length() < command.length())
			{
				verbOperands = command.subStr ((size_t) (verbPos + itr->length() + 1));
			}
			else
			{
				verbOperands.clear();
			}

			// Set the found verb, and operands as a story variable.
			PlayerAction::setStoryVariable ("_last_verb_", *itr);
			PlayerAction::setStoryVariable ("_last_objects_", verbOperands);

			if (command.length() == itr->length())
			{
				// Single word/phrase.  Use empty responses.
				responsePool = &emptyResponses;
			}
			else
			{
				// Additional stuff to word/phrase.
				responsePool = &badResponses;
			}
			
			// Check that we have a response in the specified pool.
			if (responsePool->empty())
			{
				LOG ("No response available; Skipping default response for: ", command);
				return result;
			}
	
			result.verb = *itr;
			result.parseSuccessfull = true;
			
			// Select a random response.
			result.returnText = getRandomResponse (*responsePool);
		}
	}
	
	return result;
}

void DefaultResponse::setFuzzy (const bool fuzzy)
{
	isFuzzy = fuzzy;
}

ATL::String DefaultResponse::getRandomResponse (const StringVector_t& responsePool)
//ATL::String DefaultResponse::getRandomResponse (void);
{
	int64_t responseIndex = 0;
	ASSERT (!responsePool.empty(), "Can't get random response; pool empty!");
	
	// Get a random number.
	responseIndex = GMGR.getRandomNumber (0, responsePool.size() - 1);
	
	return responsePool [(size_t) responseIndex];
}

// --- Default Actions --- //

DefaultActions::DefaultActions (void)
{

}

DefaultActions::~DefaultActions (void)
{

}

void DefaultActions::loadResponsesFromFile (ATL::String file)
{
	FileParserDefault fileParser;
	DefaultActionLists defActList;
	
	defActList = fileParser.parseDefaultFile (file);
	
	defaultResponses = defActList.defaultResponses;
	emptyObjects = defActList.emptyObjects;
	actions = defActList.playerActions;
	badCommandResponses = defActList.badCommandResponses;
	
	LOGV ("Loaded ", (double) defaultResponses.size() + " default responses.");
}

bool DefaultActions::isTractableInScope (const ATL::String& objName) const
{
	TractableList_t::const_iterator itr;
	
	for (itr = emptyObjects.begin(); itr != emptyObjects.end(); ++itr)
	{
		if ((*itr)->hasName (objName))
		{
			return true;
		}
	}
	
	return false;
}

Tractable_ptr DefaultActions::getTractable (ATL::String objName)
{
	TractableList_t::const_iterator itr;
	
	// Find out which tractable this names belongs to.
	for (itr = emptyObjects.begin(); itr != emptyObjects.end(); ++itr)
	{
		if ((*itr)->hasName (objName))
		{
			return *itr;
		}
	}

	// Didn't find tractable?
	FAIL_S ("Unable to get Inv Tractable: ", objName);
}

ParseResult DefaultActions::attemptParse (ATL::String command)
{
	DefaultResponseList_t::iterator itr;
	ParseResult result;
	ATL::String failText;
	
	// Try the default tractables.
	TractableList_t::iterator tracItr;
	
	for (tracItr = emptyObjects.begin(); tracItr != emptyObjects.end(); ++tracItr)
	{
		LOG ("Attempting empty obj: ", (*tracItr)->getName());
		result = (*tracItr)->attemptParse (command);
		
		// Did we perform a successful parse?
		if (result.parseSuccessfull == true)
		{
			return result;
		}
		else if (failText.empty() && !result.returnText.empty())
		{
			failText = result.returnText;
		}
	}

	// Look through default responses for an appropriate response.
	for (itr = defaultResponses.begin(); itr != defaultResponses.end(); ++itr)
	{
// 		if (command == "use")
// 		{
// 			int someVar = 0;
// 		}

		result = (*itr)->attemptParse (command);

		// Did we perform a successful parse?
		if (result.parseSuccessfull == true)
		{
			LOG ("Using default response to: ", result.verb);
			return result;
		}
	}
	
	return ParseResult (false, failText);
}

ATL::String DefaultActions::getBadCommandResponse (void)
{
	int64_t responseIndex = 0;
	ASSERT (!badCommandResponses.empty(), "badCommandResponses empty! How bad!?");
	
	// Get a random number.
	responseIndex = GMGR.getRandomNumber (0, badCommandResponses.size() - 1);
	
	return badCommandResponses [(size_t) responseIndex];

}

ATL::String DefaultActions::getParserName (void) const
{
	return "default";
}
