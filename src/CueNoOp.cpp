#include "CueNoOp.h"

bool CueNoOp::execute (void)
{
	// This Cue does nothing. |:-[
	return false;
}

ATL::String CueNoOp::getName (void)
{
	return "no_op";
}

void CueNoOp::parseStoredCueString (void)
{

}