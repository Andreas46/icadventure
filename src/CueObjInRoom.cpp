#include "CueObjInRoom.h"


CueObjInRoom::CueObjInRoom ()
{
}


CueObjInRoom::~CueObjInRoom ()
{
}

bool CueObjInRoom::execute (void)
{

}

ATL::String CueObjInRoom::getName (void)
{

}


void CueObjInRoom::parseStoredCueString (void)
{
	// Format: %()

	if (hasCueBeenParsed)
	{
		// Already done this...
		return;
	}

	// Extract the variable name.
	varName = unparsedCueParameters.getNextWord();

	ASSERT (!varName.empty(), "CueSetVar: VarName must not be empty!");
	
	// Next we get the new state.
	newVarValue = unparsedCueParameters.getNextWord (varName.length());
	
	// Setting empty variables will be O.K.
	if (newVarValue.empty())
	{
		LOGW ("CueSetVar: Variable '", varName + "' is being set to empty!");
	}
}
