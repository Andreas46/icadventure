#pragma once

#include "PlayerAction.h"

#include <memory>

class ActionUse;

typedef std::shared_ptr <ActionUse> ActionUse_ptr;

class ActionUse: public PlayerAction
{
public:
	ActionUse (GameObject* parent);
	ActionUse (const ActionUse& rhs, GameObject* newParent);
	~ActionUse (void);

	void addTargetFromKeyVal (const KeyValList_t params);

	ParseResult attemptParse (ATL::String command);
	
// 	virtual bool attemptParseProperty (const ATL::String& property,
// 										const ATL::String& value);

	virtual PlayerAction_ptr copy (GameObject* newParent) const;
	
protected:
// 	virtual bool evaluateTargetState (ActionTarget target,
// 									  ATL::String& retText,
// 									  ATL::String& failString,
// 									  const ATL::String& verb);

virtual bool evaluateTargetFromString (ActionTarget target,
									   const ATL::String& fromString,
									   ATL::String& retText,
									   ATL::String& failString,
									   const ATL::String& verb);
};