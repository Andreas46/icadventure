#include "ActionLook.h"

#include "DebugLog.h"

Look::Look (GameObject* gameObject): PlayerAction (gameObject)
{
	actionName = "look";
	actionUid = actionName;
	
	actionAliases.push_back ("observe");
	actionAliases.push_back ("stare");
	actionAliases.push_back ("gawk");
	actionAliases.push_back ("leer");
	actionAliases.push_back ("inspect");
	actionAliases.push_back ("examine");
	
	// Spelling mistakes.
	actionAliases.push_back ("loo");
}

Look::Look (const Look& rhs, GameObject* newObject): PlayerAction (rhs)
{
	parentObject = newObject;
}

Look::~Look (void)
{
}

ParseResult Look::attemptParse (ATL::String command)
{
	ParseResult result;

	result = extractVerbParams (command);
	
	//LOG ("Extracted verb: ", result.verb);

	if (result.parseSuccessfull == true)
	{
		return evaluateActionTargetsFromString (result.returnText, result.verb);
	}
	else
	{
		return ParseResult (false, "");
	}
}

void Look::addTargetFromKeyVal (const KeyValList_t params)
{
	
}

PlayerAction_ptr Look::copy (GameObject* newParent) const
{
	return std::make_shared <Look> (*this, newParent);
}

// bool Look::attemptParseProperty (const ATL::String& property,
// 								 const ATL::String& value)
// {
// 	if (property == actionName)
// 	{
// 		//addTarget (parentObject->getName(), value);
// 		addObjTarget (value);
// 		//DebugLog::log (ATL::String ("Adding look object: ") + actionName + " print text: " + value);
// 	}
// 	else if (property == actionName + "_state")
// 	{
// 		StringVector_t vals = splitValueString (value, 2);
// 		
// 		//addTarget (parentObject->getName(), vals [0], vals [1]);
// 		addObjTarget (vals [1], vals[0]);
// 	}
// 	else
// 	{
// 		return false;
// 	}
// 	
// 	return true;
// }
