#pragma once

#include "Cue.h"

class CueSetRoomState: public Cue
{
public:
    virtual bool execute (void);
    virtual ATL::String getName (void);
    
protected:
	ATL::String roomName;
	ATL::String newState;
	
    virtual void parseStoredCueString (void);
};