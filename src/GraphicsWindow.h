#pragma once

#include <AtlString.h>

#include <curses.h>
#include <panel.h>

#include "ForwardDeclarations.h"


#define WIN_PADDING_X 2
#define WIN_PADDING_Y 1
#define EFFECTIVE_WIN_X (sizeX > (2 * WIN_PADDING_X) ? sizeX - (2 * WIN_PADDING_X) : 0)
#define EFFECTIVE_WIN_Y (sizeY - (2 * WIN_PADDING_Y))

class GraphicsWindow
{
public:
	GraphicsWindow (void);
	virtual ~GraphicsWindow (void);

	unsigned posX;
	unsigned posY;
	
	unsigned sizeX;
	unsigned sizeY;
	
	virtual void setSize (unsigned newPosX, unsigned newPosY, unsigned newSizeX, unsigned newSizeY);
	
	void drawBoarder (void);
	
	virtual void addText (ATL::String text);
	void clearText (void);
	void setText (ATL::String text);

	virtual void update (void);
	
	virtual void setHidden (bool hidden);

	WINDOW* getWindow (void);

protected:

	ATL::String winText;
	bool isHidden;

	// Curses variables.
	WINDOW* cursWin; // curses window.
	PANEL* cursPanel; // curses pannel.
	
	virtual StringVector_t getWindowLinesForText (const ATL::String& text);
	void drawText (void);
	void zeroLineRemainder (void);
};
