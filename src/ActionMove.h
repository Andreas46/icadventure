#pragma once

#include "PlayerAction.h"

#include <memory>

class ActionMove;

typedef std::shared_ptr <ActionMove> ActionMove_ptr;

class ActionMove: public PlayerAction
{
public:
	ActionMove (GameObject* parent);
	ActionMove (const ActionMove& rhs, GameObject* parent);
	virtual ~ActionMove (void);

//	static PlayerAction_ptr newObjectFromParams (KeyValList_t params, GameObject* parent);

	//void addMoveTargetFromKeyVal (const KeyValList_t params);
	void addTargetFromKeyVal (const KeyValList_t params);

	virtual bool evaluateTargetState (ActionTarget target,
									  ATL::String& retText,
									  ATL::String& failString,
									  const ATL::String& verb);

	ParseResult attemptParse (ATL::String command);
	
	//virtual bool attemptParseProperty (const ATL::String& property,
		//								const ATL::String& value);

	virtual PlayerAction_ptr copy (GameObject* newParent) const;
};


