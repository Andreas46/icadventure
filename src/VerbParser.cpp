#include "VerbParser.h"

#include "DebugLog.h"

ParseResult::ParseResult (void)
{
	parseSuccessfull = false;
}

ParseResult::ParseResult (bool parseSucc,
			 ATL::String retText,
			 ATL::String target,
			 ATL::String verb)
{
	this->parseSuccessfull = parseSucc;
	this->returnText = retText;
	this->target = target;
	this->verb = verb;
}

VerbParser::VerbParser (void)
{
	
}

VerbParser::~VerbParser (void)
{
	
}

bool VerbParser::hasAction (ATL::String actionUid) const
{
	PlayerActionList_t::const_iterator itr;

	for (itr = actions.begin(); itr != actions.end(); ++itr)
	{
		if ((*itr)->getUid() == actionUid)
		{
			return true;
		}
	}
	
	return false;
}

void VerbParser::addAction (PlayerAction_ptr action)
{
	ASSERT_S (hasAction (action->getUid()) == 0,
		"Verb parser already has action with id: ", action->getUid());
	
	//LOG ("Adding action :", action->getUid());
 
	actions.push_back (action);
}

PlayerAction_ptr VerbParser::getAction (const ATL::String& actionUid) const
{
// 	ASSERT_S (actions.count (actionName) > 0,
// 		"Parser does not have action '", actionName + "'.");
	
	PlayerActionList_t::const_iterator itr;

	for (itr = actions.begin(); itr != actions.end(); ++itr)
	{
		if ((*itr)->getUid () == actionUid)
		{
			return *itr;
		}
	}
	
	FAIL_S ("Parser does not have action '", actionUid + "'.");
}
