#include "GraphicsPopUpWindow.h"

#include "GameManager.h"
#include "Graphics.h"

#include <fstream>

PopUpSet_t GraphicsPopUpWindow::activePopUps;

PopUp_ptr GraphicsPopUpWindow::newPopUp (const ATL::String& text,
										 unsigned width,
										 bool centreJustified)
{
	PopUp_ptr newPopUp = std::make_shared <GraphicsPopUpWindow> ();
	
	newPopUp->setWidth (width);
	newPopUp->setCentreJustified (centreJustified);
	
	newPopUp->setText (text);
	
	activePopUps.insert (newPopUp);

	return newPopUp;
}

GraphicsPopUpWindow::GraphicsPopUpWindow (void)
{
	// Default starting width.
	sizeX = 30;
	sizeY = 1;

	isTextCentreJustified = true;

	currentPage = 0;
//	numPages = 0;
//	pageOffset = 0;
//	minPageHeight = 0;
	fixedWinHeight = 0;

	prompt = "[press space to continue]";

	recalculateDimensions ();
	
	// Register input handler.
	PUSH_IN_HANDLER (this);
	
// 	activePopUps.insert (shared_from_this());
}

GraphicsPopUpWindow::~GraphicsPopUpWindow (void)
{
	REMOVE_IN_HANDLER (this);
}

void GraphicsPopUpWindow::setSize (unsigned newPosX, unsigned newPosY, unsigned newSizeX, unsigned newSizeY)
{
	posX = newPosX;
	posY = newPosY;

	sizeX = newSizeX;
	sizeY = newSizeY;

//	GraphicsWindow::setSize ();
	recalculateDimensions ();
}

void GraphicsPopUpWindow::setWidth (const unsigned newWidth)
{
	setSize (1,1,newWidth,1);
}

void GraphicsPopUpWindow::setFixedWinHeight (const int height)
{
	fixedWinHeight = height;
}

int GraphicsPopUpWindow::getPageHeight (void)
{
	int height;
	
	if (fixedWinHeight > 0 && fixedWinHeight < GET_SCRN_Y - 2 * WIN_PADDING_Y - 2)
	{
		height = fixedWinHeight - 3 - (WIN_PADDING_Y * 2);
	}
	else
	{
		height = MAX_PAGE_HEIGHT;
	}
	
	return height;
}

// bool GraphicsPopUpWindow::rawInputHandler (int c)
// {
// 	return false;
// }

int GraphicsPopUpWindow::getChar (void)
{
	//return wgetch (cursWin);
	return getch();
}

bool GraphicsPopUpWindow::handleChar (int c)
{
	// Handle multiple pages.
	if ((unsigned) (currentPage + 1) < getNumPages())
	{
		++currentPage;
		clear();
		//recalculateDimensions ();
		generatePages();
		recalculateDimensions ();
		drawText();
		Graphics::refreshScreen();
		return false;
	}


	if (isHidden == false)
	{
		setHidden (true);
		return false;
	}
	else
	{
		return true;
	}
}

void GraphicsPopUpWindow::recalculateDimensions (void)
{
	int numLines;
	int newWinHeight;
	int newWinWidth = sizeX;

	// Minimum height for "getWindowLinesForText" to return correct text.
	sizeY = 3;

	//Verify that x size is not too large.
	if (newWinWidth > GET_SCRN_X - 2 * WIN_PADDING_X)
	{
		newWinWidth = GET_SCRN_X - 2 * WIN_PADDING_X;
		sizeX = newWinWidth;  // This stopps crashings.
	}

	// Calculate X position.
	posX = (GET_SCRN_X - sizeX) / 2;

	if (fixedWinHeight > 0)
	{
		newWinHeight = fixedWinHeight;
	}
	else
	{
		// Calculate the new Y size that this window will be.
		numLines = getWindowLinesForText (winText).size ();// -MAX_PAGE_HEIGHT * currentPage;

		newWinHeight = numLines + (WIN_PADDING_Y * 2);
	}
	
	// Make sure we meet minimum height requirements if this parameter is set.
// 	if (fixedWinHeight > 0 &&
// 		//minPageHeight < MAX_PAGE_HEIGHT &&
// 		numLines < minPageHeight)
// 	{
// 		newWinHeight = minPageHeight + (WIN_PADDING_Y * 2);
// 	}

	//LOGW ("New win height: ", newWinHeight);
	//LOGW ("numLines: ", newWinHeight);
	//LOGW ("offset: ", pageOffset);

	// Resize window if it exceeds maximum height.
	if (newWinHeight > GET_SCRN_Y - 2 * WIN_PADDING_Y - 2)
	{
		newWinHeight = GET_SCRN_Y - 2 * WIN_PADDING_Y - 2;
		//LOGW ("New win height: ", newWinHeight);
	}

	// Calculate newYpos.
	posY = (GET_SCRN_Y - newWinHeight) / 2;

	// Set window height.
	GraphicsWindow::setSize (posX, posY, newWinWidth, newWinHeight);

	//GraphicsWindow::setSize (2, 2, newWinWidth, newWinHeight);

	Graphics::getInstance ().refreshScreen ();
}

void GraphicsPopUpWindow::addTextFromFile (ATL::String textFilePath)
{
	std::ifstream fileStream (textFilePath.cStr());

	if (fileStream.is_open())
	{
		char buff [51];
		
		while (fileStream.get (buff, 50, EOF))
		{
			addText (buff);

			if (fileStream.eof())
			{
				break;
			}
		}

		fileStream.close();
	}
	else
	{
		FAIL ("Failed to open popup text file '" + textFilePath + "'.");
	}
}

void GraphicsPopUpWindow::setCentreJustified (const bool justified)
{
	isTextCentreJustified = justified;
}

void GraphicsPopUpWindow::setHidden (bool hidden)
{
	//GraphicsWindow::setHidden (hidden);
	
	if (activePopUps.count (shared_from_this()))
	{
		activePopUps.erase (shared_from_this());
	}
}


void GraphicsPopUpWindow::addText (ATL::String text)
{
//	size_t numLines;
//	size_t newWinHeight;

	GraphicsWindow::addText (text);

	generatePages();

	recalculateDimensions ();

	//// Calculate the new Y size that this window will be.
	//numLines = getWindowLinesForText (winText).size();
	//newWinHeight = numLines + (WIN_PADDING_Y * 2);

	//// Set window height.
	//GraphicsWindow::setSize (posX, posY, newWinHeight, sizeY);
}

void GraphicsPopUpWindow::update (void)
{
	wrefresh (cursWin);
}

ATL::String& GraphicsPopUpWindow::centreJustifyText (ATL::String& text)
{
	if (text.length() < EFFECTIVE_WIN_X)
	{
		ATL::String padding;
		padding.padToNChars ((EFFECTIVE_WIN_X - text.length()) / 2);

		text.prepend (padding);
	}
	
	return text;
}

void GraphicsPopUpWindow::pushPage (StringVector_t page)
{
	ATL::String centeredPrompt;
	size_t leftPromptPadLength = 0;

	StringVector_t textLines;

	// Prompt is always centred.
	if (prompt.length () > 0 &&
		EFFECTIVE_WIN_X > (prompt.length ()) &&
		!isTextCentreJustified &&
		!prompt.empty())
	{
		leftPromptPadLength = (EFFECTIVE_WIN_X / 2) - (prompt.length() / 2) ;

		// left pad Str.
		centeredPrompt.padToNChars (leftPromptPadLength);
	}
	centeredPrompt += prompt;
	
	// Pad the page lines if a minimum height has been set.
	if (fixedWinHeight > 0)
	{
		while ((signed) page.size() < getPageHeight())
		{
			page.push_back ("");
		}
	}

	page.push_back ("");
	page.push_back (centeredPrompt);
	
	pages.push_back (page);
}

StringVector_t GraphicsPopUpWindow::getWindowLinesForText (const ATL::String& text)
{
// 	size_t leftPromptPadLength = 0;
// 	ATL::String centeredPrompt;
// 
// 	StringVector_t textLines;
// 
// 	// Prompt is always centred.
// 	if (prompt.length () > 0 &&
// 		EFFECTIVE_WIN_X > (prompt.length ()) &&
// 		!isTextCentreJustified &&
// 		!prompt.empty())
// 	{
// 		leftPromptPadLength = (EFFECTIVE_WIN_X / 2) - (prompt.length() / 2) ;
// 
// 		// left pad Str.
// 		centeredPrompt.padToNChars (leftPromptPadLength);
// 	}
// 	centeredPrompt += prompt;
// 
// 	//LOGW ("winText: ", winText);
// 
//  	textLines = GraphicsWindow::getWindowLinesForText (text);
// 
//  	// Calculate number of pages for text.
//  	//int pageSize = EFFECTIVE_WIN_Y - 3;
//  	numPages = (int) std::ceil ((double) textLines.size() / (double) MAX_PAGE_HEIGHT);
// 
// 	int startPos = MAX_PAGE_HEIGHT * currentPage;
// 
// 	if (startPos > 0)
// 	{
// 		pageOffset = startPos;
// 	}
// 	//int endPos
// 
//  	if (currentPage + 1 < numPages)  // +1 so this condition doesn't handle last page.
//  	{
// 		StringVector_t::iterator itr = textLines.begin () + startPos;
// 		std::advance (itr, MAX_PAGE_HEIGHT);
// 
//  		// Reform text lines as a subset of text lines that fits in given area.
//  		textLines = StringVector_t (textLines.begin() + startPos, itr);
// 
// 		//textLines.push_back ("");
// 		//textLines.push_back (centeredPrompt);
//  	}
// 	else if (numPages > 1 && startPos >= 0)
// 	{
// 		// Format last page.
// 		//StringVector_t::iterator itr = textLines.begin () + startPos;
// 		//std::advance (itr, pageSize);
// 
//  		// Reform text lines as a subset of text lines that fits in given area.
//  		textLines = StringVector_t (textLines.begin() + startPos, textLines.end());
// 
// 		//textLines.push_back ("");
// 		//textLines.push_back (centeredPrompt);
// 	}
// 
// 	textLines.push_back ("");
// 	textLines.push_back (centeredPrompt);
// 
// //	textLines = GraphicsWindow::getWindowLinesForText (text + "\n\n" + centeredPrompt);
// 
// 	if (isTextCentreJustified)
// 	{
// 		for (unsigned i = 0; i < textLines.size(); ++i)
// 		{
// 			if (textLines [i].length() < EFFECTIVE_WIN_X)
// 			{
// 				ATL::String padding;
// 				padding.padToNChars ((EFFECTIVE_WIN_X - textLines [i].length()) / 2);
// 
// 				textLines [i].prepend (padding);
// 			}
// 		}
// 	}

//	return textLines;

	return getNextPage();
}

StringVector_t GraphicsPopUpWindow::getNextPage (void)
{
	generatePages();

	if (pages.size() == 0)
	{
		return StringVector_t();
	}

	ASSERT (pages.size() > (size_t) currentPage, "Failed to get page " +
		currentPage + " in " + (double) pages.size() + " pages.");

	return pages [currentPage];
}

void GraphicsPopUpWindow::generatePages (void)
{
 	StringVector_t textLines;
	int pageSize;

	if (fixedWinHeight > 0)
	{
		pageSize = fixedWinHeight - 3;
	}
	else
	{
		pageSize = MAX_PAGE_HEIGHT;
	}

	pages.clear();

// 	// Prompt is always centred.
// 	if (prompt.length () > 0 &&
// 		EFFECTIVE_WIN_X > (prompt.length ()) &&
// 		!isTextCentreJustified &&
// 		!prompt.empty())
// 	{
// 		leftPromptPadLength = (EFFECTIVE_WIN_X / 2) - (prompt.length() / 2) ;
// 
// 		// left pad Str.
// 		centeredPrompt.padToNChars (leftPromptPadLength);
// 	}
// 	centeredPrompt += prompt;

	//LOGW ("winText: ", winText);

 	textLines = GraphicsWindow::getWindowLinesForText (winText);

 	// Calculate number of pages for text.
 	//int pageSize = EFFECTIVE_WIN_Y - 3;
 	//numPages = (int) std::ceil ((double) textLines.size() / (double) MAX_PAGE_HEIGHT);

// 	int startPos = pageSize * currentPage;
//
// 	if (startPos > 0)
// 	{
// 		pageOffset = startPos;
// 	}
	//int endPos

	//StringVector_t::iterator itr;
	StringVector_t nextPage;

	//for (itr = textLines.begin(); itr != textLines.end(); ++itr)
	for (unsigned i = 0; i < textLines.size(); ++i)
	{
		//LOGW ("Processing line '" + textLines [i] + "'.")
		// Look for page marker.
		if (textLines [i].find ("[page]") >= 0)
		{
			pushPage (nextPage);
			nextPage.clear();
			continue;
		}
		
		// Check to see if we've exceeded the page size.
		if (nextPage.size() >= (unsigned) pageSize)
		{
			pushPage (nextPage);
			nextPage.clear();
		}

		nextPage.push_back (textLines [i]);

		if (isTextCentreJustified)
		{
			centreJustifyText (nextPage.back());
		}
	}
	
	// append final page.
	if (!nextPage.empty())
	{
		pushPage (nextPage);
	}
}

size_t GraphicsPopUpWindow::getNumPages()
{
	return pages.size();
}

