#include "ActionEmpty.h"

#include "DebugLog.h"

ActionEmpty::ActionEmpty (GameObject* parent): PlayerAction(parent)
{
	actionName = "empty_action";
}

ActionEmpty::ActionEmpty (const ActionEmpty& rhs, GameObject* parent): PlayerAction (rhs)
{
	parentObject = parent;
}

ActionEmpty::~ActionEmpty (void)
{

}

ParseResult ActionEmpty::attemptParse (ATL::String command)
{
	ParseResult result;

	// Note: no verb used in empty action.
	return evaluateActionTargetsFromString (command, result.verb);

}

void ActionEmpty::addTargetFromKeyVal (const KeyValList_t params)
{

}

PlayerAction_ptr ActionEmpty::copy (GameObject* newParent) const
{
	return std::make_shared <ActionEmpty> (*this, newParent);
}

bool ActionEmpty::attemptParseProperty(const ATL::String& property, const ATL::String& value)
{
    if (property == "empty")
	{
		addTarget (parentObject->getName(), value);
	}
	else if (property == "empty_state")
	{
		StringVector_t vals = splitValueString (value, 2);
		
		addTarget (parentObject->getName(), vals [0], vals[1]);
		
		//DebugLog::log (ATL::String ("Adding empty target: ") + actionName + " print text: " + value);
	}
	else
	{
		return false;
	}
	
	return true;
}

