#include "Cue.h"

#include "CueDestoryObject.h"
#include "CueMoveTo.h"
#include "CueMoveObject.h"
#include "CueNoOp.h"
#include "CuePopUp.h"
#include "CuePopUpFromFile.h"
#include "CuePrintIf.h"
#include "CueSetBrief.h"
#include "CueSetRoomState.h"
#include "CueSetState.h"
#include "CueSetVar.h"
#include "CueShowJournal.h"
#include "CueSpawnObject.h"
#include "DebugLog.h"

std::set <Cue*> Cue::allTheCues;

Cue::Cue (void)
{
	hasCueBeenParsed = false;
	
	allTheCues.insert (this);
}

Cue::~Cue (void)
{
	allTheCues.erase (this);
}

void Cue::parseAllCues (void)
{
	std::set<Cue*>::iterator itr;
	
	for (itr = allTheCues.begin(); itr != allTheCues.end(); ++itr)
	{
		(*itr)->parseStoredCueString();
	}
}

CueTextSplit Cue::extractCueTextFromString (const ATL::String& src, const size_t startPos)
{
	int64_t cuePos;
	int64_t cueEndPos;
	//int64_t lastCueEndPos = 0;
	int64_t nextCuePos = 0;
	
	//ATL::String newString;
	ATL::String cueStr;
	
	CueTextSplit retTxtSplit;

	//CueList_t cueList;

	// Find the opening cue syntax.
	cuePos = src.find ("%(", startPos);
	if (cuePos >= 0)
	{
		// Append string up until this point.
		retTxtSplit.preCue = src.subStr (0, (size_t) (cuePos));

		cueEndPos = src.find (")", (size_t) cuePos);
		nextCuePos = src.find ("%(", (size_t) (cuePos + 2));
		
		// Hanldle nested cues.
		while (nextCuePos >= 0 &&
			   cueEndPos >=0 &&
			   nextCuePos < cueEndPos)
		{
			nextCuePos = src.find ("%(", (size_t) (nextCuePos + 2));
			// Find next end bracket.
			cueEndPos = src.find (")", (size_t) (cueEndPos + 1));
		}

		ASSERT_S (cueEndPos > 0, "Unterminated Cue in string: ",
			src + " At Position '" + (double)cuePos + "'.");
		
		retTxtSplit.cue = src.subStr ((size_t) (cuePos + 2), (cueEndPos - cuePos - 2));


		// The postion to start from for the next pass.
		//lastCueEndPos = cueEndPos + 1;

		//cuePos = src.find ("%(", lastCueEndPos);
		
		retTxtSplit.postCue = src.subStr ((size_t) (cueEndPos + 1));
	}
	else
	{
		retTxtSplit.preCue = src;
	}

	// Append remainder of string.
	//newString += src.subStr (lastCueEndPos);

	//src = newString;
	
	//return cueStr;
	
	return retTxtSplit;
}

Cue_ptr Cue::newCueFromString (ATL::String cueAsString, bool parseImidiately)
{
	ATL::String cueName;
	ATL::String cueParams;
	int cueLen;
	Cue_ptr retPtr;

	// Extract the first "word" of the string (should be the cue's name).
	cueLen = static_cast <int> (cueAsString.find (" "));

	if (cueLen == -1)
	{
		// Zero param string?
		cueName = cueAsString;
	}
	else
	{
		cueName = cueAsString.subStr (0, cueLen);
		cueParams = cueAsString.subStr (cueLen + 1);
	}

	LOG ("Parsing cue: ", cueAsString);

	if (cueName == "destroy_obj")
	{
		retPtr = std::make_shared <CueDestoryObject>();
	}
	else if (cueName == "move")
	{
		retPtr = std::make_shared <CueMoveTo>();
		//retPtr.addMoveTargetFromKeyVal ();
	}
	else if (cueName == "move_obj_to")
	{
		retPtr = std::make_shared <CueMoveObject>();
	}
	else if (cueName == "pop_up")
	{
		retPtr = std::make_shared <CuePopUp>();
	}
	else if (cueName == "pop_up_from_file")
	{
		retPtr = std::make_shared <CuePopUpFromFile>();
	}
	else if (cueName == "print_if")
	{
		retPtr = std::make_shared <CuePrintIf>();
	}
	else if (cueName == "set_obj_brief")
	{
		retPtr = std::make_shared <CueSetBrief>();
	}
	else if (cueName == "set_room_state")
	{
		retPtr = std::make_shared <CueSetRoomState>();
	}
	else if (cueName == "set_obj_state")
	{
		retPtr = std::make_shared <CueSetState>();
	}
	else if (cueName == "set_var")
	{
		retPtr = std::make_shared <CueSetVar>();
	}
	else if (cueName == "show_journal")
	{
		retPtr = std::make_shared <CueShowJournal>();
	}
	else if (cueName == "spawn")
	{
		retPtr = std::make_shared <CueSpawnObject>();
	}
	else
	{
		LOGE ("Failed to identify que '", cueName + "' in String: " + cueAsString);
		retPtr = std::make_shared <CueNoOp>();
	}

	retPtr->setParameters (cueParams);

	//unparsedCueString = cueAsString;

// 	if (parseImidiately)
// 	{
// 		parseStoredCueString();
// 	}
	return retPtr;
}

CueList_t Cue::extractAllCuesFromString (ATL::String& str,  const bool skipPrintIf)
{
	//int64_t cuePos;
//	int64_t cueEndPos;
//	int64_t lastCueEndPos = 0;
	ATL::String newString;
	//int64_t nextCuePos = 0;

	CueList_t cueList;
	
	ATL::String cueStr;

	CueTextSplit retTxtSplit;
	
	// Find the opening cue syntax.
//	cuePos = str.find ("%(");
	
	retTxtSplit = extractCueTextFromString (str);
	
	//while (cuePos >= 0)
	//while (!cueStr.empty())
	while (retTxtSplit.wasCueFound())
	{
		newString += retTxtSplit.preCue;
		// Append string up until this point.
		//newString += str.subStr (lastCueEndPos, (size_t) (cuePos - lastCueEndPos));

		//cueEndPos = str.find (")", cuePos);
		//nextCuePos = str.find ("%(", cuePos + 2);
		
		// Hanldle nested cues.
// 		while (nextCuePos >= 0 &&
// 			   cueEndPos >=0 &&
// 			   nextCuePos < cueEndPos)
// 		{
// 			nextCuePos = str.find ("%(", nextCuePos + 2);
// 			// Find next end bracket.
// 			cueEndPos = str.find (")", cueEndPos + 1);
// 		}
// 
// 		ASSERT_S (cueEndPos > 0, "Untermindated Cue in string: ",
// 			str + " At Position '" + (double)cuePos + "'.");
		
		//cueStr = str.subStr (cuePos + 2, (cueEndPos - cuePos - 2));

		// Ignore cues where return text will be split later.
		//if (str.getNextWord (cuePos + 2) == "print_if")
		if (skipPrintIf && retTxtSplit.cue.getNextWord() == "print_if")
		{
			// Add a marker for later substitution.
			//newString = newString + "%(" + cueStr + ")";
			newString = newString + "%(" + retTxtSplit.cue + ")";
			
			LOGV ("replacing string: ", newString);
		}
		else
		{
			// Extract found cue.
			Cue_ptr newCue = newCueFromString (retTxtSplit.cue);

			cueList.push_back (newCue);
		}

		// The postion to start from for the next pass.
		//lastCueEndPos = cueEndPos + 1;

		//cuePos = str.find ("%(", lastCueEndPos);
		
		retTxtSplit = extractCueTextFromString (retTxtSplit.postCue);
	}

// 	if (cueList.empty())
// 	{
// 		No cues were found.
// 	}
	// Append remainder of string.
	//newString += str.subStr (lastCueEndPos);
	newString += retTxtSplit.preCue;

	str = newString;

	return cueList;
}

void Cue::setParameters (const ATL::String& parameters)
{
	unparsedCueParameters = parameters;
}
