#pragma once

#include <AtlString.h>
#include <utility>
#include <list>
#include <map>
#include <memory>
#include <vector>

class Cue;
class CuePrintIf;

typedef std::shared_ptr <Cue> Cue_ptr;
typedef std::list <Cue_ptr> CueList_t;

typedef std::shared_ptr <CuePrintIf> CuePrintIf_ptr;

typedef std::pair <ATL::String, int> VerbSearchResult;
//typedef std::pair <bool, ATL::String> ParseResult;

typedef std::pair <ATL::String, ATL::String> KeyValPair_t;
typedef std::list <KeyValPair_t> KeyValList_t;

typedef std::vector <ATL::String> StringVector_t;
typedef std::vector <StringVector_t> StringVectorVector_t;
typedef std::list <ATL::String> StringList_t;
typedef std::list <ATL::String> NameList_t;

typedef std::pair <ATL::String, ATL::String> KeyValPair_t;
typedef std::list <KeyValPair_t> KeyValList_t;
typedef std::map <ATL::String, ATL::String> StringMap_t;

extern StringVector_t splitValueString (const ATL::String input, unsigned numSplits, char spiltChar = '|');
