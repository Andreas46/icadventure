#pragma once

#include <AtlString.h>
#include <list>

#include "RawInputHandler.h"

#define GET_SCRN_X (Graphics::getInstance().getScreenXsize())
#define GET_SCRN_Y (Graphics::getInstance().getScreenYsize())

class GraphicsWindow;

typedef void (*CommandHandler) (ATL::String params);
typedef std::list <ATL::String> StringList_t;

class Graphics: public RawInputHandler
{
public:
	static Graphics& getInstance (void);

	static void refreshScreen (void);

	Graphics (void);
	~Graphics (void);

	bool inputPromptEnabled;

	void addText (ATL::String text);
	void addTextLine (ATL::String text);
	void clearOuputText (void);

	void drawFrame (void);

	void leaveCursesMode (void);
	void restoreCursesMode (void);
	
	void setCommandHandler (CommandHandler handler);

	int getScreenXsize (void) const;
	int getScreenYsize (void) const;

	void resizeTerminal (int lines, int columns);
	
protected:
	static Graphics* instance;

	int screenXSize;
	int screenYSize;

	int inputCursorStartPosX;
	int inputCursorStartPosY;

	int numInputLines;
	unsigned int inputCursorPos;

	int outTextStartPosX;
	int outTextStartPosY;
	int outTextMaxWidth;
	int outTextMaxHeight;
	
	CommandHandler cmdHandler;

	//ATL::String inputLine;
	ATL::String outputText;
	
	StringList_t inputHistory;
	StringList_t::iterator currentInput;
	
	GraphicsWindow* outTextWin;
	GraphicsWindow* inputPromptWin;

	void initCurses (void);

	void updateScreenVars (void);

	void drawBoarder (void);

	void drawPrompt (void);

	void drawOutputText (void);

	void clearInputBox (void);

	//void getPromptInput (void);
	bool handleChar (int c);
	int getChar (void);
};

