#pragma once

#include "Cue.h"

class CueObjInRoom: public Cue
{
public:
	CueObjInRoom (void);
	virtual ~CueObjInRoom (void);

	virtual bool execute (void);
	virtual ATL::String getName (void);

protected:
	virtual void parseStoredCueString (void);
};

