#pragma once

#include <AtlString.h>
#include <list>
#include <memory>

#include "ForwardDeclarations.h"

class GameObject;

typedef std::shared_ptr <GameObject> GameObject_ptr;
typedef std::list <ATL::String> NameAliases_t;

class GameObject
{
public:
	GameObject (void);
	virtual ~GameObject (void);

	virtual ATL::String getState (void) const;
	virtual void setState (const ATL::String& newState);
	
	virtual ATL::String getName (void) const = 0;
	virtual NameList_t getAllNames (void) const;
	virtual bool hasName (ATL::String name) const;
	virtual bool isNameInStr (const ATL::String& inStr) const;

protected:
	NameAliases_t nameAliases;
	
	ATL::String objectState;
};

