#pragma once

#include <AtlString.h>
#include <list>

#include "ForwardDeclarations.h"

class ActionTarget;
class TextSource;

typedef std::list <ActionTarget> ActionTargetList_t;
typedef std::list <ATL::String> TargetAliasList_t;

typedef std::list <TextSource> TextSourceList_t;

class TextSource
{
public:
	TextSource (const ATL::String& initialiser);
	TextSource (const Cue_ptr initialiser);
	
	ATL::String getText (void) const;
	bool isStringSource (void) const;

protected:
	ATL::String stringSource;
	CuePrintIf_ptr cueSource;
};

class ActionTarget
{
public:
	ATL::String target;  // What is being look at.
	ATL::String parentObjCondition;  // The necessary state for this Target to be selected (can be empty).
	ATL::String failText; // The text to return if not "stateCondition".

	CueList_t onExecActions;  // The action to be executed.
	
	void executeActionCues (void);

	void addAlias (const ATL::String& alias);
	NameList_t getAllNames (void) const;
	bool isTargetNameInString (const ATL::String& inString) const;
	ATL::String getTargetNameInString (const ATL::String& inString) const;

	void setReturnText (const ATL::String& newText);
	ATL::String getReturnText (void) const;

protected:
	//ATL::String returnText; // The text to return if "stateCondition".
	TextSourceList_t retTextSources;

	TargetAliasList_t aliases;
};
