#include "CueSetVar.h"

#include "DebugLog.h"
#include "PlayerAction.h"

bool CueSetVar::execute (void)
{
	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}
	
	//LOG ("CueSetBrief: setting obj '", tracName + "' brief to: " + newBrief);
	
	PlayerAction::setStoryVariable (varName, newVarValue);
	
	return true;
}

ATL::String CueSetVar::getName (void)
{
	return "set_var";
}

void CueSetVar::parseStoredCueString (void)
{
	// Format: %(set_var varName newValue)

	// Extract the variable name.
	varName = unparsedCueParameters.getNextWord();

	ASSERT (!varName.empty(), "CueSetVar: VarName must not be empty!");
	
	// Next we get the new state.
	newVarValue = unparsedCueParameters.getNextWord (varName.length());
	
	// Setting empty variables will be O.K.
	if (newVarValue.empty())
	{
		LOGW ("CueSetVar: Variable '", varName + "' is being set to empty!");
	}
}
