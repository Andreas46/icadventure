#include "CueMoveObject.h"

#include "DebugLog.h"
#include "GameManager.h"

CueMoveObject::CueMoveObject (void)
{
	
}

CueMoveObject::~CueMoveObject (void)
{
	
}

bool CueMoveObject::execute (void)
{
	Tractable_ptr tractable;

	LOG ("Executing Cue '", getName() + "'.");

	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	if (moveToInv)
	{
		if (!GMGR.getCurrentRoom()->isTractableInScope (tractableTarget))
		{
			LOGE ("Can't get tractable '", tractableTarget +
				"' As it is not in the current room!");
			return false;
		}

		tractable = GMGR.getCurrentRoom()->popTractable (tractableTarget);
		GET_INV.addTractable (tractable);
	}
	else
	{
		if (!GET_INV.isTractableInScope (tractableTarget))
		{
			LOGE ("Can't get tractable '", tractableTarget +
				"' As it is not in your inventory!");
			return false;
		}

		tractable = GET_INV.popTractable (tractableTarget);
		GMGR.getCurrentRoom()->addTractable (tractable);
	}

	return true;
}

ATL::String CueMoveObject::getName (void)
{
	return "move_obj_to";
}

void CueMoveObject::parseStoredCueString (void)
{
	ATL::String moveToInvStr;

	tractableTarget = unparsedCueParameters.getNextWord();

	ASSERT (!tractableTarget.empty(), "Tractable target must not be empty!");

	// Get the location of where to spawn the object.
	moveToInvStr = unparsedCueParameters.getNextWord (tractableTarget.length());

	// Are we spawning to the inventory?
	if (moveToInvStr == "inv")
	{
		moveToInv = true;
	}
	// In the room, perhaps?
	else if (moveToInvStr == "room")
	{
		moveToInv = false;
	}
	else
	{
		// Unknown, missing token.
		FAIL_S ("Can not parse '", getName() +
			"' cue; bad 'token' token: " + moveToInvStr);
	}

	hasCueBeenParsed = true;
}
