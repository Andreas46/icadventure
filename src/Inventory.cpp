#include "Inventory.h"

#include "ActionCustom.h"
#include "DebugLog.h"

Inventory::Inventory (void)
{
	PlayerAction_ptr invAction;
		//invAction = 
}

Inventory::~Inventory (void)
{
}

//VerbSearchResult Inventory::extractVerb (const ATL::String& inString) const
//{
//	return VerbSearchResult();
//}

ParseResult Inventory::attemptParse (ATL::String command)
{
	ParseResult result;
	ATL::String failText;

	// Handle the "inventory" command.
	if (command == "inventory" ||
		command == "inv")
	{
		result.returnText = getInventory();
		result.parseSuccessfull = true;
		
		return result;
	}
	else
	{
		// Interogate the inventory items.
		TractableMap_t::iterator tracItr;
		for (tracItr = inventory.begin(); tracItr != inventory.end(); ++tracItr)
		{
			//LOGD ("Attempting to parse inv tractable '", tracItr->first +
			//	"' in str: " + command);

			result = tracItr->second->attemptParse (command);
			
			// Did we perform a successful parse?
			if (result.parseSuccessfull == true)
			{
				LOGD ("Parsed inv tractable: ", result.target);
				return result;
			}
			else if (failText.empty() && !result.returnText.empty())
			{
				failText = result.returnText;
			}
		}
		
		LOGD ("Failed to parse inv.","");
		
		return ParseResult (false, failText);
	}
}

ATL::String Inventory::getInventory (void)
{
	unsigned maxStrLen = 0;
	const unsigned itemGap = 2;
	
	ATL::String retString = "You are carrying: \n";
	
	// Determin length of longest string in inventory.
	TractableMap_t::iterator itr;
	for (itr = inventory.begin(); itr != inventory.end(); ++itr)
	{
		if (itr->second->getBriefText().length() > maxStrLen)
		{
			maxStrLen = itr->second->getBriefText().length();
		}
	}
	
	// Append fixed width inventory items to string.
	for (itr = inventory.begin(); itr != inventory.end(); ++itr)
	{
		ATL::String paddedStr = itr->second->getBriefText();
		
		// Replace underscores with spaces.  Core names MUST have no spaces.
		paddedStr.replace ("_", " ");
	
		if (itr != --inventory.end())
		{
			paddedStr.append (",");  // to deliniate the items.
		}
		
		paddedStr.padToNChars (maxStrLen + itemGap);
		
		retString += paddedStr;
	}

	// Put something in return string in the case where the inventory is empty.
	if (inventory.empty())
	{
		retString += "Nothing";
	}

	return retString;
}

Tractable_ptr Inventory::getTractable (ATL::String objName)
{
	TractableMap_t::const_iterator itr;
	
	// Find out which tractable this names belongs to.
	for (itr = inventory.begin(); itr != inventory.end(); ++itr)
	{
		if (itr->second->hasName (objName))
		{
			return itr->second;
		}
	}

	// Didn't find tractable?
	FAIL_S ("Unable to get Inv Tractable: ", objName);
	
	// Seriously?
	return NULL;
}

bool Inventory::isTractableInScope (const ATL::String& objName) const
{
	TractableMap_t::const_iterator itr;
	
	for (itr = inventory.begin(); itr != inventory.end(); ++itr)
	{
		if (itr->second->hasName (objName))
		{
			return true;
		}
	}
	
	return false;
// 	if (inventory.count (objName) > 0)
// 	{
// 		return true;
// 	}
// 	else
// 	{
// 		return false;
// 	}
}

void Inventory::addTractable (Tractable_ptr tractable)
{
	ASSERT_S (inventory.count (tractable->getName()) == 0,
		"Can't add tractable '", tractable->getName() +
		"' as it is already in the inventory!");

	LOG ("Adding item '", tractable->getName() + "' to inventory.");

	inventory [tractable->getName()] = tractable;
}

Tractable_ptr Inventory::popTractable (const ATL::String& tractableName)
{
	Tractable_ptr tractable = getTractable (tractableName);
	
	inventory.erase (tractableName);
	
	return tractable;
}

ATL::String Inventory::getName (void) const
{
	return "inventory";

}

ATL::String Inventory::getParserName (void) const
{
	return "inventory";
}
