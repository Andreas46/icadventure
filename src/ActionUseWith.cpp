#include "ActionUseWith.h"
#include "DebugLog.h"
#include "GameManager.h"

UseWith::UseWith (GameObject* gameObject): PlayerAction (gameObject)
{
	actionName = "use";
	actionUid = actionName + "_with";
	
	actionAliases.push_back ("operate");
	actionAliases.push_back ("actuate");
	actionAliases.push_back ("manipulate");
}

UseWith::UseWith (const UseWith& rhs, GameObject* gameObject):
	PlayerAction (rhs)
{
	parentObject = gameObject;
}

UseWith::~UseWith (void)
{
}

// ParseResult UseWith::attemptParse (ATL::String command)
// {
// 	return std::pair <bool, ATL::String> ();
// }

void UseWith::addTargetFromKeyVal (const KeyValList_t params)
{
	
}

PlayerAction_ptr UseWith::copy (GameObject* newParent) const
{
	return std::make_shared <UseWith> (*this, newParent);
}

ParseResult UseWith::attemptParse (ATL::String command)
{
	ParseResult result;
	//int64_t findPos;

	// First, we use the base function to try and extract the "use" verb.
	result = PlayerAction::extractVerbParams (command);
	
	//LOG ("Attempting to extract verb 'use' from: ", command);
	
	if (result.parseSuccessfull == true)
	{
		// We found the "use" verb, but we need to verify that we are "using"
		// the parent object, before evaluating targets.
		//findPos = result.returnText.find (parentObject->getName());
		
		//if (findPos < 0)
		if (!parentObject->isNameInStr (result.returnText))
		{
			// No.  We're not "using" the parent object.
			return ParseResult (false, "");
		}
	}
	else
	{
		return ParseResult (false, "");
	}
	
	LOG ("Parsing command 'use with': ", command);
	
	// We're are using this object.  Evaluate if any targets are
	// being used "with" this object.
	// Note that we're searching all params, and not from "findPos".
	// This allows for the two "use" operands to be swapped, and still
	// be successfully parsed.
	result = evaluateActionTargetsFromString (result.returnText, result.verb);

// 	if (result.parseSuccessfull == false && !result.returnText.empty())
// 	{
// 		LOGD ("Failing with text: ", result.returnText);
// 	}

	if (result.parseSuccessfull == false &&
		result.returnText.empty())
		{
			LOGD ("Usewith fail string: ", badTargetFailString);

			result.returnText = badTargetFailString;
		}

	// if (GameManager::getInstance().isTractableInScope ())
	
	return result;
}

bool UseWith::evaluateTargetFromString (ActionTarget target,
										const ATL::String& fromString,
										ATL::String& retText,
										ATL::String& failString,
										const ATL::String& verb)
{
	Tractable_ptr targetObj;
	
	//LOG ("Not here. |:-[","");

	// If the target tractable is in scope.
	if (GameManager::getInstance().isTractableInScope (target.target))
	{
		//targetObj = GMGR.getTractableFromCurrentContext (target.target);
		
		//LOG ("Evaluating usewith target in scope: ", target.target);
		
		if (PlayerAction::evaluateTargetFromString (
			target, fromString, retText, failString, verb))
		{
			//LOG ("Matched usewith target: ", target.target);
			PlayerAction::setStoryVariable ("_last_objects_", target.target);
			return true;
		}
	}
	// If we failed to parse this tractable.
// 	else if (failString.empty() && !badTargetFailString.empty())
// 	{
// 		LOG ("Return fail string: ", badTargetFailString);
// 
// 		failString = badTargetFailString;
// 	}
	else
	{
		LOGD ("Skipping out of scope Usewith target: ", target.target);
	}
	
	return false;
}

bool UseWith::attemptParseProperty (const ATL::String& property,
									const ATL::String& value)
{
	if (property == actionName + "_with")
	{
		StringVector_t vals = splitValueString (value, 2);

		addTarget (vals [0],"", vals [1], vals [2]);
	}
	else if (property == actionName + "_with_state")
	{
		StringVector_t vals = splitValueString (value, 3);

		addTarget (vals [1], vals [0], vals [2], vals [3]);
		//DebugLog::log (ATL::String ("num str vals: ") + (double)vals.size());
		//DebugLog::log (vals [1] + "|" + vals [2] + "|" + vals [0]);
	}
	else if (property == actionName + "_with_fail")
	{
		setFailMessage (value);
	}
	else
	{
		return false;
	}
// 	else
// 	{
// 		// Try some of the base-class actions.
// 		return PlayerAction::attemptParseProperty (property, value);
// 	}
		
	return true;
}

