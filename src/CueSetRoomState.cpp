#include "CueSetRoomState.h"

#include "GameManager.h"
#include "Room.h"

bool CueSetRoomState::execute (void)
{
	Room_ptr room;

	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	if (!GMGR.isThereARoomNamed (roomName))
	{
		LOGE ("Can't set state of room '", roomName +
		"' to '" + newState + "', as the room does not exist!");
		return false;
	}

	LOG ("CueSetRoomState: setting state of room '", roomName +
		"' to: " + newState);
	
	// Attempt to locate the tractable in the game scene.
	room = GMGR.getRoom (roomName);
	
	room->setState (newState);
	
	return true;
}

ATL::String CueSetRoomState::getName (void)
{
	return "set_room_state";
}

void CueSetRoomState::parseStoredCueString (void)
{
	// Format: %(set_room_state roomName newState)
	
	// Extract the room's name
	roomName = unparsedCueParameters.getNextWord();

	ASSERT (!roomName.empty(), "CueSetRoomState: Room name must not be empty!");
	
	// Next we get the new state.
	newState = unparsedCueParameters.getNextWord (roomName.length());
	
	ASSERT (!newState.empty(), "CueSetRoomState: A new state MUST be specified!");
}
