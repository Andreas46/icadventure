#pragma once

#include "GraphicsPopUpWindow.h"

class JournalPopUpWindow: public GraphicsPopUpWindow
{
public:
	static void showJournal (int startPage = 0, int width = 92, int height = 50);

	JournalPopUpWindow (void);
	virtual ~JournalPopUpWindow (void);
	
	virtual void setPageNum (int pageNum);

protected:
	virtual bool handleChar (int c);
};