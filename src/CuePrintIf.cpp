#include "CuePrintIf.h"

#include "DebugLog.h"
#include "Graphics.h"

TextSourceList_t CuePrintIf::extractCueFromText (ATL::String& text)
{
	int64_t cuePos;
	//int64_t cueEndPos;
//	int64_t lastCueEndPos = 0;
	ATL::String newTextSourceStr;
//	ATL::String newInText;

	ATL::String cueStrStartPattern = ATL::String ("%(") + "print_if";

//	CueList_t cueList;
	TextSourceList_t retList;
	
	CueTextSplit textSplit;

	// Find the opening cue syntax.
	cuePos = text.find (cueStrStartPattern);

	if (cuePos >= 0)
	{
		// Append string up until this point.
		newTextSourceStr = text.subStr (0, (size_t) (cuePos));
		retList.push_back (TextSource (newTextSourceStr));
		
		// Extract the cue text.
		textSplit = Cue::extractCueTextFromString (text, (size_t) cuePos);

	//	cueEndPos = text.find (")", cuePos);

	//	ASSERT_S (cueEndPos > 0, "Untermindated Cue in string: ",
	//		text + " At Position '" + (double)cuePos + "'.");

	//	ATL::String cueStr = text.subStr (cuePos + 2, (cueEndPos - cuePos - 2));

		// Extract found cue.
		CuePrintIf_ptr newCue = std::static_pointer_cast <CuePrintIf>
			(Cue::newCueFromString (textSplit.cue));
		
		retList.push_back (TextSource (newCue));

		//text = text.subStr (cueEndPos + 1);
		text = textSplit.postCue;

		LOG ("extractCueFromText; Remainder Text: ", text);
	}
// 	else
// 	{
// 		if (!text.empty())
// 		{
// 			retList.push_back (text);
// 		}
// 	}
	
	return retList;

// 	if (cueList.empty())
// 	{
// 		No cues were found.
// 	}
	// Append remainder of string.
	//newString += text.subStr (lastCueEndPos);
}

void CuePrintIf::formatRetTextSources (TextSourceList_t& retTextSources)
{
	TextSourceList_t::iterator itr;

	TextSourceList_t newRetTextSources;

	for (itr = retTextSources.begin(); itr != retTextSources.end(); ++itr)
	{
		if (itr->isStringSource())
		{
			ATL::String itrText = itr->getText();
			TextSourceList_t itrTextSources;

			// Attempt to extract this cue from the text.
			itrTextSources = extractCueFromText (itrText);
			
			while (!itrTextSources.empty())
			{
				// Append first half of string and found cue.
				newRetTextSources.insert (newRetTextSources.end(),
										  itrTextSources.begin(),
										  itrTextSources.end());
				
				itrTextSources = extractCueFromText (itrText);
			}
			
			//LOG ("Remainder Text: ", itrText);
			
			// Append the remainder of the string.
			newRetTextSources.push_back (itrText);
		}
		else
		{
			newRetTextSources.push_back (*itr);
		}
	}

	retTextSources = newRetTextSources;
}

CuePrintIf::CuePrintIf (void)
{
}

CuePrintIf::CuePrintIf (const CuePrintIf& rhs): Cue (rhs)
{
	condition = rhs.condition;
	textToPrint = rhs.textToPrint;
}

CuePrintIf::~CuePrintIf (void)
{
}

bool CuePrintIf::execute (void)
{
	Graphics::getInstance().addText (execAndGetRetString());

	return true;
}

ATL::String CuePrintIf::execAndGetRetString (void)
{
	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}

	if (condition->evaluate() == true)
	{
		LOGV ("adding cuePrintIf Text: ", textToPrint);
		
		// Execute cues.
		CueList_t::iterator itr;
		if (execCues.size() > 0)
		{
			LOG ("Executing ", (double) execCues.size() + " printf cues.");
		}
		
		for (itr = execCues.begin(); itr != execCues.end(); ++itr)
		{
			(*itr)->execute();
		}
		
		return textToPrint;
	}
	else
	{
		return textToPrintOnFailure;
	}
}

ATL::String CuePrintIf::getName (void)
{
	return "print_if";
}

void CuePrintIf::parseStoredCueString (void)
{
	ATL::String cueCondName;

	// Extract name of cue condition.
	cueCondName = unparsedCueParameters.getNextWord ();

	ASSERT_S (!cueCondName.empty(),
			  "CuePrintIf: no condition specified in String", unparsedCueParameters);

	// Create the cue condition from this name.
	ATL::String condAsStr = unparsedCueParameters;
	condition = CueCondition::newCueCondFromString (condAsStr);

	// The remaining text is what is printed.
	textToPrint = condAsStr;
	
	// ...Also extract any cues nested in the "text to print".
	execCues = Cue::extractAllCuesFromString (textToPrint, false);
}
