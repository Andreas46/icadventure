#pragma once

#include <AtlString.h>
#include <list>
#include <map>

#include "Cue.h"

#include "ForwardDeclarations.h"
#include "GameObject.h"
#include "PlayerActionTarget.h"

class PlayerAction;

typedef std::shared_ptr <PlayerAction> PlayerAction_ptr;

typedef std::list <ATL::String> ActionAliasList_t;

typedef std::map <ATL::String, PlayerAction_ptr> PlayerActionMap_t;
typedef std::list <PlayerAction_ptr> PlayerActionList_t;

//int64_t findWordPosInString (const ATL::String& string, const ATL::String& wordToFind);

class ParseResult
{
public:
	bool parseSuccessfull;
	ATL::String returnText;

	ATL::String target;	
	ATL::String verb;
	
	ParseResult (void);
	ParseResult (bool parseSucc,
				 ATL::String retText,
				 ATL::String target = "",
				 ATL::String verb = "");
};

class PlayerAction //: public VerbParser
{
	friend class ActionCustom;
public:
	PlayerAction (GameObject* gameObject);
	virtual ~PlayerAction (void);

	//static ATL::String normaliseActionFrom (ATL::String string);
	//static ATL::String extractVerbFrom (ATL::String);

	/**
	* @brief Parse the command.
	*
	* @param operands
	*	The rest of the string appling to this verb.
	*
	* @param action
	*	The verb being used (usually that of this action).
	*
	* @return A pair; The first return item (bool) indicating
	*	successful handling (true) or failure (false), and the second
	*	item is the string to be reported.
	*	
	*	The reason for the two parameters is, that if a command fails early in
	*	The parsing hirachey, it can optinally return a witty fail message.
	*	If it is not provided (empty string) an appropriate fail response will be
	*	returned from the default responses.
	**/
	virtual ParseResult attemptParse (ATL::String command) = 0;

	/**
	* @brief Add a target for this action.
	*
	*
	* @param
	*
	*
	* @return
	**/
	virtual void addTarget (const ATL::String& target,
							const ATL::String& parentObjCondition,
							const ATL::String& returnText,
						 	const ATL::String& failText = "",
						 	CueList_t execActions = CueList_t());
	
	virtual void addTarget (ATL::String target, ATL::String returnText);
	
	virtual void addObjTarget (const ATL::String& returnText,
							   const ATL::String& parentObjCondition = "",
							   const ATL::String& failText = "",
							   CueList_t execActions = CueList_t());

	/**
	* @brief Extract the first occurance of this actions verb.
	*
	*
	* @param inString
	*	The String to parse.
	*
	*
	* @return A pair contining the extracted and normalised verb,
	*	and a number corresponding To the position in "inString" that
	*	is was found.
	*	If the verb is not found, the pair consists of an empty string
	*	and "-1".
	**/
	//virtual VerbSearchResult extractVerb (const ATL::String& inString) const;
	
	virtual void addTargetFromKeyVal (const KeyValList_t params) = 0;
	
	/**
	* @brief Attempt to interpret a property, and modify this object based
	* on it's value.
	* 
	* @param property
	* 	The property to be handled.
	* 
	* @param value
	* 	The value of this property to be interpreted.
	* 
	* @retval true
	* 	Property was successfully handled.
	* 
	* @retval false
	* 	Property was not handled.  This object was not modifed.
	**/
	virtual bool attemptParseProperty (const ATL::String& property,
										const ATL::String& value);
										
	ATL::String getName (void) const;
	ATL::String getUid (void) const;
	
	void setFailMessage (ATL::String message);
	
	static void setStoryVariable (const ATL::String& varName, const ATL::String& varValue);
	static ATL::String getStoryVariable (const ATL::String& varName);
	static bool isStoryVarSet (const ATL::String& varName);
	static ATL::String& replaceAllStoryVarsIsStr (ATL::String& inStr);
	static void clearStoryVariables (void);

	// Deep copier.
	virtual PlayerAction_ptr copy (GameObject* newParent) const = 0;

protected:
	static StringMap_t storyVariables;

	ATL::String actionName;
	ATL::String actionUid;

	ActionAliasList_t actionAliases;

	ActionTargetList_t actionTargets;
	ATL::String badTargetFailString;

	//GameObject_ptr parentObject;
	GameObject* parentObject;
	
	/**
	* @brief Returns whether this action's verb was found (first),
	*	and the parameters to that verb (second).
	**/
	virtual ParseResult extractVerbParams (ATL::String command);

	virtual bool evaluateTargetState (ActionTarget target,
								 ATL::String& retText,
								 ATL::String& failString,
								 const ATL::String& verb);
	
	virtual bool evaluateTargetFromString (ActionTarget target,
										   const ATL::String& fromString,
										   ATL::String& retText,
										   ATL::String& failString,
										   const ATL::String& verb);

	virtual ParseResult evaluateActionTargetsFromString (const ATL::String& params,
														 const ATL::String& verb);

	//KeyValPair_t find (KeyValList_t);
};
