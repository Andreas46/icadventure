#include "Tractable.h"

#include <vector>

#include "ActionPickup.h"
#include "ActionUse.h"
#include "ActionEmpty.h"
#include "ActionLook.h"
#include "ActionOpen.h"
#include "ActionUseWith.h"
#include "DebugLog.h"
#include "GameManager.h"

TractableMap_t Tractable::objectTemplates;
std::set <Tractable_ptr> Tractable::thingsToDestroy;

StringVector_t splitValueString (const ATL::String input, unsigned numSplits, char spiltChar)
{
	StringVector_t retStrVec;
	unsigned i;
	unsigned nextStartPos = 0;
	
	DebugLog::assert (numSplits != 0, "Invalid number of string splits");

	for (i = 0; i < numSplits; ++i)
	{
		int64_t findPos = input.find (spiltChar, nextStartPos);
		
		ASSERT (findPos >= 0, ATL::String ("Failed to split str: ") + input);
		
	//	DebugLog::log ("Split string ");
		
		retStrVec.push_back (input.subStr (nextStartPos, findPos - nextStartPos));
		nextStartPos = (size_t) findPos + 1;
	}
	
	// Append remaining String.
	retStrVec.push_back (input.subStr (nextStartPos));
	
	return retStrVec;
}

Tractable_ptr Tractable::newObjectFromParams (KeyValList_t params,
											  GameObject* parent,
											  const PlayerActionList_t& moreActions,
											  bool isObjectTemplate
 											 )
{
	KeyValList_t::iterator itr;
	ATL::String objectName;

	Tractable_ptr newTractable = std::make_shared <Tractable>();

	// Add the additional parameters supplied (if any).
	if (moreActions.size() > 0)
	{
		PlayerActionList_t::const_iterator acItr;

		for (acItr = moreActions.begin(); acItr != moreActions.end(); ++acItr)
		{
			LOGD ("Adding extra action: ", (*acItr)->getUid());
			// Add these tractables to the /front/ of the list, so that they may
			// override compariable, pre-exisiting actions.
			newTractable->actions.push_front((*acItr)->copy (newTractable.get()));
		}
	}

	for (itr = params.begin (); itr != params.end (); ++itr)
	{
		if (itr->first == "name")
		{
			newTractable->objectName = itr->second;
			objectName = itr->second;
		}
		else if (itr->first == "alias")
		{
			newTractable->nameAliases.push_back (itr->second);
		}
		else if (itr->first == "pickable")
		{
			newTractable->pickable = itr->second.getAsBool();

			if (newTractable->pickable)
			{
				ATL::String cueText;
				// Add text for when this object is in the inventory.
				cueText << "%(print_if obj_in_inv " << newTractable->objectName <<
					" You've already got that.)";

				// Set the "pickup action" to react to this object.
				cueText << "%(print_if obj_in_current_room " << newTractable->objectName << 
					" %(move_obj_to " << newTractable->objectName << " inv))";

				LOG ("Cue text: ", cueText);
				newTractable->getAction ("pickup")->addTarget (newTractable->getName (), cueText);
			}

		}
		else if (itr->first == "state")
		{
			newTractable->objectState = itr->second;
		}
		else if (itr->first == "brief")
		{
			newTractable->briefText = itr->second;
		}
		else if (newTractable->attemptParsePropertyWithActions (itr->first, itr->second))
		{
			// Action succesfully parsed.
		}
		else
		{
			ATL::String str;
			str << "Failed to parse Tractable Param: " << itr->first << ": " << itr->second;
			DebugLog::log (str, DebugLog::LevelWarning);
		}
	}

	return newTractable;
}

bool Tractable::spawnObjFromTemplate (const ATL::String& objName, bool spawnToInv)
{
	Tractable_ptr templateTractable;
	Tractable_ptr newTractable;
	
	if (!objectTemplates.count (objName))
	{
		LOGE ("Can't spawn object '", objName + "' as it does not have a template!");
		return false;
	}

	// Attempt to locate the tractable in the game scene.
	templateTractable = getTemplateObject (objName);
	newTractable = templateTractable->copy ();

	if (spawnToInv)
	{
		GET_INV.addTractable (newTractable);
	}
	else
	{
		GMGR.getCurrentRoom()->addTractable (newTractable);
	}
	
	return true;
}

Tractable::Tractable (void)
{
	pickable = false;
	isInInv = false;
	
	// Add the basic Actions.
	addAction (std::make_shared <Look> (this));
	addAction (std::make_shared <ActionUse> (this));
	addAction (std::make_shared <ActionPickup> (this));
	addAction (std::make_shared <ActionOpen> (this));
	addAction (std::make_shared <ActionEmpty> (this));
	
	// Must be last to circumvent bug I'm not smart enough to fix. :(
	addAction (std::make_shared <UseWith> (this));
}

Tractable::Tractable (const Tractable& rhs): GameObject (rhs)
{
	PlayerActionList_t::const_iterator itr;
	// Duplicate actions.

	for (itr = rhs.actions.begin(); itr != rhs.actions.end(); ++itr)
	{
		addAction ((*itr)->copy (this));
	}

	objectName = rhs.objectName;
	//objectState = rhs.objectState;
	briefText = rhs.briefText;

	outText = rhs.outText;

	pickable = rhs.pickable;
	isInInv = rhs.isInInv;
}

Tractable::~Tractable (void)
{
}

ATL::String Tractable::getName (void) const
{
	return objectName;
}

ATL::String Tractable::getParserName (void) const
{
	return "tractable";
}

void Tractable::setBriefText (ATL::String text)
{
	briefText = text;
}

ATL::String Tractable::getBriefText (void)
{
	// The brief text is optional, so retun the name if it is empty.
	if (briefText.empty())
	{
		return getName();
	}
	else
	{
		return briefText;
	}
}

void Tractable::setObjectState (const ATL::String& newState)
{
	objectState = newState;
}

bool Tractable::isPickable (void) const
{
	return pickable;
}

ParseResult Tractable::attemptParse (ATL::String command)
{
	ParseResult result;

	ATL::String failText;

	// Iterate over all the actions, and ask them to attempt a parse.
	PlayerActionList_t::iterator itr;
	for (itr = actions.begin(); itr != actions.end(); ++itr)
	{
		//LOG ("attempting to parse cmd in tractable: ", command);

		result = (*itr)->attemptParse (command);
		//DebugLog::log (ATL::String ("search obj: ") + objectName );//+ " print text: " + itr->second);

		// Did we perform a successful parse?
		if (result.parseSuccessfull == true)
		{
			LOG ("Using tractable: ", getName());
			return result;
		}
		else if (!result.returnText.empty() && failText.empty())
		{
			// Custom fail text provided.
			failText = result.returnText;
		}
	}

	return ParseResult (false, failText);
}

bool Tractable::attemptParsePropertyWithActions (const ATL::String& property,
												 const ATL::String& value)
{
	PlayerActionList_t::iterator itr;
	for (itr = actions.begin(); itr != actions.end(); ++itr)
	{
		if ((*itr)->attemptParseProperty (property, value))
		{
			return true;
		}
	}
	
	return false;
}

// void Tractable::addAction (PlayerAction_ptr action)
// {
// 	actions [action->getName()] = action;
// }

Tractable_ptr Tractable::getTemplateObject (ATL::String templateName)
{
	ASSERT (objectTemplates.count (templateName) > 0,
		ATL::String ("Bad template name: ") + templateName);

	return objectTemplates [templateName];
}

void Tractable::addTemplateObject (Tractable_ptr tractable)
{
	ASSERT (objectTemplates.count (tractable->getName()) == 0,
		ATL::String ("Object template exists: ") + tractable->getName());

	objectTemplates [tractable->getName()] = tractable;
}

void Tractable::clearObjectTemplates(void)
{
	objectTemplates.clear();
}

void Tractable::destroyDeferedDestruction (void)
{
	thingsToDestroy.clear();
}

void Tractable::destroyTractableLater (Tractable_ptr tractable)
{
	thingsToDestroy.insert (tractable);
}

Tractable_ptr Tractable::copy (void) const
{
	return std::make_shared <Tractable> (*this);
}

// ATL::String Tractable::getState (void) const
// {
// 	return objectState;
// }
