#pragma once

#include <memory>
#include <set>

#include "GraphicsWindow.h"
#include "RawInputHandler.h"

#define POPUP_WIN_Y_PAD = 2

#define PAGE_HEIGHT (EFFECTIVE_WIN_Y - 3)
#define MAX_PAGE_HEIGHT (GET_SCRN_Y - 2 * WIN_PADDING_Y - 2 - 3 - 1)

class GraphicsPopUpWindow;

typedef std::shared_ptr <GraphicsPopUpWindow> PopUp_ptr;
typedef std::set <PopUp_ptr> PopUpSet_t;

class GraphicsPopUpWindow:
	public GraphicsWindow,
	public RawInputHandler,
	public std::enable_shared_from_this <GraphicsPopUpWindow>
{
public:
	static PopUp_ptr newPopUp (const ATL::String& text,
							   unsigned width = 30,
							   bool centreJustified = true);

	GraphicsPopUpWindow (void);
	virtual ~GraphicsPopUpWindow (void);

	virtual void setSize (unsigned newPosX, unsigned newPosY, unsigned newSizeX, unsigned newSizeY);
	virtual void setWidth (const unsigned newWidth);
	virtual void setFixedWinHeight (const int height);

	virtual int getPageHeight (void);
	//virtual bool rawInputHandler (int c);

	virtual void addText (ATL::String text);
	virtual void recalculateDimensions (void);

	virtual void addTextFromFile (ATL::String textFilePath);

	virtual void setCentreJustified (const bool justified);
	virtual void setHidden (bool hidden);

	virtual void update (void);

protected:
	static PopUpSet_t activePopUps;
	
	StringVectorVector_t pages;

	ATL::String prompt;
	bool isTextCentreJustified;
	int currentPage;
//	int numPages;
	//int pageSize;
//	int pageOffset;

	//int minPageHeight;
	int fixedWinHeight;

	virtual bool handleChar (int c);
	virtual int getChar (void);

	ATL::String& centreJustifyText (ATL::String& text);
	virtual void pushPage (StringVector_t page);

	virtual StringVector_t getWindowLinesForText (const ATL::String& text);
	
	virtual StringVector_t getNextPage (void);
	virtual void generatePages (void);
	size_t getNumPages (void);
};
