#include "CueSetState.h"

#include "GameManager.h"
#include "DebugLog.h"

CueSetState::CueSetState (void)
{
}


CueSetState::~CueSetState (void)
{
}

bool CueSetState::execute (void)
{
	Tractable_ptr tractable;

	if (!hasCueBeenParsed)
	{
		parseStoredCueString();
	}
	
	LOG ("CueSetState: Changing state of '", objToChangeName + "' to '" + newState + "'.");
	
	tractable = GMGR.getTractableFromCurrentContext (objToChangeName);
	
	tractable->setObjectState (newState);
	
	return true;
}

ATL::String CueSetState::getName (void)
{
	return "set_obj_state";
}

void CueSetState::parseStoredCueString (void)
{
	// Format: %(set_obj_state objName newState)
	
	// Extract the object name.
	objToChangeName = unparsedCueParameters.getNextWord();

	ASSERT (!objToChangeName.empty(), "CueSetState: Object name must not be empty!");
	
	// Next we get the new state.
	newState = unparsedCueParameters.getNextWord (objToChangeName.length());
	
	ASSERT (!newState.empty(), "CueSetState: A new state MUST be specified!");
}
