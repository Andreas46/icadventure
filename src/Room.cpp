#include "Room.h"

#include "ActionMove.h"
#include "DebugLog.h"

Room::Room (void): lookAction (this)
{
	// Room will always have a "look" action.
	addAction (std::make_shared <Look>(this));

	// It will also always have a "move" action.
	addAction (std::make_shared <ActionMove> (this));
}


Room::~Room (void)
{
}

Room_ptr Room::newObjectFromParams (KeyValList_t params)
{
	KeyValList_t::iterator itr;

	Room_ptr newRoom = std::make_shared <Room>();

	for (itr = params.begin(); itr != params.end(); ++itr)
	{
		if (itr->first == "name")
		{
			newRoom->roomName = itr->second;
		}
		else if (itr->first == "alias")
		{
			newRoom->nameAliases.push_back (itr->second);
			newRoom->nameAliases.back().toLower();
		}
		else if (itr->first == "state")
		{
			newRoom->objectState = itr->second;
			//newRoom->objectState.toLower();
		}
		else if (itr->first == "brief")
		{
			newRoom->roomBrief = itr->second;
		}
		else if (itr->first == "on_enter")
		{
			newRoom->onEnterTarget.setReturnText (itr->second);
		}
		else if (itr->first == "on_exit")
		{
			newRoom->onExitTarget.setReturnText (itr->second);
		}
		else if (itr->first == "description")
		{
			ATL::String roomText;
			//roomText << "-----\n" << itr->second;
			roomText = itr->second;
			//ActionTarget target;
			newRoom->getAction ("look")->addTarget ("", roomText);  // Default.
			
			// Some aliases for "".  Not the most efficient method
			// of course.  Mabye I'll fix it later. ;)
			newRoom->getAction ("look")->addTarget ("room", roomText);
			newRoom->getAction ("look")->addTarget ("around", roomText);
		}
		else if (newRoom->attemptParsePropertyWithActions (itr->first, itr->second))
		{
			// Parse successful.
		}
		else
		{
			ATL::String str;
			str << "Failed to parse Room Param: " << itr->first << ": " << itr->second;
			DebugLog::log (str, DebugLog::LevelWarning);
		}
	}
	
	// Assert that critical parameters have been set.
	if (newRoom->roomName.empty())
	{
		DebugLog::log ("Room is missing name!", DebugLog::LevelError);
		throw std::runtime_error ("Room is missing name!");
	}
	
	return newRoom;
}

ParseResult Room::attemptParse (ATL::String command)
{
	ParseResult result;

	ATL::String failText;

	//return ParseResult (true, "some victory text!");
	
	// Iterate over all the actions, and ask them to attempt a parse.
	PlayerActionList_t::iterator itr;
	for (itr = actions.begin(); itr != actions.end(); ++itr)
	{
		result = (*itr)->attemptParse (command);

		// Did we perform a successful parse?
		if (result.parseSuccessfull == true)
		{
			LOG ("Used verb: ", result.verb);
			if (result.verb == "look")
			{
				// prepend some text to help keep things clear.
				//result.returnText.prepend ("-----\n");

				// Append the "near you is" tractables.
				result.returnText += getNearbyTractableText();

				//LOG ("Adding trac text: ", result.returnText);
			}

			return result;
		}
	}
	
	// Result was not found in the rooms actions.  Expand search by
	// interrogating all the room's tractables.
	TractableMap_t::iterator tracItr;
	
	for (tracItr = tractables.begin(); tracItr != tractables.end(); ++tracItr)
	{
		result = tracItr->second->attemptParse (command);
		
		// Did we perform a successful parse?
		if (result.parseSuccessfull == true)
		{
			return result;
		}
		else if (failText.empty() && !result.returnText.empty())
		{
			failText = result.returnText;
		}
	}
	
	return ParseResult (false, failText);
}

bool Room::attemptParsePropertyWithActions (const ATL::String& property,
												 const ATL::String& value)
{
	PlayerActionList_t::iterator itr;
	for (itr = actions.begin(); itr != actions.end(); ++itr)
	{
		if ((*itr)->attemptParseProperty (property, value))
		{
			return true;
		}
	}
	
	return false;
}

// void Room::addRoomAction (ATL::String verb, PlayerAction_ptr action)
// {
// 	// Assert action's uniqueness.
// 	if (hasAction (verb))
// 	{
// 		throw std::runtime_error ((ATL::String ("Verb '") + verb + "' is already in use!").cStr());
// 	}
// 
// 	getAction (verb) = action;
// }

void Room::addTractable (Tractable_ptr tractable)
{
	DebugLog::assert (tractables.count (tractable->getName()) == 0,
		ATL::String ("Cannot add tractable '") + tractable->getName() +
		"' to room '" + roomName + "' as one of the same name is already attached.");

	tractables [tractable->getName()] = tractable;
}

Tractable_ptr Room::popTractable (ATL::String tractableName)
{
	Tractable_ptr tractable = getTractable (tractableName);
	
	tractables.erase (tractableName);
	
	return tractable;
}

//VerbSearchResult Room::extractVerb (const ATL::String& inString) const
//{
//	return std::pair <ATL::String, int>();
//}

Tractable_ptr Room::getTractable (ATL::String objName)
{
	TractableMap_t::const_iterator itr;
	
	// Find out which tractable this names belongs to.
	for (itr = tractables.begin(); itr != tractables.end(); ++itr)
	{
		if (itr->second->hasName (objName))
		{
			return itr->second;
		}
	}

	// Didn't find tractable?
	FAIL_S ("Unable to get Inv Tractable: ", objName);
}

bool Room::isTractableInScope (const ATL::String& objName) const
{
	TractableMap_t::const_iterator itr;
	
	for (itr = tractables.begin(); itr != tractables.end(); ++itr)
	{
		if (itr->second->hasName (objName))
		{
			return true;
		}
	}
	
	return false;
}

//void Room::destroyTractable (const ATL::String& objName)
//{
//	ASSERT (tractables.count (objName) > 0, "Can't destory obj '" + objName +
//			"' in room '" + roomName + "' as it does not exist!.");
//
//	tractables.erase (objectState);
//}

ATL::String Room::getNearbyTractableText (void)
{
	TractableList_t pickableTract;
	ATL::String retText;
	size_t maxBriefLen = 0;
	const size_t minBriefPadding = 2;

	// Build a list of all the "pickable" tractables in the room.
	TractableMap_t::iterator mapItr;
	for (mapItr = tractables.begin(); mapItr != tractables.end(); ++mapItr)
	{
		if (mapItr->second->isPickable())
		{
			pickableTract.push_back (mapItr->second);
			
			// Determine the length of the largest brief (for formatting).
			if (mapItr->second->getBriefText().length() > maxBriefLen)
			{
				maxBriefLen = mapItr->second->getBriefText().length();
			}
		}
	}
	
	// Is there anything nearby we can pick up?
	if (pickableTract.empty())
	{
		// No.  Return nothing.
		return retText;
	}
	
	retText = "\n\nNear you, you can see:\n";
	
	// Add the briefs for all the visible tractables.
	while (!pickableTract.empty())
	{
		ATL::String brief = pickableTract.front()->getBriefText();
		
		brief.padToNChars (maxBriefLen + minBriefPadding);
		
		retText += brief;
		
		pickableTract.pop_front();
	}
	
	return retText;
}

ATL::String Room::triggerRoomEnter (void)
{
	ATL::String retText;

	onEnterTarget.executeActionCues();
	
	// prepend some text to help keep things clear.
	retText = "-----\n";
	
	retText += onEnterTarget.getReturnText();
	
	// Inject look Command unpon entering a room;
	retText += attemptParse ("look").returnText;
	
	return retText;
}

ATL::String Room::triggerRoomExit (void)
{
	onExitTarget.executeActionCues();
	
	return onExitTarget.getReturnText();
}

ATL::String Room::getName (void) const
{
	return roomName;
}

ATL::String Room::getParserName(void) const
{
	return "room";
}


// ATL::String Room::getState (void) const
// {
// 	return ATL::String();
// }
