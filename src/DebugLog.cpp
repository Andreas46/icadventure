#include "DebugLog.h"

#include <stdexcept>
#include <fstream>

#include "Graphics.h"
#include "GameManager.h"

DebugLog* DebugLog::instance = NULL;

DebugLog::LogLevelMap_t DebugLog::logLevelMap;

DebugLog& DebugLog::getInstance (void)
{
	return *instance;
}

DebugLog::DebugLog (void)
{
	if (instance == NULL)
	{
		instance = this;
	}
	
	logToGraphics = false;
	
	debugLogName = "debug.log";
	initLogLevelMap();
	
	// Clear debug log on start.
	std::ofstream logFile (debugLogName.cStr());
	logLevel = LevelWarning;
	//logLevel = LevelInfo;
	//logLevel = LevelVerbose;
}

DebugLog::~DebugLog (void)
{
	
}

ATL::String DebugLog::getLogLevelAsText (LogLevel level)
{

	if (logLevelMap.count (level) == 0)
	{
		return "Unknown Log Level";
	}

	return logLevelMap [level];
}

void DebugLog::log (ATL::String text, LogLevel level)
{
	ATL::String logName = getInstance().debugLogName;

	ATL::String msg;

	msg << "[" << getLogLevelAsText (level) << "] " << text << '\n';
	
	if (level < getInstance().logLevel)
	{
		// Supress log message.
		return;
	}
	
	std::ofstream logFile (logName.cStr(), std::ios::app);
	
	if (!logFile.is_open())
	{
		// I'd log this an error, but...
		throw std::runtime_error ("Failed to open debug log!");
	}
	
	logFile << msg.cStr();
	
	// If we've opted to log to the graphics, do this now.
	if (getInstance().logToGraphics)
	{
		Graphics::getInstance().addText (msg);
	}
}

void DebugLog::assert (bool testConidtion, ATL::String message)
{
	if (testConidtion)
	{
		return;
	}
	
	abort (message);

// 	if (!message.empty())
// 	{
// 		log (message, LevelError);
// 	}
// 	
// 	Graphics::getInstance().leaveCursesMode();
// 
// 	throw std::runtime_error (message.cStr());
}


void DebugLog::abort (ATL::String message)
{
	if (!message.empty())
	{
		log (message, LevelError);
	}
	
	Graphics::getInstance().leaveCursesMode();

	throw std::runtime_error (message.cStr());
}

void DebugLog::setLogLevel (LogLevel newLevel)
{
	logLevel = newLevel;
}

void DebugLog::setLogLevelFromText (ATL::String level)
{
	LogLevelMap_t::iterator itr;

	for (itr = logLevelMap.begin(); itr != logLevelMap.end(); ++itr)
	{
		if (itr->second == level)
		{
			setLogLevel (itr->first);
			return;
		}
	}

	LOGW ("Failed to set log level to: ", level);
}

DebugLog::LogLevel DebugLog::getLogLevel (void) const
{
	return logLevel;
}

void DebugLog::initLogLevelMap (void)
{
	logLevelMap [LevelDebug] =  "Debug";
	logLevelMap [LevelVerbose] = "Verbose";
	logLevelMap [LevelInfo] = "Info";
	logLevelMap [LevelWarning] = "Warning";
	logLevelMap [LevelError] = "Error";
	logLevelMap [LevelCritical] = "Critical";
}
