#include "PlayerActionTarget.h"

#include "CuePrintIf.h"
#include "DebugLog.h"
#include "GameManager.h"

TextSource::TextSource (const ATL::String& initialiser)
{
	stringSource = initialiser;
}

TextSource::TextSource (const Cue_ptr initialiser)
{
	cueSource = std::static_pointer_cast <CuePrintIf> (initialiser);
}

ATL::String TextSource::getText (void) const
{
	if (isStringSource())
	{
		return stringSource;
	}
	else
	{
		return cueSource->execAndGetRetString();
	}
}

bool TextSource::isStringSource (void) const
{
	if (cueSource.use_count() == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// --- ActionTarget --- //

void ActionTarget::executeActionCues (void)
{
	std::list <Cue_ptr>::iterator itr;

	if (onExecActions.size() > 0)
	{
		LOG ("Executing ", (double) onExecActions.size() + " cues.");
	}
	
	for (itr = onExecActions.begin(); itr != onExecActions.end(); ++itr)
	{
		(*itr)->execute();
	}
}

void ActionTarget::addAlias (const ATL::String& alias)
{
	aliases.push_back (alias);
}

NameList_t ActionTarget::getAllNames (void) const
{
	NameList_t allTheNames = aliases;
	
	allTheNames.push_front (target);
	
	return allTheNames;
}

bool ActionTarget::isTargetNameInString (const ATL::String& inString) const
{
	NameList_t names; // = getAllNames();
	NameList_t::const_iterator itr;
	
//	int64_t startPos = 0;

	if (GMGR.isTractableInScope(target))
	{
		LOGD ("Making tactable list for target:", target);
		names = GMGR.getTractableFromCurrentContext (target)->getAllNames();
	}
	else
	{
		//LOG ("Skipping tactable list for target:", target)
		names.push_back (target);
	}
	
	for (itr = names.begin(); itr != names.end(); ++itr)
	{
		//int64_t wordPos;
		//int64_t startPos = inString.length() - itr->length();
		LOGD ("Looking for name '", *itr + "' in string: " + inString);
		
		//if (inString.find (*itr, startPos) >= 0)
 		if (inString.findWordPosInString (*itr) >=0)
//		wordPos = findWordPosInString (inString, *itr, startPos);
//		if (wordPos >=0 && wordPos)
		{
			LOGD ("Using tractable '", target + "' to handle name: " + inString);
			return true;
		}
	}
	
	return false;
}

ATL::String ActionTarget::getTargetNameInString (const ATL::String& inString) const
{
	NameList_t names;
	NameList_t::const_iterator itr;
	
	if (GMGR.isTractableInScope (target))
	{
		//LOGD ("Making tactable list for target:", target)
		names = GMGR.getTractableFromCurrentContext (target)->getAllNames();
	}
	else
	{
		//LOG ("Skipping tactable list for target:", target)
		names.push_back (target);
	}
	
	for (itr = names.begin(); itr != names.end(); ++itr)
	{
		//int64_t wordPos;
		int64_t startPos = inString.length() - itr->length();
		//LOG ("Looking for name '", *itr + "' in string: " + inString);
		
 		if (inString.findWordPosInString (*itr, (size_t) startPos) >=0)
		{
			//LOGD ("Using tractable '", target + "' to handle name: " + inString)
			return *itr;
		}
	}

	return "";
}

void ActionTarget::setReturnText (const ATL::String& newText)
{
	ATL::String returnText = newText;
	CueList_t newCues;
	CueList_t::iterator itr;

	// Extract and parse cues from newText.
	newCues = Cue::extractAllCuesFromString (returnText);

	retTextSources.clear();

	retTextSources.push_back (returnText);

	// Let certain cues handle splitting of text.
	CuePrintIf::formatRetTextSources (retTextSources);

	// Add the newly extracted cues to this target.
	for (itr = newCues.begin(); itr != newCues.end(); ++itr)
	{
		onExecActions.push_back (*itr);
	}
}

ATL::String ActionTarget::getReturnText (void) const
{
	ATL::String retText;
	TextSourceList_t::const_iterator itr;
	
	for (itr = retTextSources.begin(); itr != retTextSources.end(); ++itr)
	{
		retText += itr->getText();
	}
	
	return retText;
}