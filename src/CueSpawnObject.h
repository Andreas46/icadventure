#pragma once

#include "Cue.h"
#include "Tractable.h"

class CueSpawnObject: public Cue
{
public:
	CueSpawnObject (void);
	~CueSpawnObject (void);

	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	bool spawnInInv;

	//Tractable_ptr objectToSpawn;
	ATL::String objTemplateName;

	virtual void parseStoredCueString (void);
};