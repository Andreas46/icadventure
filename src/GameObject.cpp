#include "GameObject.h"


GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

ATL::String GameObject::getState (void) const
{
	return objectState;
}

void GameObject::setState (const ATL::String& newState)
{
	objectState = newState;
}

NameList_t GameObject::getAllNames (void) const
{
	NameList_t names;
	
	names = nameAliases;
	names.push_front (getName());
	
	return names;
}

bool GameObject::hasName (ATL::String name) const
{
	NameList_t names = getAllNames();
	NameList_t::const_iterator itr;

	for (itr = names.begin(); itr != names.end(); ++itr)
	{
		if (*itr == name)
		{
			return true;
		}
	}
	
	// name not found.
	return false;
}

bool GameObject::isNameInStr (const ATL::String& inStr) const
{
	NameList_t names = getAllNames();
	NameList_t::const_iterator itr;

	for (itr = names.begin(); itr != names.end(); ++itr)
	{
		if (inStr.findWordPosInString (*itr) >= 0)
		{
			return true;
		}
	}
	
	// name not found.
	return false;
}
