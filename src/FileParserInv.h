#pragma once

#include "FileParser.h"

class FileParserInv: public FileParser
{
public:
	
protected:
	virtual void parseSection (void);
};