#include "FileParserDefault.h"

#include "ActionCustom.h"
#include "DebugLog.h"

DefaultActionLists FileParserDefault::parseDefaultFile (ATL::String filePath)
{
	DefaultActionLists defLists;

	parseFile (filePath);

	defLists.emptyObjects = emptyObjects;
	defLists.playerActions = playerActions;
	defLists.defaultResponses = defaultResponses;
	defLists.badCommandResponses = badCommandResponses;

	return defLists;
}

void FileParserDefault::parseSection (void)
{
	KeyValList_t::iterator itr;
	ATL::String str;

	LOGV ("Parsing Default Section: [", currentSection + "].");

	for (itr = sectionKeyPairs.begin(); itr != sectionKeyPairs.end(); ++itr)
	{
		str.clear();
		str = ATL::String (itr->first) + ": " + itr->second;
		DebugLog::getInstance().log (str, DebugLog::LevelVerbose);
	}

	// Parse based on section.
	if (currentSection == "verb_default")
	{
		DefaultResponse_ptr response;
		response = DefaultResponse::newResponseFromParams (sectionKeyPairs);

		defaultResponses.push_back (response);
	}
	else if (currentSection == "fuzzy_match")
	{
		DefaultResponse_ptr response;
		response = DefaultResponse::newFuzzyResponseFromParams (sectionKeyPairs);

		defaultResponses.push_back (response);
	}
// 	else if (currentSection == "custom_verb")
// 	{
// 		//DebugLog::log (ATL::String ("Parsing custom verb"));
// 		PlayerAction_ptr customAction;
// 		customAction = ActionCustom::newObjectFromParams (sectionKeyPairs,
// 														  currentRoom.get());
// 		
// 		LOG ("Parsing Custom Action: ", (customAction->getUid()));
// 
// 		currentRoom->addAction (customAction);
// 		customActions.push_back (customAction->copy (currentRoom.get()));
// 
// 		DebugLog::log (ATL::String ("Parsing custom verb: ") + customAction->getName());
// 	}
	else if (currentSection == "object")
	{
		//PlayerActionMap_t::iterator itr;
		Tractable_ptr tractable = Tractable::newObjectFromParams (sectionKeyPairs,
																  NULL,
																  customActions
 																);

		emptyObjects.push_back (tractable);
	}
	else if (currentSection == "fail_messages")
	{
		KeyValList_t::iterator itr;
		for (itr = sectionKeyPairs.begin(); itr != sectionKeyPairs.end(); ++itr)
		{
			if (itr->first == "message")
			{
				badCommandResponses.push_back (itr->second);
			}
			else
			{
				LOGW ("unknown fail message key: ", itr->first);
			}
		}
	}
	else
	{
		LOGW ("Failed to handle default section with name: ", currentSection);
	}
}
