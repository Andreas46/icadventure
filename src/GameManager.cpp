#include "GameManager.h"

#include "CuePopUpFromFile.h"
#include "FileParserRoom.h"
#include "ForwardDeclarations.h"
#include "GraphicsPopUpWindow.h"

#include <thread>
#include <chrono>
#include <random>
#include <stdexcept>

GameManager* GameManager::instance = NULL;

GameManager::GameManager()
{
	isQuitting = false;
	isRestarting = false;
	startingLocation = "verandah";
	debugModeEnabled = true;
	
	PlayerAction::clearStoryVariables();
	Tractable::clearObjectTemplates();

	// Check and enable singleton.
	if (instance == NULL)
	{
		instance = this;
	}
	else
	{
		throw std::runtime_error ("Multiple instances of Graphics!");
	}
	
	inputManager.pushHandler (&graphics);

	graphics.drawFrame ();
	graphics.inputPromptEnabled = true;
	graphics.setCommandHandler (inputCommandCallback);

	//char *temp;
	//temp = _getcwd (NULL, 0);

	//std::cerr << temp << std::endl;
	//DebugLog::log (ATL::String ("Working dir: ") + temp);

	debugLog.logToGraphics = true;
	//graphics.leaveCursesMode();

	loadAllFiles();

//	GraphicsPopUpWindow popUp;
//	popUp.setSize (5, 5, 30, 2);
//	popUp.addText ("here!");
}


GameManager::~GameManager()
{
	debugLog.logToGraphics = false;
	instance = NULL;
}

GameManager& GameManager::getInstance (void)
{
	ASSERT (instance != NULL, "Game manager not initialised!");

	return *instance;
}

void GameManager::seedRandNumGen(void)
{
	std::random_device rd;
	
	randGen.seed (rd());
}

int64_t GameManager::getRandomNumber (const int64_t min, const int64_t max)
{
	//return min + (std::rand() % (int)(max - min + 1));
	std::uniform_int_distribution<int64_t> distribution (min, max);

	return distribution (randGen); 
	
}

void GameManager::loadAllFiles (void)
{
	FileParserRoom fileParser;

	FilePathList_t dirListing;
	FilePathList_t::iterator dirListItr;
	
	// Parse the rooms
	dirListing = fsManager.getDirListing ("resources/rooms", "ini");
	for (dirListItr = dirListing.begin (); dirListItr != dirListing.end (); ++dirListItr)
	{
		debugLog.log (ATL::String ("Loading File: ") + *dirListItr + "\n");

		try 
		{
			Room_ptr room = fileParser.parseRoomFile (*dirListItr);
			rooms [room->getName()] = room;
		}
		catch (std::runtime_error e)
		{
			ATL::String errorMsg;
			errorMsg << "failed to parse room file '" << *dirListItr << "'.";
			DebugLog::log (errorMsg, DebugLog::LevelError);
			errorMsg.clear();

			errorMsg << "reason: " << e.what();
			DebugLog::log (errorMsg, DebugLog::LevelError);
		}

	}

	// Now that all files have been loaded, we'll force the parsing of all cues.
	// This is done after the world loading, so that all expected rooms and
	// objects will be in place and referencable when they parse.
	Cue::parseAllCues();
	
	// Load the default responses from file
	defaultActions.loadResponsesFromFile ("resources/default/default.ini");
}

void GameManager::enterGameLoop (void)
{
	initialiseGameState();
	
	//GraphicsPopUpWindow win;
	//PopUp_ptr popUp = GraphicsPopUpWindow::newPopUp ("Here, have some text!, and soem text, and soem MOAR text!");
	
	while (isQuitting == false)
	{
		inputManager.checkForInput();
		graphics.drawFrame();

		Tractable::destroyDeferedDestruction();

		// So we don't spin at 100% cpu usage, sleep for 10ms.
		std::this_thread::sleep_for (std::chrono::milliseconds (10));
	}

}

void GameManager::processCommand (ATL::String command)
{
	int nextContext = InventoryContext;

	ATL::String responseText;

	ParseResult ParseResult;
	bool successfulParse = false;

	// Set the story variable.
	PlayerAction::setStoryVariable ("_last_command_", command);
	
	while (getVerbParser (nextContext) != NULL)
	{
		VerbParser* verbParser = getVerbParser (nextContext);
		
		if (!responseText.empty() && verbParser->getParserName() == "default")
		{
			// If we've recieved custom fail text, no need to use the default parser.
			break;
		}
		
		// Attempt to parse this command.
		ParseResult = verbParser->attemptParse (command);

		if (ParseResult.parseSuccessfull == true)
		{
			if (!responseText.empty() && !successfulParse)
			{
				LOG ("GMGR: overwriting fail text with: ", ParseResult.returnText);
			}
			
			// Successful parse!
			responseText = ParseResult.returnText;
			successfulParse = true;
			break;
		}
		else if (responseText.empty() && !ParseResult.returnText.empty())
		{
			// Parse failed, but some custom fail text has been supplied.
			responseText = ParseResult.returnText;
			
			LOG ("GMGR: adding fail text: ", responseText);
		}

		++nextContext;
	}
	
	// Was this a control command?
	ParseResult = attemptParseControlCommand (command);
	
	LOG ("control parse result: ", ParseResult.parseSuccessfull);
	
	if (ParseResult.parseSuccessfull)
	{
		// Yes.
		responseText = ParseResult.returnText;
		successfulParse = true;
	}

	// If we failed to parse, and no custom fail response was provided.	
	if (!successfulParse && responseText.empty ())
	{
		responseText = defaultActions.getBadCommandResponse ();
	}

	// Perform late variable substitution on on return text.
	PlayerAction::replaceAllStoryVarsIsStr (responseText);

	graphics.addTextLine (responseText);
}

void GameManager::moveToRoom (ATL::String roomName)
{
	ATL::String errMsg;
	
	DebugLog::log (ATL::String ("Moving to room: ") + roomName);
	
	if (! rooms.count (roomName))
	{
		ATL::String currRoomName;
		if (currentRoom.use_count() != 0)
		{
			currRoomName = currentRoom->getName ();
		}
		else
		{
			currRoomName = "[No current room]";
		}
		
		errMsg << "Cannot move to room '" << roomName << "' from room '" <<
			 currRoomName << "' as it does not exist!";

		DebugLog::log (errMsg, DebugLog::LevelError);
		
		//throw std::runtime_error (errMsg.cStr());
		// Lets just stay in the current room with the above error message.
		return;
	}
	
	if (currentRoom.use_count() > 0)
	{
		graphics.addText (currentRoom->triggerRoomExit());
	}

	currentRoom = rooms [roomName];
	
	// Clear screen for new room if we're not debugging.
	if (debugLog.getLogLevel() > DebugLog::LevelInfo)
	{
		graphics.clearOuputText();
	}

	// 
	graphics.addText (currentRoom->triggerRoomEnter());
}

bool GameManager::isThereARoomNamed (ATL::String roomName)
{
	if (rooms.count (roomName))
	{
		return true;
	}
	else
	{
		return false;
	}
}

Room_ptr GameManager::getRoom (const ATL::String roomName)
{
	ASSERT (isThereARoomNamed (roomName) == true, "Can not get room '" + roomName +
		"' as it does not exist!");

	return rooms [roomName];
}

Tractable_ptr GameManager::getTractableFromCurrentContext (ATL::String objName)
{
	ASSERT_S (isTractableInScope (objName),
		"Can't get tractable '", objName + "'; not in scope");
	
	if (inventory.isTractableInScope (objName))
	{
		return inventory.getTractable (objName);
	}
	else if (currentRoom->isTractableInScope (objName))
	{
		return currentRoom->getTractable (objName);
	}
	else if (defaultActions.isTractableInScope (objName))
	{
		return defaultActions.getTractable (objName);
	}
	else
	{
		throw std::runtime_error ("bad tractable name");
	}
}

bool GameManager::isTractableInScope (const ATL::String& objName) const
{
	if (currentRoom->isTractableInScope (objName) ||
		inventory.isTractableInScope (objName) ||
		defaultActions.isTractableInScope (objName))
	{
		return true;
	}
	else
	{
		return false;
	}
}

//void GameManager::destroyTractable (const ATL::String& objName)
Tractable_ptr GameManager::popTractable (const ATL::String& objName)
{
	ASSERT_S (isTractableInScope (objName),
		"Can't get tractable '", objName + "'; not in scope");
	
	if (currentRoom->isTractableInScope (objName))
	{
		return currentRoom->popTractable (objName);
	}
	else if (inventory.isTractableInScope (objName))
	{
		return inventory.popTractable (objName);
	}
	//else if (defaultActions.isTractableInScope (objName))
	//{
	//	defaultActions.destroyTractable (objName);
	//}
	else
	{
		throw std::runtime_error ("bad tractable name");
	}
}

Room_ptr GameManager::getCurrentRoom (void)
{
	return currentRoom;
}

Inventory& GameManager::getInventory (void)
{
	return inventory;
}

void GameManager::pushInputHandler (RawInputHandler* handler)
{
	inputManager.pushHandler (handler);
}

RawInputHandler* GameManager::popInputHandler (void)
{
	return inputManager.popHandler ();
}

void GameManager::removeInputHandler (RawInputHandler* handler)
{
	return inputManager.removeHandler (handler);
}

void GameManager::quit (void)
{
	isQuitting = true;
}

void GameManager::inputCommandCallback (ATL::String params)
{
	getInstance().processCommand (params);
}

ParseResult GameManager::attemptParseControlCommand (ATL::String command)
{
	ParseResult result;

	if (command == "exit" || command == "quit")
	{
		isQuitting = true;
	}
	if (command == "restart")
	{
		isRestarting = true;
		isQuitting = true;
	}
	else if (command.find ("loglevel") >= 0)
	{
		
		// Set last word as new log level.
		debugLog.setLogLevelFromText (command.getWordNumInString (-1));
	}
	else if (command.getNextWord () == "resize")
	{
		LOGW ("Resizing terminal.","");
		graphics.resizeTerminal ((int) command.getWordNumInString (2).getAsDouble (),
								 (int) command.getWordNumInString (3).getAsDouble ());
	}
	else if (command == "mctnafn")
	{
		LOGW ("Password accepted. Enabling debug mode.","");
		debugModeEnabled = true;
	}
	else if (command.getNextWord() == "warp" && debugModeEnabled)
	{
		LOG ("Attempting warp to room: ", command.getWordNumInString (-1));
		moveToRoom (command.getWordNumInString (-1));
	}
	else if (command.getNextWord() == "spawn" && debugModeEnabled)
	{
		LOG ("Spawning object: ", command.getWordNumInString (-1));
		Tractable::spawnObjFromTemplate (command.getWordNumInString (-1), true);
	}
	else if (command.getNextWord() == "set_var" && debugModeEnabled)
	{
		ATL::String varName = command.getWordNumInString (2);
		ATL::String varValue = command.getWordNumInString (3);

		LOG ("Setting variable '", varName + "' to: " + varValue);
		
		PlayerAction::setStoryVariable (varName, varValue);
	}
	else if (command.getNextWord() == "set_state" && debugModeEnabled)
	{
		ATL::String objectName = command.getWordNumInString (2);
		ATL::String newObjState = command.getWordNumInString (3);

		if (!isTractableInScope (objectName))
		{
			LOGE ("Bad tractable name.","");
			return result;
		}
		
		LOG ("Changing state of '", objectName + "' to '" + newObjState + "'.");
		
		getTractableFromCurrentContext (objectName)->setState (newObjState);
	}
	else
	{	
		return result;
	}
	
	result.parseSuccessfull = true;

	return result;
}

void GameManager::initialiseGameState (void)
{
	moveToRoom (startingLocation);
	
	//graphics.drawFrame();

	// Popup intro text.
	CuePopUpFromFile cue;
	cue.setParameters ("200 false resources/story_text/intro.txt");
	cue.execute();
}

VerbParser* GameManager::getVerbParser (int context)
{
	if (context == InventoryContext)
	{
		return &inventory;
	}
	else if (context == CurrentRoomContext)
	{
		return currentRoom.get();
	}
	else if (context == DefaultContext)
	{
		return &defaultActions;
	}

	return NULL;
}
