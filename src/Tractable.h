#pragma once

#include <AtlString.h>
#include <list>
#include <map>

#include "PlayerAction.h"
#include "GameObject.h"
#include "VerbParser.h"

class Tractable;

typedef std::shared_ptr <Tractable> Tractable_ptr;
typedef std::map <ATL::String, Tractable_ptr> TractableMap_t;
typedef std::list <Tractable_ptr> TractableList_t;

class Tractable: public GameObject, public VerbParser
{
public:
	//static Tractable newObjFromParams (std::list <ATL::String> params);
	static Tractable_ptr newObjectFromParams (KeyValList_t params,
											  GameObject* parent,
										      const PlayerActionList_t& moreActions =
										      PlayerActionList_t(),
											  bool isObjectTemplate = false
 											 );
	
	static bool spawnObjFromTemplate (const ATL::String& objName, bool spawnToInv);

	Tractable();
	Tractable (const Tractable& rhs);
	virtual ~Tractable();

	ATL::String getName (void) const;
	ATL::String getParserName (void) const;

	void setBriefText (ATL::String text);
	ATL::String getBriefText (void);

	void setObjectState (const ATL::String& newState);

	/**
	* @brief Return true if this object can be picked up.
	**/
	bool isPickable (void) const;

// 	/**
// 	* @brief Get the string for this object being looked at.
// 	*
// 	* Return The returned string will be relevant to the current
// 	*	context, and object state.
// 	**/
// 	std::string getLookString (void);

	virtual ParseResult attemptParse (ATL::String command);
	
	virtual bool attemptParsePropertyWithActions (const ATL::String& property,
												  const ATL::String& value);

// 	/**
// 	* @brief return a pointer to the action with name "actionName".
// 	* 
// 	* @param actionName
// 	* 	The name of the action to return.
// 	**/
// 	PlayerAction_ptr getAction (ATL::String actionName);
	
// 	void addAction (PlayerAction_ptr action);

	static Tractable_ptr getTemplateObject (ATL::String templateName);
	static void addTemplateObject (Tractable_ptr Tractable);
	static void clearObjectTemplates (void);

	static void destroyDeferedDestruction (void);
	static void destroyTractableLater (Tractable_ptr tractable);

	virtual Tractable_ptr copy (void) const;

protected:
	/**
	* A list of uninstantiated objects which can have objects "spawed" from these.
	* Useful for objects which are only generated after events/actions/etc.
	**/
	static TractableMap_t objectTemplates;

	static std::set <Tractable_ptr> thingsToDestroy;

	ATL::String objectName;

	ATL::String briefText;

	ATL::String outText;

	bool pickable;
	bool isInInv;
};

