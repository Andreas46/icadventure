#pragma once

#include "Cue.h"
#include "GameObject.h"

class CueNoOp: public Cue
{
public:
	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	virtual void parseStoredCueString (void);
};