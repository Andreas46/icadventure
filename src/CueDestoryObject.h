#pragma once
#include "Cue.h"

#include "AtlString.h"

class CueDestoryObject: public Cue
{
public:
	CueDestoryObject (void);
	virtual ~CueDestoryObject (void);

	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	ATL::String objectName;

	virtual void parseStoredCueString (void);
};

