#include "FileParser.h"

#include "ini.h"

#include <iostream>
#include <stdexcept>
#include <string>

// #include "ActionMove.h"
// #include "ActionCustom.h"
#include "DebugLog.h"

FileParser::FileParser()
{

}

FileParser::~FileParser()
{
	
}

void FileParser::parseFile (ATL::String filePath)
{
	clear();

	if (ini_parse (filePath.cStr(), inihHandler, this, sectionHandler) < 0)
	{
		ATL::String errMsg;

		errMsg << "Can't load iniFile: " << filePath;

		DebugLog::log (errMsg, DebugLog::LevelCritical);
		throw std::runtime_error ((ATL::String ("Can't load iniFile: ") + filePath).cStr());
	}
	
	// The last section will still need to be handled, as this is normally done
	// at the beginning of a new section.
	parseSection();
}

int FileParser::inihHandler (void* user,
							 const char* section,
							 const char* name,
							 const char* value
							)
{
// 	ATL::String str;
 	FileParser* instance = reinterpret_cast <FileParser*> (user);
// 	
// 	str += ATL::String ("Section: ") + section + ", ";
// 	str += ATL::String ("name: ") + name + ", ";
// 	str += ATL::String ("value: ") + value;
// 
// 	DebugLog::getInstance().log (str);
	
	ATL::String cleanKey = name;
	ATL::String cleanValue = instance->firstParseOfValue (value);
	
	// Make sure key is lowercase.
	cleanKey.toLower();

	// Replace any occurances of "\n" with an actual new line.
	cleanValue.replace("\\n", "\n", true);

	instance->sectionKeyPairs.push_back (KeyValPair_t (cleanKey, cleanValue));
	
	return 1;
}

void FileParser::sectionHandler (void* user, const char* sectionName)
{
	FileParser* instance = reinterpret_cast <FileParser*> (user);
	
	//DebugLog::getInstance().log (ATL::String ("new Section: [") + sectionName + "].");
	
	// Is this a new (not the first) section?
	if (! instance->currentSection.empty())
	{
		// Create new object.
		instance->parseSection();
		
		// Reset parameters.
		instance->sectionKeyPairs.clear();
	}
	
	instance->currentSection = sectionName;
	instance->currentSection.toLower();  // Make sure section name is lowercase
}

void FileParser::clear (void)
{
	//currentRoom = NULL;
	currentSection.clear();
	sectionKeyPairs.clear();
	customActions.clear();
	valueVariables.clear();
}

ATL::String FileParser::firstParseOfValue (const ATL::String& value) const
{
	ATL::String RetString = value;

	// Check for, and substitue any matching "variables".
	StringMap_t::const_iterator itr;
	for (itr = valueVariables.begin(); itr != valueVariables.end(); ++itr)
	{
		//LOG ("Replacing Str: ", itr->first);
		RetString.replace (ATL::String ('$') + itr->first, itr->second, true, 0, true);
	}

	return RetString;
}

