#pragma once
#include "PlayerAction.h"

#include <AtlString.h>

class UseWith: public PlayerAction
{
public:
	UseWith (GameObject* gameObject);
	UseWith (const UseWith& rhs, GameObject* gameObject);
	virtual ~UseWith (void);

	virtual ParseResult attemptParse (ATL::String command);
	
	virtual bool evaluateTargetFromString (ActionTarget target,
										   const ATL::String& fromString,
										   ATL::String& retText,
										   ATL::String& failString,
										   const ATL::String& verb);

// 	ParseResult evaluateActionTargetsFromString (ATL::String params);
	
	void addTargetFromKeyVal (const KeyValList_t params);

	virtual PlayerAction_ptr copy (GameObject* newParent) const;

protected:
	//virtual ParseResult extractVerbParams (ATL::String command);
	
	virtual bool attemptParseProperty (const ATL::String& property,
									   const ATL::String& value);
};

