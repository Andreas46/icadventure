#pragma once

#include <memory>

#include "Cue.h"
#include "CueCondition.h"
#include "PlayerActionTarget.h"

class CuePrintIf: public Cue
{
public:
	static TextSourceList_t extractCueFromText (ATL::String& text);
	static void formatRetTextSources (TextSourceList_t& retTextSources);

	CuePrintIf (void);
	CuePrintIf (const CuePrintIf& rhs);
	virtual ~CuePrintIf (void);

	virtual bool execute (void);
	virtual ATL::String execAndGetRetString (void);

	virtual ATL::String getName (void);

protected:
	CueCondition_ptr condition;
	ATL::String textToPrint;
	ATL::String textToPrintOnFailure;
	
	CueList_t execCues;

	virtual void parseStoredCueString (void);
};
