#pragma once

#include <AtlString.h>
#include <map>
#include <random>

#include "Room.h"

#include "DebugLog.h"
#include "DefaultActions.h"
#include "FsManager.h"
#include "Graphics.h"
#include "RawInputHandler.h"
#include "Inventory.h"

#include "GraphicsPopUpWindow.h"

#define GMGR GameManager::getInstance()
#define GET_INV GameManager::getInstance().getInventory()
#define PUSH_IN_HANDLER(X) GameManager::getInstance().pushInputHandler (X)
#define POP_IN_HANDLER GameManager::getInstance().pushInputHandler ()
#define REMOVE_IN_HANDLER(X) GameManager::getInstance().removeInputHandler (X)

class GameManager
{
public:
	typedef enum
	{
		UnassignedContext = 0,
		InventoryContext,
		CurrentRoomContext,
		DefaultContext,
		NoMoreContexts
	} CommandContext;

public:
	bool isRestarting;
	
	GameManager (void);
	~GameManager (void);

	static GameManager& getInstance (void);
	
	void seedRandNumGen (void);
	int64_t getRandomNumber (const int64_t min, const int64_t max);

	void loadAllFiles (void);
	void enterGameLoop (void);
	void processCommand (ATL::String command);
	
	void moveToRoom (ATL::String roomName);
	bool isThereARoomNamed (ATL::String roomName);
	Room_ptr getRoom (const ATL::String roomName);

	Tractable_ptr getTractableFromCurrentContext (ATL::String objName);
	bool isTractableInScope (const ATL::String& objName) const;
	//void destroyTractable (const ATL::String& objName);
	Tractable_ptr popTractable (const ATL::String& objName);

	Room_ptr getCurrentRoom (void);
	Inventory& getInventory (void);
	
	void pushInputHandler (RawInputHandler* handler);
	RawInputHandler* popInputHandler (void);
	void removeInputHandler (RawInputHandler* handler);

	void quit (void);

protected:
	static GameManager* instance;
	RawInputManager inputManager;
	DebugLog debugLog;
	DefaultActions defaultActions;
	FsManager fsManager;
	Graphics graphics;
	Inventory inventory;

//	GraphicsPopUpWindow popUp;
	
	std::mt19937 randGen;

	std::map <ATL::String, Room_ptr> rooms;
	//std::vector <ATL::String, VerbParser*> verbParsingContexts;

	bool isQuitting;
	ATL::String startingLocation;
	
	bool debugModeEnabled;

	static void inputCommandCallback (ATL::String params);
	
	ParseResult attemptParseControlCommand (ATL::String command);

	void initialiseGameState (void);
	
	VerbParser* getVerbParser (int context);

private:
	Room_ptr currentRoom;
};

