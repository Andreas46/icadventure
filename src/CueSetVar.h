#pragma once

#include "Cue.h"

class CueSetVar: public Cue
{
public:
	virtual bool execute (void);
	virtual ATL::String getName (void);

protected:
	ATL::String varName;
	ATL::String newVarValue;
	
	virtual void parseStoredCueString (void);
};