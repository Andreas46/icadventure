#pragma once

#include <AtlString.h>

#include <list>
#include <map>

#include "ForwardDeclarations.h"
#include "PlayerAction.h"

class FileParser
{
public:
	FileParser();
	virtual ~FileParser();

	bool parseAllResourceFiles (const ATL::String& resourcesPath);

	virtual void parseFile (ATL::String filePath);
	
	virtual void clear (void);
	
protected:
	ATL::String currentSection;
	KeyValList_t sectionKeyPairs;
	
	PlayerActionList_t customActions;
	StringMap_t valueVariables;
	
	static int inihHandler (void* user,
					  const char* section,
					  const char* name,
					  const char* value
						   );
	
	static void sectionHandler (void* user, const char* sectionName);
	
	virtual void parseSection (void) = 0;

	// The first parse of the value portion of the key/value.
	// It deals with Variable substituion.
	ATL::String firstParseOfValue (const ATL::String& value) const;
};
