#pragma once

#include "FileParser.h"

#include "DefaultActions.h"

class DefaultActionLists
{
public:
	TractableList_t emptyObjects;
	PlayerActionList_t playerActions;
	DefaultResponseList_t defaultResponses;
	StringVector_t badCommandResponses;
};

class FileParserDefault: public FileParser
{
public:
	DefaultActionLists parseDefaultFile (ATL::String filePath);

protected:
	TractableList_t emptyObjects;
	PlayerActionList_t playerActions;
	DefaultResponseList_t defaultResponses;
	StringVector_t badCommandResponses;

	virtual void parseSection (void);
};
