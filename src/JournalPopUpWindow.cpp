#include "JournalPopUpWindow.h"

#include "DebugLog.h"
#include "PlayerAction.h"

#define journalPath "resources/story_text/journal.txt"

void JournalPopUpWindow::showJournal (int startPage, int width, int height)
{
	PopUp_ptr newPopUp = std::make_shared <JournalPopUpWindow> ();

	newPopUp->setWidth (width);
	newPopUp->setFixedWinHeight (height);
	newPopUp->setCentreJustified (false);

	newPopUp->addTextFromFile (journalPath);

	activePopUps.insert (newPopUp);
}

JournalPopUpWindow::JournalPopUpWindow (void)
{
	//ATL::String prompt;
	
	// Setup prompt.
	prompt = "[<-- Previous Page]  [space to close]  [Next Page -->]";
}

JournalPopUpWindow::~JournalPopUpWindow (void)
{

}

void JournalPopUpWindow::setPageNum (int pageNum)
{
	ATL::String newPrompt;
	
	ASSERT (pageNum >= 0 && (size_t) pageNum < getNumPages(),
		"JPUW: Invalid page num: " + pageNum);

	currentPage = pageNum;
	clear();  // Clear window of drawn text.
	
	if (pageNum > 0)
	{
		newPrompt << "[<-- Page " << (pageNum) << "]";
	}
	
	newPrompt += "   [Space to close]   ";
	
	if ((size_t) pageNum != getNumPages() - 1)
	{
		newPrompt << "[Page " << (pageNum + 2) << " -->]";
	}
	
	prompt = newPrompt;

	// Hack to trigger even when reading page 9.
	if (pageNum == 8)
	{
		PlayerAction::setStoryVariable ("blessing_known", "true");
	}

}

bool JournalPopUpWindow::handleChar (int c)
{
	if (c == 'a' || c == KEY_LEFT)
	{
		if (currentPage > 0)
		{
			setPageNum (currentPage - 1);
			drawText();

			return false;
		}
	}
	else if (c == 'd' || c == KEY_RIGHT)
	{
		if ((size_t)(currentPage + 1) < getNumPages())
		{
	// 		++currentPage;
	// 		clear();
			//recalculateDimensions ();
	//		generatePages();
	//		recalculateDimensions ();
			setPageNum (currentPage + 1);
			drawText();

			return false;
		}
	}
	else if (c == ' ' || c == 27) // 27 = Esc
	{
		setHidden (true);
		return false;
	}
	return false;
}
