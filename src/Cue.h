#pragma once

#include <AtlString.h>

#include <list>
#include <memory>
#include <set>

#include "ForwardDeclarations.h"

class CueTextSplit
{
public:
	ATL::String preCue;
	ATL::String cue;
	ATL::String postCue;
	
	inline bool wasCueFound (void) { return !cue.empty(); }
};

/**
* @brief A pre-defined action that can be executed for various events.
**/
class Cue
{
public:
	Cue (void);
	//Cue (ATL::String cueAsString, bool parseImidiately = false);
	virtual ~Cue (void);
	
	static CueTextSplit extractCueTextFromString (const ATL::String& src,
												  const size_t startPos = 0);
	
	static Cue_ptr newCueFromString (ATL::String cueAsString,
									 bool parseImidiately = false);

	/**
	* @brief Return extracted cues, modifying "str" with the cue syntax removed.
	*
	* @param[out] str
	* 	The string to extract the cues from.  Str will be upded with the
	* 	Cue syntax "%(...)" removed, and return an orded list of those
	* 	Cues extracted.
	*
	* @return
	* 	A list of the parsed Cues.
	**/
	static CueList_t extractAllCuesFromString (ATL::String& str, const bool skipPrintIf = true);

	static void parseAllCues (void);

	virtual void setParameters (const ATL::String& parameters);

	virtual bool execute (void) = 0;

	virtual ATL::String getName (void) = 0;

protected:
	static std::set <Cue*> allTheCues;
	
	bool hasCueBeenParsed;
	ATL::String unparsedCueParameters;

	virtual void parseStoredCueString (void) = 0;
};

