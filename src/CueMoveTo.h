#pragma once

#include <memory>

#include "Cue.h"

class Room;
class CueMoveTo;

typedef std::shared_ptr <CueMoveTo> CueMoveTo_ptr;

class CueMoveTo: public Cue
{
public:
	CueMoveTo (void);
	~CueMoveTo (void);

	virtual bool execute (void);

	virtual ATL::String getName (void);

protected:
	ATL::String targetRoomName;
	
	virtual void parseStoredCueString (void);
};
