#include "CueCondition.h"

#include "DebugLog.h"
#include "GameManager.h"
#include "Tractable.h"

//std::set <CueCondition*> CueCondition::allTheConditions;

CueCondition_ptr CueCondition::newCueCondFromString (ATL::String& inString)
{
	ATL::String condName;
	ATL::String condParams;
	int condLen;
	CueCondition_ptr retPtr;

	// Extract the first "word" of the string (should be the cue's name).
	condLen = static_cast <int> (inString.find (" "));

	if (condLen == -1)
	{
		// Zero param string?
		condName = inString;
	}
	else
	{
		condName = inString.subStr (0, condLen);
		condParams = inString.subStr (condLen + 1);
	}

	LOGV ("Parsing cue condition: ", inString);

	if (condName == "in_room")
	{
		retPtr = std::make_shared <CueConditionInRoom>();
	}
	else if (condName == "not")
	{
		retPtr = std::make_shared <CueConditionNot>();
	}
	else if (condName == "obj_in_inv")
	{
		retPtr = std::make_shared <CueConditionObjInInv>();
	}
	else if (condName == "obj_in_room")
	{
		retPtr = std::make_shared <CueConditionObjInRoom>();
	}
	else if (condName == "obj_in_current_room")
	{
		retPtr = std::make_shared <CueConditionObjInCurrentRoom>();
	}
	else if (condName == "obj_in_state")
	{
		retPtr = std::make_shared <CueConditionIsObjInState>();
	}
	else if (condName == "var_equal")
	{
		retPtr = std::make_shared <CueConditionVarEqual>();
	}
	else if (condName == "var_not_equal")
	{
		retPtr = std::make_shared <CueConditionVarNotEqual>();
	}
	else if (condName == "room_in_state")
	{
		retPtr = std::make_shared <CueConditionRoomInState>();
	}
	else
	{
		LOGE ("Failed to identify que condition '", condName + "' in String: " + inString);
		//retPtr = std::make_shared <CueNoOp>();
		//FAIL (":[");

		// Use "false" cue as placeholder.
		retPtr = std::make_shared <CueConditionFalse>();
	}

	//retPtr->setParameters (condParams);
	//retPtr->parseStoredString();
	
	// Modify original string to be stripped of this condition's syntax.
	inString = retPtr->parseFromString (condParams);

	return retPtr;
}

// void CueCondition::parseAllCueConditions (void)
// {
// 	std::set <CueCondition*>::iterator itr;
// 	
// 	for (itr = allTheConditions.begin(); itr != allTheConditions.end(); ++itr)
// 	{
// 		(*itr)->parseStoredString();
// 	}
// }

CueCondition::CueCondition (void)
{
	hasConditionBeenParsed = false;
//	allTheConditions.insert (this);
}

CueCondition::~CueCondition (void)
{
//	allTheConditions.erase (this);
}

// --- CueConditionIsObjInState --- //

bool CueConditionIsObjInState::evaluate (void)
{
	Tractable_ptr tractable;
	ATL::String logStr;

//	LOG ("From stored Str: ", unparsedString);
	
	ASSERT (hasConditionBeenParsed, "Can't evalue unparsed Cue Condition!");
	//if (!hasConditionBeenParsed)
	//{
		//parseStoredString();
		
	//}
	
	logStr << "CueConditionIsObjInState: Is State of '" <<
		NameOfObjToCheck << "' equal to '" << state << "': ";
	
	tractable = GMGR.getTractableFromCurrentContext (NameOfObjToCheck);
	
	if (tractable->getState () == state)
	{
		DebugLog::log (logStr + "true");
		return true;
	}
	else
	{
		DebugLog::log (logStr + "true");
		return false;
	}
}

ATL::String CueConditionIsObjInState::getName (void)
{
	return "obj_in_state";
}

//void CueConditionIsObjInState::parseStoredString (void)
ATL::String CueConditionIsObjInState::parseFromString (const ATL::String& parameters)
{
	// Format: %(obj_in_state objName newState)

//	LOG ("Parseing stored Str: ", parameters);
	
	// Extract the object name.
	NameOfObjToCheck = parameters.getNextWord();

	ASSERT (!NameOfObjToCheck.empty(),
			"CueConditionIsObjInState: Object name must not be empty!");
	
	// Next we get the new state.
	state = parameters.getNextWord (NameOfObjToCheck.length());
	
	ASSERT (!state.empty(), "CueConditionIsObjInState: state MUST be specified!");
	
	//ATL::String logStr;
// 	logStr << "CueConditionIsObjInState: Is State of '" <<
// 		NameOfObjToCheck << "' equal to '" << state << "': ";
// 	LOG (logStr,"");

	hasConditionBeenParsed = true;
	
	// +2 for the spaces following each token.
	return parameters.subStr (NameOfObjToCheck.length() + state.length() + 2);
}

// --- CueConditionEqual --- //

bool CueConditionVarEqual::evaluate (void)
{
	ATL::String storyVarState;

	if (PlayerAction::isStoryVarSet (varName))
	{
		storyVarState = PlayerAction::getStoryVariable (varName);
	}
	else
	{
		// Variable has not actually been set, so we'll proceed as if it's
		// value is an empty string.
		storyVarState = "";
	}

	if (storyVarState == compareState)
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionVarEqual::getName (void)
{
	return "var_equal";
}

ATL::String CueConditionVarEqual::parseFromString (const ATL::String& parameters)
{
	// Format: var_equ varName testState

	// Extract the variable name.
	varName = parameters.getNextWord();

	ASSERT (!varName.empty(),
			"CueConditionVarNotEqual: Variable name must not be empty!");

	// Next we get the state to test against.
	compareState = parameters.getNextWord (varName.length());

	ASSERT (!compareState.empty(),
			"CueConditionVarNotEqual: test state MUST be specified!");

	LOG ("Adding 'var_not_equal' cue condition for var '", varName +
		"' tested against state: " + compareState);

	hasConditionBeenParsed = true;

	// +2 for the spaces following each token.
	return parameters.subStr (varName.length() + compareState.length() + 2);
}

// --- CueConditionVarNotEqual --- //

bool CueConditionVarNotEqual::evaluate (void)
{
	ATL::String storyVarState;

	if (PlayerAction::isStoryVarSet (varName))
	{
		storyVarState = PlayerAction::getStoryVariable (varName);
	}
	else
	{
		// Variable has not actually been set, so we'll proceed as if it's
		// value is an empty string.
		storyVarState = "";
	}
	
	// Remember this the /NOT EQUAL TO/ test.
	if (storyVarState != compareState)
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionVarNotEqual::getName (void)
{
	return "var_not_equal";
}

ATL::String CueConditionVarNotEqual::parseFromString (const ATL::String& parameters)
{
	// Format: var_not_equ varName testState
	
	// Extract the variable name.
	varName = parameters.getNextWord();

	ASSERT (!varName.empty(),
			"CueConditionVarNotEqual: Variable name must not be empty!");
	
	// Next we get the state to test against.
	compareState = parameters.getNextWord (varName.length());
	
	ASSERT (!compareState.empty(),
			"CueConditionVarNotEqual: test state MUST be specified!");

	LOG ("Adding 'var_not_equal' cue condition for var '", varName +
		"' tested against state: " + compareState);

	hasConditionBeenParsed = true;
	
	// +2 for the spaces following each token.
	return parameters.subStr (varName.length() + compareState.length() + 2);
}

// --- CueConditionRoomInState --- //

bool CueConditionRoomInState::evaluate (void)
{
	ATL::String roomCurrState;

	// Make sure this room actually exists.
	if (!GMGR.isThereARoomNamed (roomName))
	{
		LOGE ("Can't execute roomInState Cue Condition for room '", roomName +
			"' as it does not exist!");
		return false;
	}

	roomCurrState = GMGR.getRoom (roomName)->getState();

	// Is the state equal?
	if (compareState == roomCurrState)
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionRoomInState::getName (void)
{
	return "room_in_state";
}

ATL::String CueConditionRoomInState::parseFromString (const ATL::String& parameters)
{
	// Format: room_in_state dependantState

	// Extract the room name.
	roomName = parameters.getNextWord();

	ASSERT (!roomName.empty(),
			"CueConditionRoomInState: Room name must not be empty!");

	// Next we get the state to test against.
	compareState = parameters.getNextWord (roomName.length());

	ASSERT (!compareState.empty(),
			"CueConditionRoomInState: test state MUST be specified!");

	LOG ("Adding 'room_in_state' cue condition for room '", roomName +
		"' tested against state: " + compareState);

	hasConditionBeenParsed = true;

	// +2 for the spaces following each token.
	return parameters.subStr (roomName.length() + compareState.length() + 2);
}

// --- CueConditionInRoom --- //

bool CueConditionInRoom::evaluate (void)
{
	// Does this room contain the desired tractable?
	if (GMGR.getCurrentRoom()->getName() == roomName)
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionInRoom::getName (void)
{
	return "in_room";
}

ATL::String CueConditionInRoom::parseFromString (const ATL::String& parameters)
{
	// Format: roomName objName

	// Extract the room name.
	roomName = parameters.getNextWord();

	ASSERT (!roomName.empty(),
			"CueConditionInRoom: Room name must not be empty!");

	hasConditionBeenParsed = true;

	// +1 for space following roomname.
	return parameters.subStr (roomName.length() + 1);
}

// --- CueConditionObjInRoom --- //

bool CueConditionObjInRoom::evaluate (void)
{
	Room_ptr currRoom;

	// Make sure this room actually exists.
	if (!GMGR.isThereARoomNamed (roomName))
	{
		LOGE ("Can't execute 'obj_in_room' Cue Condition for room '", roomName +
			"' as it the room does not exist!");
		return false;
	}

	currRoom = GMGR.getRoom (roomName);

	// Does this room contain the desired tractable?
	if (currRoom->isTractableInScope (objectName))
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionObjInRoom::getName (void)
{
	return "obj_in_room";
}

ATL::String CueConditionObjInRoom::parseFromString (const ATL::String& parameters)
{
	// Format: roomName objName

	// Extract the room name.
	roomName = parameters.getNextWord();

	ASSERT (!roomName.empty(),
			"CueConditionObjInRoom: Room name must not be empty!");

	// Next we get object to search for.
	objectName = parameters.getNextWord (roomName.length());

	ASSERT (!objectName.empty(),
			"CueConditionObjInRoom: test state MUST be specified!");

	LOGV ("Adding 'obj_in_room' cue condition for room '", roomName +
		"' tested against state: " + objectName);

	hasConditionBeenParsed = true;

	// +2 for the spaces following each token.
	return parameters.subStr (roomName.length() + objectName.length() + 2);
}

// --- CueConditionObjInCurrentRoom --- //

bool CueConditionObjInCurrentRoom::evaluate (void)
{
	Room_ptr currRoom;

	currRoom = GMGR.getCurrentRoom ();

	// Does this room contain the desired tractable?
	if (currRoom->isTractableInScope (objectName))
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionObjInCurrentRoom::getName (void)
{
	return "obj_in_current_room";
}

ATL::String CueConditionObjInCurrentRoom::parseFromString (const ATL::String& parameters)
{
	// Format: obj_in_current_room objName

	// Extract the room name.
	objectName = parameters.getNextWord();

	ASSERT (!objectName.empty(),
			"CueConditionObjInCurrentRoom: Object name must not be empty!");

	LOGV ("Adding 'obj_in_current_room' cue condition for object '", objectName + "'.");

	hasConditionBeenParsed = true;

	// +1 for the space following the token.
	return parameters.subStr (objectName.length() + 1);
}

// --- CueConditionObjInInv --- //

bool CueConditionObjInInv::evaluate (void)
{
	// Does this room contain the desired tractable?
	if (GMGR.getInventory().isTractableInScope (objectName))
	{
		return true;
	}
	else
	{
		return false;
	}
}

ATL::String CueConditionObjInInv::getName (void)
{
	return "obj_in_inv";
}

ATL::String CueConditionObjInInv::parseFromString (const ATL::String& parameters)
{
	// Format: objName

	// Extract the object name.
	objectName = parameters.getNextWord();

	ASSERT (!objectName.empty(),
			"CueConditionObjInInv: Object name must not be empty!");

	LOGV ("Adding 'obj_in_inv' cue condition for object '", objectName +
		"'.");

	hasConditionBeenParsed = true;

	// +1 for the space following token.
	return parameters.subStr (objectName.length() + 1);
}

// --- CueConditionFalse --- //

bool CueConditionFalse::evaluate (void)
{
	// Always return false;
	return false;
}

ATL::String CueConditionFalse::getName (void)
{
	return "False";
}


ATL::String CueConditionFalse::parseFromString (const ATL::String& parameters)
{
	// False.
	return parameters;
}

// --- Logical "NOT" condition modifier --- //

bool CueConditionNot::evaluate (void)
{
	return !conditionToProcess->evaluate();
}

ATL::String CueConditionNot::getName (void)
{
	return "not";
}

ATL::String CueConditionNot::parseFromString (const ATL::String& parameters)
{
	ATL::String retString = parameters;

	conditionToProcess = CueCondition::newCueCondFromString (retString);

	hasConditionBeenParsed = true;

	// +1 for the space following token.
	return retString;
}
