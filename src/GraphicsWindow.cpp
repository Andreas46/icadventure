#include "GraphicsWindow.h"

#include <vector>

#include "Graphics.h"

GraphicsWindow::GraphicsWindow (void)
{
	posX = 0;
	posY = 0;

	sizeX = 0;
	sizeY = 0;
	
	isHidden = false;

	// Create the curses window.
	cursWin = newwin (0, 0, 0, 0);

	// Create the pannel.  This sets the initial ordering of the windows.
	cursPanel = new_panel (cursWin);
}

GraphicsWindow::~GraphicsWindow (void)
{
	del_panel (cursPanel);
	delwin (cursWin);
}

void GraphicsWindow::setSize (unsigned newPosX, unsigned newPosY,
							  unsigned newSizeX, unsigned newSizeY)
{
	// No need to redraw unless the windows position, or size has changes.
	if (posX != newPosX ||
		posY != newPosY ||
		sizeX != newSizeX ||
		sizeY != newSizeY)
	{
		// Destroy the old boarder.
		wborder (cursWin, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	//	wrefresh (cursWin);
	//	refresh();
	}
	
	posX = newPosX;
	posY = newPosY;

	sizeX = newSizeX;
	sizeY = newSizeY;
	
	wresize (cursWin, newSizeY, newSizeX);
	mvwin (cursWin, newPosY, newPosX);
	//move_panel (cursPanel, newPosX, newPosX);
	
	drawBoarder ();
	drawText ();
	
	//wrefresh (cursWin);
}

void GraphicsWindow::drawBoarder (void)
{
	unsigned screenWidth, screenHeight;
	int ulcorner, urcorner, llcorner, lrcorner;
	
	getmaxyx (stdscr, screenHeight, screenWidth);
	
	// Determin Upperleft corner marker.
	if (posX == 0 && posY != 0)
	{
		ulcorner = ACS_LTEE;
	}
	else
	{
		ulcorner = ACS_ULCORNER;
	}
	
	// Determin upper right corner marker.
	if (sizeX == screenWidth && posY != 0)
	{
		urcorner = ACS_RTEE;
	}
	else
	{
		urcorner = ACS_URCORNER;
	}
	
	// Determin lower left corner marker.
	if (posX == 0 && sizeY != screenHeight - posY)
	{
		llcorner = ACS_LTEE;
	}
	else
	{
		llcorner = ACS_LLCORNER;
	}

	// Determin lower right corner marker.
	if (screenWidth - posX == sizeX && sizeY != screenHeight - posY)
	{
		lrcorner = ACS_RTEE;
	}
	else
	{
		lrcorner = ACS_LRCORNER;
	}

	wborder(cursWin, ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE,
		  ulcorner, urcorner, llcorner, lrcorner);
	//box (cursWin, 0 , 0);
}

void GraphicsWindow::addText (ATL::String text)
{
	winText += text;
	
	drawText();

//	wrefresh (cursWin);
	Graphics::refreshScreen ();
}

void GraphicsWindow::clearText (void)
{
	winText.clear();
	werase (cursWin);
	drawText();
//	wrefresh(cursWin);
	Graphics::refreshScreen ();
}

void GraphicsWindow::setText (ATL::String text)
{
	//winText = text;

	winText.clear ();
	addText (text);

	//werase (cursWin);
//	drawText();
}

void GraphicsWindow::update (void)
{

}

void GraphicsWindow::setHidden (bool hidden)
{
	isHidden = hidden;
	
	if (isHidden)
	{
		hide_panel (cursPanel);
	}
	else
	{
		show_panel (cursPanel);
	}
}
WINDOW* GraphicsWindow::getWindow (void)
{
	return cursWin;
}


StringVector_t GraphicsWindow::getWindowLinesForText (const ATL::String& text)
{
	// We will need to sort all the text into lines, each being not longer
	// than the width of the window.
	StringVector_t lines;
	ATL::String line;
	ATL::String word;
	
	//unsigned int lineStart = 0;
	
	if (EFFECTIVE_WIN_X < 1 || EFFECTIVE_WIN_Y < 1)
	{
		// No. |:-[
		return lines;
	}
	
	// Build the lines list.
	for (unsigned i = 0; i < text.length(); ++i)
	{
		if (line.length() + word.length() >= EFFECTIVE_WIN_X)
		{
			lines.push_back (line);
			line.clear();
		}

		//if (word.length () == 0 && text [i] == ' ')
		//{
		//	// trim leading white space.
		//	continue;
		//}

		if (text [i] == ' ' || text [i] == '\n')
		{
			//if (word.length() > 0 &&
			//	winText [i] == ' ')
			//{
			//	word.append (' ');
			//}

			if (word.length() > 0 &&
				i + 1 < text.length() &&
				text [i + 1] != ' ')
			{
				//if ()
				// Add a word seperator (only if this is not
				// the first word in the line)
				if (line.length() > 0)
				{
					line.append (' ');
				}

				line.append (word);
				word.clear();
			}
			else if (text [i] == ' ')
			{
				word.append (' ');
			}
		}
		else
		{
			word.append (text [i]);
		}

		//if (winText [i] == ' ' )// && word.length () > 0 && line.length () > 0);
		//{
		//	line.append (' ');
		//}

		if (text [i] == '\n')
		{
			if (!word.empty ())
			{
				line << " " << word;
				word.clear();
			}

			lines.push_back (line);
			line.clear();
		}
	}

	// Handle last line (sans trailing \n).
	if (word.length() > 0)
	{
		// Add a word seperator (only if this is not
		// the first word in the line)
		if (line.length() > 0)
		{
			line.append (' ');
		}

		line.append (word);
	}
	if (line.length() > 0)
	{
		lines.push_back (line);
	}

	return lines;
}

// This is a rather arduious proceedure, as curses does not really provide any
// methods for automatically word-wrapping, or padding the edges of a window.
// This will therefor, need to be done manually. ):-[
void GraphicsWindow::drawText (void)
{
	//// We will need to sort all the text into lines, each being not longer
	//// than the width of the window.
	//std::vector <ATL::String> lines;
	//ATL::String line;
	//ATL::String word;
	//
	unsigned int lineStart = 0;
	//
	//if (EFFECTIVE_WIN_X < 1 || EFFECTIVE_WIN_Y < 1)
	//{
	//	// No. |:-[
	//	return;
	//}
	//
	//// Build the lines list.
	//for (unsigned i = 0; i < winText.length(); ++i)
	//{
	//	if (line.length() + word.length() >= EFFECTIVE_WIN_X)
	//	{
	//		lines.push_back (line);
	//		line.clear();
	//	}

	//	if (word.length () == 0 && winText [i] == ' ')
	//	{
	//		// trim leading white space.
	//		continue;
	//	}

	//	if (winText [i] == ' ' || winText [i] == '\n')
	//	{
	//		//if (word.length() > 0 &&
	//		//	winText [i] == ' ')
	//		//{
	//		//	word.append (' ');
	//		//}

	//		if (word.length() > 0 &&
	//			i + 1 < winText.length() &&
	//			winText [i + 1] != ' ')
	//		{
	//			//if ()
	//			// Add a word seperator (only if this is not
	//			// the first word in the line)
	//			if (line.length() > 0)
	//			{
	//				line.append (' ');
	//			}

	//			line.append (word);
	//			word.clear();
	//		}
	//		else if (winText [i] == ' ')
	//		{
	//			word.append (' ');
	//		}
	//	}
	//	else
	//	{
	//		word.append (winText [i]);
	//	}

	//	//if (winText [i] == ' ' )// && word.length () > 0 && line.length () > 0);
	//	//{
	//	//	line.append (' ');
	//	//}

	//	if (winText [i] == '\n')
	//	{
	//		lines.push_back (line);
	//		line.clear();
	//	}
	//}

	//// Handle last line (sans trailing \n).
	//if (word.length() > 0)
	//{
	//	// Add a word seperator (only if this is not
	//	// the first word in the line)
	//	if (line.length() > 0)
	//	{
	//		line.append (' ');
	//	}

	//	line.append (word);
	//}
	//if (line.length() > 0)
	//{
	//	lines.push_back (line);
	//}

	StringVector_t lines = getWindowLinesForText (winText);

	// If we have more lines than can be displayed
	if (lines.size() > EFFECTIVE_WIN_Y)
	{
		// Calculate an offset so we only display the last of the lines.
		lineStart = lines.size() - EFFECTIVE_WIN_Y;
		
		// Lines are now moving up, so we'll also need to erase the
		// screen to prevent artifacts.  This will cause blinking. :(
		//werase (cursWin);
		//drawBoarder();
	}
	
	// Print the Lines.
	for (unsigned i = 0; i < lines.size() - lineStart; ++i)
	{
		ATL::String lineToPrint = lines[i + lineStart];

		// Print the line.
		mvwprintw (cursWin, i + WIN_PADDING_Y, WIN_PADDING_X, lineToPrint.cStr());
		
 		// Fill the remainder of the line with spaces, to prevent artifacts.
 		zeroLineRemainder();
// 		for (unsigned j = lineToPrint.length() - WIN_PADDING_X; j < EFFECTIVE_WIN_X; ++j)
// 		{
// 			waddch (cursWin, ' ');
// 		}
	}
}

void GraphicsWindow::zeroLineRemainder (void)
{
	int cursPosX ;
	
	// Get cursor position.
	getyx (cursWin, cursPosX, cursPosX);
	
	// Stupid unsigned integers. |:-[
	if (sizeX < WIN_PADDING_X)
	{
		return;
	}
	
	// Fill the remainder of the line with spaces, to prevent artifacts.
	for (unsigned i = cursPosX; i < sizeX - WIN_PADDING_X + 1; ++i)
	{
		waddch (cursWin, ' ');
	}
}
