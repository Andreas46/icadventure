#include <iostream>

#include "GameManager.h"

void printLogo (void)
{
std::cout << "                                                        .,                   " << std::endl;
std::cout << "                                                          ,,                 " << std::endl;
std::cout << "                                                            ,,               " << std::endl;
std::cout << "                                $$$$$$    $$$$$.   $ZZZZZ.ZZZZZ$ZZZZZZZZZZZZ~" << std::endl;
std::cout << "                                $$$$$$    $$$$$.   $$$$$$.$$$$$$$$$$$$$$$$$$:" << std::endl;
std::cout << "                              $ $$$$$$    $$$$$.   $$$$$$.$$$$$$$$$$$$$$$$$$:" << std::endl;
std::cout << "                            .$$ $$$$$$    $$$$$.   $$$$$$.$$$$$$$$$$$$$$$$$$:" << std::endl;
std::cout << "                          .$$$$ $$$$$$    $$$$$.   $$$$$$.     ,,,,,,        " << std::endl;
std::cout << "                         .$$$$$ $$$$$$    $$$$$.   $$$$$$.     ,,,,,,.       " << std::endl;
std::cout << "                       .$$$$$$$ $$$$$$    $$$$$.   $$$$$$......,,,,,,,....   " << std::endl;
std::cout << "                      :$$$$$$$, $$$$$$    $$$$$.   $$$$$$.$$$$$$$$$$$$$$$.   " << std::endl;
std::cout << "                    .$$$$$$$Z.  $$$$$$    $$$$$.   $$$$$$.$$$$$$$$$$$$$$$.   " << std::endl;
std::cout << "                   $$$$$$$$.    $$$$$$    $$$$$.   $$$$$$.$$$$$$$$$$$$$$$.   " << std::endl;
std::cout << "                 .$$$$$$$$$$$$$ $$$$$$    $$$$$.   $$$$$$.    ::::::,,       " << std::endl;
std::cout << "               .Z$$$$$$$$$$$$$$ $$$$$$    $$$$$.   $$$$$$.   ::::::::,     " << std::endl;
std::cout << "              .$$$$$$$$$$$$$$$$ $$$$$$    $$$$$.   $$$$$$.  ~:::::::::    " << std::endl;
std::cout << "            .$$$$$$$$.          $$$$$$    $$$$$.   $$$$$$. ~~~:::::::       " << std::endl;
std::cout << "           ,$$$$$$$=.           $$$$$$    $$$$$.   $$$$$$$$$$$$$$$$$$$$$$$$$~ " << std::endl;
std::cout << "         .$$$$$$$$.             $$$$$$    $$$$$.   $$$$$$$$$$$$$$$$$$$$$$$$$~" << std::endl;
std::cout << " .      I$$$$$$$.               $$$$$$    $$$$$.   $$$$$$$$$$$$$$$$$$$$$$$$$~" << std::endl;
std::cout << "   Z                                              .=======~~~~~~~    " << std::endl;
std::cout << "     ZZ                                         +++=========~~~    " << std::endl;
std::cout << "       Z$$Z                                 ++++++++=========,  " << std::endl;
std::cout << "         .$$$$$$I                    .=??????++++++++======    " << std::endl;
std::cout << "             $$$$$$777777777IIIIIIIII?????????+++++++++=        " << std::endl;
std::cout << "                .$$$$77777777IIIIIIIII?????????+++++,          " << std::endl;
std::cout << "                    ..,7777777IIIIIIIII????????                   " << std::endl;
std::cout << "                                 .. .                           " << std::endl;
}

int main (void)
{
	GameManager* gameMgr = NULL;
	bool isRestarting = false;

	do
	{
		gameMgr = new GameManager();

		gameMgr->enterGameLoop();
		
		isRestarting = gameMgr->isRestarting;

		delete gameMgr;
	}
	while (isRestarting == true);

	printLogo ();

	std::cout << "\n\nCreated with the help and support of the AIE students, and incredible staff." << std::endl;

	std::cout << "\n\nThank you for playing I.C.Adventure." << std::endl;
	std::cout << "press ENTER to continue." << std::endl;

	std::cin.clear ();
	std::cin.ignore ();

	return 0;
}

